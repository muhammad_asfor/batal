import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/reports/domain/entities/report.dart';

abstract class ReportsRepository {
  Future<Either<Failure, List<Report>>> getReports();
  Future<Either<Failure, bool>> report(int reportTypeId, int itemId, String comment);
}
