import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/reports/domain/entities/report.dart';
import 'package:tajra/inner_layer/reports/domain/repositories/reports_repository.dart';

class GetReportsUseCase
    extends UseCase<List<Report>, NoParams> {
  final ReportsRepository repository;

  GetReportsUseCase(
    this.repository,
  );

  @override
  Future<Either<Failure, List<Report>>> call(
      NoParams params) async {
    return await repository.getReports();
  }
}
