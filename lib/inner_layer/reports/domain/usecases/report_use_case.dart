import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/reports/domain/repositories/reports_repository.dart';

class ReportUseCase extends UseCase<bool, ReportParams> {
  final ReportsRepository repository;

  ReportUseCase(
    this.repository,
  );

  @override
  Future<Either<Failure, bool>> call(ReportParams params) async {
    return await repository.report(
        params.reportTypeId, params.itemId, params.comment);
  }
}

class ReportParams {
  final int reportTypeId;
  final int itemId;
  final String comment;

  ReportParams(
      {required this.reportTypeId,
      required this.itemId,
      required this.comment});
}
