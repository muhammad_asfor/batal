import 'package:json_annotation/json_annotation.dart';

part 'report.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Report {
  final int id;
  final String? title;

  factory Report.fromJson(json) => _$ReportFromJson(json);

  toJson() => _$ReportToJson(this);

  static List<Report> fromJsonList(List json) {
    return json.map((e) => Report.fromJson(e)).toList();
  }

  Report({
    required this.id,
    this.title,
  });
}
