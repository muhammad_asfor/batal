import 'package:dio/dio.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/reports/domain/entities/report.dart';

class ReportsRemoteDataSource {
  ReportsRemoteDataSource();

  Future<List<Report>> getReports() async {
    try {
      return Report.fromJsonList(
          (await sl<Dio>().get("api/report_types")).data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> report(int reportTypeId, int itemId, String comment) async {
    try {
      await sl<Dio>().post("api/report_types", data: {
        'report_type_id': reportTypeId,
        'item_id': itemId,
        'comment': comment,
      });
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
