import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/reports/data/datasources/reports_remote_data_source.dart';
import 'package:tajra/inner_layer/reports/domain/entities/report.dart';
import 'package:tajra/inner_layer/reports/domain/repositories/reports_repository.dart';

class ReportsRepositoryImpl implements ReportsRepository {
  final ReportsRemoteDataSource reportsRemoteDataSource;

  ReportsRepositoryImpl(this.reportsRemoteDataSource);

  @override
  Future<Either<Failure, List<Report>>> getReports() async {
    try {
      final result =
          await reportsRemoteDataSource.getReports();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> report(int reportTypeId, int itemId, String comment) async {
    try {
      final result =
      await reportsRemoteDataSource.report(reportTypeId, itemId, comment);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
