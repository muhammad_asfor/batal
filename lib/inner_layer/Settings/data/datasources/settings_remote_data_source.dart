import 'package:dio/dio.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/MyPackage.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/pay.dart';

class SettingsRemoteDataSource {
  SettingsRemoteDataSource();

  Future<List<Package>> getPackages() async {
    try {
      return Package.fromJsonList(
          (await sl<Dio>().get("api/packages")).data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<pay> subscribe(Map<String, dynamic> params) async {
    try {
      final result = await sl<Dio>().post("api/subscriptions", data: params);
      pay resultall = pay.fromJson(result.data);
      return resultall;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
