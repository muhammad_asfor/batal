import 'package:shared_preferences/shared_preferences.dart';

class SettingsLocalDataSource {
  final SharedPreferences _preferences;

  SettingsLocalDataSource(this._preferences);
}

class SettingsConstants {
  // static const String preferences = "preferences";
}
