import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/Settings/data/datasources/settings_remote_data_source.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/MyPackage.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/pay.dart';
import 'package:tajra/inner_layer/Settings/domain/repositories/settings_repository.dart';

class SettingsRepositoryImpl implements SettingsRepository {
  final SettingsRemoteDataSource settingsRemoteDataSource;
  // final HomeSettingsLocalDataSource settingsLocalDataSource;

  SettingsRepositoryImpl(this.settingsRemoteDataSource);

  @override
  Future<Either<Failure, List<Package>>> getPackages() async {
    try {
      final result = await settingsRemoteDataSource.getPackages();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, pay>> subscribe(Map<String, dynamic> params) async {
    try {
      final result = await settingsRemoteDataSource.subscribe(params);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
