import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/MyPackage.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/pay.dart';

abstract class SettingsRepository {
  Future<Either<Failure, List<Package>>> getPackages();
  Future<Either<Failure, pay>> subscribe(Map<String, dynamic> params);
}
