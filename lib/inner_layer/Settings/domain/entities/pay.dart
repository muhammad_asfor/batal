class pay {
  bool? success;
  String? message;
  Data? data;

  pay({this.success, this.message, this.data});

  pay.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? userId;
  int? packageId;
  String? updatedAt;
  String? createdAt;
  int? id;
  String? uuid;
  String? payUrl;
  int? payId;
  String? sessionId;

  Data(
      {this.userId,
      this.packageId,
      this.updatedAt,
      this.createdAt,
      this.id,
      this.uuid,
      this.payUrl,
      this.payId,
      this.sessionId});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    packageId = json['package_id'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
    uuid = json['uuid'];
    payUrl = json['pay_url'];
    payId = json['pay_id'];
    sessionId = json['session_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['package_id'] = this.packageId;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['pay_url'] = this.payUrl;
    data['pay_id'] = this.payId;
    data['session_id'] = this.sessionId;
    return data;
  }
}
