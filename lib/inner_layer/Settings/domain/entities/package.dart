import 'package:json_annotation/json_annotation.dart';
part 'package.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Package {
  final int id;
  final String? priceText;
  final String? title;
  final String? description;

  factory Package.fromJson(json) => _$PackageFromJson(json);
  toJson() => _$PackageToJson(this);
  static List<Package> fromJsonList(List json) {
    return json.map((e) => Package.fromJson(e)).toList();
  }

  Package({
    required this.id,
    this.priceText,
    this.title,
    this.description,
  });
}
