import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/Settings/domain/repositories/settings_repository.dart';

class GetPackagesUseCase
    extends UseCase<List<Package>, NoParams> {
  final SettingsRepository repository;

  GetPackagesUseCase(
    this.repository,
  );

  @override
  Future<Either<Failure, List<Package>>> call(
      NoParams params) async {
    return await repository.getPackages();
  }
}
