import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/pay.dart';
import 'package:tajra/inner_layer/Settings/domain/repositories/settings_repository.dart';

class SubscribeUseCase extends UseCase<pay, SubscribeParams> {
  final SettingsRepository repository;

  SubscribeUseCase(
    this.repository,
  );

  @override
  Future<Either<Failure, pay>> call(SubscribeParams params) async {
    return await repository.subscribe(params.params);
  }
}

class SubscribeParams {
  final Map<String, dynamic> params;

  SubscribeParams(this.params);
}
