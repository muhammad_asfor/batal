import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Ui/Auth/forgot_password_page.dart';
import 'package:tajra/Ui/Auth/sign_up_page.dart';
import 'package:tajra/Ui/Search/pages/SearchPage.dart';
import 'package:tajra/Ui/Settings/add_service_page.dart';
import 'package:tajra/Ui/Settings/my_package.dart';
import 'package:tajra/Ui/Settings/no_acctev.dart';
import 'package:tajra/Ui/Settings/payment_done_page.dart';
import 'package:tajra/Ui/Settings/personal_profile_page.dart';
import 'package:tajra/Ui/worker_profile/worker_profile_page.dart';

import '/Ui/Onboardings/OnBoardingScreen.dart';
import '/Ui/Settings/SettingsPage.dart';
import './/Ui/BasePage/BasePage.dart';

import './/Ui/Home/HomePage.dart';
import './/Ui/Notifications/NotificationsPage.dart';
import './/Ui/Splash/SplashPage.dart';

import 'Ui/Auth/LoginPage.dart';
import 'Ui/Settings/cheakpage.dart';
import 'Ui/Settings/packages_page.dart';
import 'Ui/Settings/page_data_page.dart';
import 'Ui/Settings/payment_page.dart';
import 'Ui/Settings/promote_account_page.dart';

class AppRouter {
  static String currentRoute = "/";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    currentRoute = settings.name ?? "/";
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => SplashPage());
      case '/login':
        return CupertinoPageRoute(builder: (_) => LoginPage());
      case '/signUp':
        return CupertinoPageRoute(builder: (_) => SignUpPage());
      case '/forgotPassword':
        return CupertinoPageRoute(builder: (_) => ForgotPasswordPage());
      case '/base':
        return CupertinoPageRoute(builder: (_) => BasePage());
      case '/NoAcctevPage':
        return CupertinoPageRoute(builder: (_) => NoAcctevPage());
      case '/cheakpage':
        return CupertinoPageRoute(builder: (_) => cheakpage());
      case '/home':
        return CupertinoPageRoute(
            builder: (_) => HomePage(
                  onServiceTap: settings.arguments as Function,
                ));
      case '/promote_account':
        return CupertinoPageRoute(builder: (_) => PromoteAccountPage());
      case '/packages':
        final args = settings.arguments as Map;
        return CupertinoPageRoute(
            builder: (_) => PackagesPage(
                  fullName: args['name'],
                  countryCode: args['countryCode'],
                  mobile: args['mobile'],
                  countryId: args['countryId'],
                  stateId: args['stateId'],
                  cityId: args['cityId'],
                ));
      case '/payment':
        return CupertinoPageRoute(builder: (_) => PaymentPage());
      case '/profile':
        return CupertinoPageRoute(builder: (_) => SettingsPage());
      case '/notifications':
        return CupertinoPageRoute(builder: (_) => NotificationsPage());
      case '/onboard':
        return CupertinoPageRoute(builder: (_) => OnBoardingScreen());
      case '/MyPackagePage':
        return CupertinoPageRoute(builder: (_) => MyPackagePage());
      case '/workerProfile':
        final args = settings.arguments as String;
        return CupertinoPageRoute(
            builder: (_) => WorkerProfilePage(
                  id: args,
                ));
      case '/searchPage':
        final args = settings.arguments as Map;
        return CupertinoPageRoute(
            builder: (_) => SearchPage(
                  name: args['name'],
                ));
      case '/addService':
        return CupertinoPageRoute(builder: (_) => AddServicePage());
      case '/paymentDone':
        return CupertinoPageRoute(builder: (_) => PaymentDonePage());
      case '/personalProfile':
        return CupertinoPageRoute(builder: (_) => PersonalProfilePage());
      case '/pageData':
        return CupertinoPageRoute(
            builder: (_) =>
                PageDataPage(page: settings.arguments as PageModel));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
