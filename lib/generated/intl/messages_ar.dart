// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Back_to": MessageLookupByLibrary.simpleMessage("العودة الى التطبيق"),
        "Please_wait": MessageLookupByLibrary.simpleMessage(
            "طلبك قيد المعالجة نرجوا الانتظار,شكرا."),
        "activated": MessageLookupByLibrary.simpleMessage("مفعل"),
        "addAddress": MessageLookupByLibrary.simpleMessage("إضافة عنوان"),
        "addCoupon": MessageLookupByLibrary.simpleMessage("إضافة كوبون"),
        "addToCart": MessageLookupByLibrary.simpleMessage("إضافة الى السلة"),
        "add_address_subtitle": MessageLookupByLibrary.simpleMessage(
            "يمكنك إضافة عنوانك مباشرةً لإستخدامة في عملية الشراء بسهولة."),
        "add_an_ad": MessageLookupByLibrary.simpleMessage("إضافة اعلان"),
        "add_service": MessageLookupByLibrary.simpleMessage("إضافة خدمة"),
        "addedSuccessfully":
            MessageLookupByLibrary.simpleMessage("تمت الإضافة بنجاح"),
        "addedToCart":
            MessageLookupByLibrary.simpleMessage("تمت اضافة المنتج الى السلة."),
        "address": MessageLookupByLibrary.simpleMessage("العنوان"),
        "addressDetails":
            MessageLookupByLibrary.simpleMessage("العنوان بالتفصيل"),
        "addressName": MessageLookupByLibrary.simpleMessage("اسم العنوان:"),
        "address_name_required":
            MessageLookupByLibrary.simpleMessage("يرجى ادخال الاسم."),
        "address_tour": MessageLookupByLibrary.simpleMessage(
            "هنا يمكنك إضافة و تعديل عناوينك الخاصة."),
        "addresses": MessageLookupByLibrary.simpleMessage("العناوين"),
        "addser": MessageLookupByLibrary.simpleMessage("أضافة خدمة"),
        "advertise_with_us": MessageLookupByLibrary.simpleMessage("اعلن لدينا"),
        "all": MessageLookupByLibrary.simpleMessage("الكل"),
        "all_services": MessageLookupByLibrary.simpleMessage("جميع الخدمات"),
        "allcity": MessageLookupByLibrary.simpleMessage("كل المناطق"),
        "allstate": MessageLookupByLibrary.simpleMessage("كل المدن"),
        "already_have_a_service": MessageLookupByLibrary.simpleMessage(
            "انت لديك خدمة أساسا, لا يمكن إضافة أكثر من مرة."),
        "and": MessageLookupByLibrary.simpleMessage(" و "),
        "app": MessageLookupByLibrary.simpleMessage("التطبيق"),
        "appNotifications":
            MessageLookupByLibrary.simpleMessage("إشعارات التطبيق"),
        "appVersion": MessageLookupByLibrary.simpleMessage("رقم الإصدار"),
        "apple": MessageLookupByLibrary.simpleMessage("آبل"),
        "applyFilter": MessageLookupByLibrary.simpleMessage("تطبيق الفلتر"),
        "arabic": MessageLookupByLibrary.simpleMessage("العربية"),
        "are_you_sure": MessageLookupByLibrary.simpleMessage("هل أنت متأكد."),
        "back": MessageLookupByLibrary.simpleMessage("رجوع"),
        "blogs": MessageLookupByLibrary.simpleMessage("المدونة"),
        "book_now": MessageLookupByLibrary.simpleMessage("احجز الآن"),
        "brand": MessageLookupByLibrary.simpleMessage("الماركة"),
        "breaks": MessageLookupByLibrary.simpleMessage("استراحات"),
        "buyNow": MessageLookupByLibrary.simpleMessage("اشتري الأن"),
        "by_login": MessageLookupByLibrary.simpleMessage(
            "سجَل دخول لتستفيد من مزايا التطبيق"),
        "by_login_dialoge": MessageLookupByLibrary.simpleMessage(
            "عند تسجيل الدخول ستتمكن من الإستفادة من مزايا التطبيق ومشاهدة العروض الخاصة للمستخدمين."),
        "cancel": MessageLookupByLibrary.simpleMessage("إلغاء"),
        "cancelOrder": MessageLookupByLibrary.simpleMessage("إلغاء الطلب"),
        "cancel_order": MessageLookupByLibrary.simpleMessage("الغاء الطلب ؟"),
        "canceled": MessageLookupByLibrary.simpleMessage("ملغي"),
        "cardName": MessageLookupByLibrary.simpleMessage("اسم البطاقة"),
        "cardNumber": MessageLookupByLibrary.simpleMessage("رقم البطاقة"),
        "cart": MessageLookupByLibrary.simpleMessage("السلة"),
        "cartEmpty": MessageLookupByLibrary.simpleMessage("السلة فارغة!"),
        "cart_empty_subtitle": MessageLookupByLibrary.simpleMessage(
            "لماذا لا تذهبين وتملئينه بأدوات التجميل الرائعة؟ "),
        "cart_tour": MessageLookupByLibrary.simpleMessage(
            "هنا تجد الخدمات التي تمت إضافتها للسلة لمتابعة عملية الشراء."),
        "categories": MessageLookupByLibrary.simpleMessage("التصنيفات"),
        "categories_tour": MessageLookupByLibrary.simpleMessage(
            "من هنا يمكنك الدخول إلى التصنيفات الرئيسية."),
        "category": MessageLookupByLibrary.simpleMessage("التصنيف"),
        "change_address": MessageLookupByLibrary.simpleMessage("تغيير العنوان"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("إعادة تعيين كلمة المرور"),
        "change_password_message": MessageLookupByLibrary.simpleMessage(
            "أدخل عنوان البريد الإلكتروني الذي استخدمته لإنشاء حسابك وسنرسل لك رابطًا عبر البريد الإلكتروني لإعادة تعيين  كلمة المرور الخاصة بك"),
        "change_points_with_products":
            MessageLookupByLibrary.simpleMessage("إستبدال النقاط"),
        "chat_via_phone":
            MessageLookupByLibrary.simpleMessage("التواصل عبر الهاتف"),
        "chat_via_whatsapp":
            MessageLookupByLibrary.simpleMessage("التواصل عبر الواتساب"),
        "checkoutCart": MessageLookupByLibrary.simpleMessage("شراء السلة"),
        "checkoutSuccess":
            MessageLookupByLibrary.simpleMessage("تمت عملية الشراء بنجاح"),
        "checkoutSuccess_subtitle":
            MessageLookupByLibrary.simpleMessage("شكرا لك. تم وضع طلبك بنجاح."),
        "checkout_tour": MessageLookupByLibrary.simpleMessage(
            "لمتابعة عملية الشراء عن طريق التطبيق وباستخدام معلومات الحساب الشخصي (يتطلب تسجيل دخول)"),
        "checkout_whatsapp_tour_subtitle": MessageLookupByLibrary.simpleMessage(
            " عن طريق ارسال رسالة واتساب للبائع تحوي معلومات السلة دون الحاجة لتسجيل الدخول"),
        "checkout_whatsapp_tour_title":
            MessageLookupByLibrary.simpleMessage("شراء السلة عن طريق الواتساب"),
        "choose_a_package": MessageLookupByLibrary.simpleMessage("اختر باقة"),
        "choose_service": MessageLookupByLibrary.simpleMessage("اختر خدمة"),
        "choose_the_package":
            MessageLookupByLibrary.simpleMessage("اختيار باقة"),
        "city": MessageLookupByLibrary.simpleMessage("مدينة"),
        "clean": MessageLookupByLibrary.simpleMessage("تنظيف"),
        "click_twice":
            MessageLookupByLibrary.simpleMessage("اضغط مرتين للخروج"),
        "code": MessageLookupByLibrary.simpleMessage("الكود"),
        "comment": MessageLookupByLibrary.simpleMessage("التعليق"),
        "commonSearch": MessageLookupByLibrary.simpleMessage("البحث الشائع"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("تأكيد كلمة المرور"),
        "continueAsGuest": MessageLookupByLibrary.simpleMessage("الدخول كضيف"),
        "continueShopping":
            MessageLookupByLibrary.simpleMessage("أكمل التسوّق"),
        "continuee": MessageLookupByLibrary.simpleMessage("متابعة"),
        "country": MessageLookupByLibrary.simpleMessage("دولة"),
        "coupon": MessageLookupByLibrary.simpleMessage("كوبون"),
        "coupon_accepted":
            MessageLookupByLibrary.simpleMessage("تم إستخدام الكوبون."),
        "coupon_rejected":
            MessageLookupByLibrary.simpleMessage("الكوبون غير صالح."),
        "create_account": MessageLookupByLibrary.simpleMessage("انشاء حساب "),
        "creditCard": MessageLookupByLibrary.simpleMessage("بطاقة ائتمانية"),
        "currency": MessageLookupByLibrary.simpleMessage("العملة"),
        "current_points_balance":
            MessageLookupByLibrary.simpleMessage("رصيد النقاط الحالي"),
        "danger": MessageLookupByLibrary.simpleMessage("تحذير"),
        "dateOfOrder": MessageLookupByLibrary.simpleMessage("تاريخ الطلب : "),
        "deactivated": MessageLookupByLibrary.simpleMessage("غير مفعل"),
        "delete": MessageLookupByLibrary.simpleMessage("مسح"),
        "deleteAc": MessageLookupByLibrary.simpleMessage("حذف الحساب"),
        "deleteAcDe": MessageLookupByLibrary.simpleMessage(
            "هل انت متاكد انك تريد حذف حسابك ؟"),
        "delete_all": MessageLookupByLibrary.simpleMessage("حذف الكل"),
        "delete_tour": MessageLookupByLibrary.simpleMessage(
            "لحذف المنتج من السلة, قم بسحب المنتج  "),
        "deliver_address":
            MessageLookupByLibrary.simpleMessage("عنوان الإستلام"),
        "deliveredOrders":
            MessageLookupByLibrary.simpleMessage("طلباتي السابقة"),
        "descriptionRequired":
            MessageLookupByLibrary.simpleMessage("الرجاء إدخال الوصف"),
        "details": MessageLookupByLibrary.simpleMessage("التفاصيل"),
        "discover": MessageLookupByLibrary.simpleMessage("اكتشف"),
        "do_you_need_help": MessageLookupByLibrary.simpleMessage(
            "بحاجة إلى بعض المساعدة اليوم؟"),
        "dont_have_account":
            MessageLookupByLibrary.simpleMessage("ليس لديك حساب ؟"),
        "dont_have_points_enogh":
            MessageLookupByLibrary.simpleMessage("ليس لديك نقاط كافية"),
        "e_mail": MessageLookupByLibrary.simpleMessage("البريد الالكتروني"),
        "edit": MessageLookupByLibrary.simpleMessage("تعديل"),
        "editProfile":
            MessageLookupByLibrary.simpleMessage("تعديل الملف الشخصي"),
        "edit_personal_profile":
            MessageLookupByLibrary.simpleMessage("تعديل الحساب الشخصي"),
        "edit_profile_tour": MessageLookupByLibrary.simpleMessage(
            "لتعديل المعلومات الشخصية اضغط هنا."),
        "editser": MessageLookupByLibrary.simpleMessage("تعديل الخدمة"),
        "emailOrPasswordWrong": MessageLookupByLibrary.simpleMessage(
            "البريد الإلكتروني أو كلمة المرور غير صحيحة"),
        "emailRequired": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال البريد الإلكتروني"),
        "emailValidator": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال بريد الكتروني صالح"),
        "endtime": MessageLookupByLibrary.simpleMessage(
            "تاريخ انتهاء الاشتراك بالباقة :"),
        "english": MessageLookupByLibrary.simpleMessage("الإنجليزية"),
        "enterCouponHere":
            MessageLookupByLibrary.simpleMessage("أدخل الكود هنا"),
        "enter_code": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال الرمز المرسل للبريد الالكتروني"),
        "expiryDate": MessageLookupByLibrary.simpleMessage("تاريخ الإنتهاء"),
        "facebook": MessageLookupByLibrary.simpleMessage("فيسبوك"),
        "favorite_tour": MessageLookupByLibrary.simpleMessage(
            "هنا تجد الخدمات التي قمت بإضافتها للمفضلة."),
        "featured_products":
            MessageLookupByLibrary.simpleMessage("منتجات جديدة"),
        "features": MessageLookupByLibrary.simpleMessage("المميزات"),
        "fill_login_form": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال التفاصيل الخاصة بك للتسجيل وتسجيل الدخول."),
        "fill_phone_number": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال رقمك. سنرسل رمزًا إلى هاتفك لإعادة تعيين كلمة المرور الخاصة بك."),
        "fill_signup_form": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال التفاصيل الخاصة بك للتسجيل وإنشاء حساب. "),
        "fill_singup_form": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال التفاصيل الخاصة بك للتسجيل وإنشاء حساب."),
        "filter": MessageLookupByLibrary.simpleMessage("الفلتر"),
        "fix": MessageLookupByLibrary.simpleMessage("تصليح"),
        "food": MessageLookupByLibrary.simpleMessage("طعام"),
        "food_prepare": MessageLookupByLibrary.simpleMessage("تحضير الطعام"),
        "for_general_services":
            MessageLookupByLibrary.simpleMessage("للخدمات العامة"),
        "forget_password":
            MessageLookupByLibrary.simpleMessage("هل نسيت كلمة المرور ؟"),
        "fullName": MessageLookupByLibrary.simpleMessage("الاسم الكامل"),
        "google": MessageLookupByLibrary.simpleMessage("غوغل"),
        "great": MessageLookupByLibrary.simpleMessage("رائع"),
        "haveAnAccount": MessageLookupByLibrary.simpleMessage("لديك حساب ؟ "),
        "hello": MessageLookupByLibrary.simpleMessage("اهلا"),
        "here": MessageLookupByLibrary.simpleMessage("هنا"),
        "high_price": MessageLookupByLibrary.simpleMessage("الأعلى سعراً"),
        "home": MessageLookupByLibrary.simpleMessage("الرئيسية"),
        "home_subtitle_tour": MessageLookupByLibrary.simpleMessage(
            "هنا تجد الخدمات المميزة و أحدث العروض."),
        "home_title_tour":
            MessageLookupByLibrary.simpleMessage("الصفحة الرئيسية"),
        "house": MessageLookupByLibrary.simpleMessage("منزل"),
        "house_arrange": MessageLookupByLibrary.simpleMessage("ترتيب المنزل"),
        "house_clean": MessageLookupByLibrary.simpleMessage("تنظيف المنزل"),
        "info": MessageLookupByLibrary.simpleMessage("معلومة"),
        "ingredients": MessageLookupByLibrary.simpleMessage("المكونات"),
        "join_us": MessageLookupByLibrary.simpleMessage("انضم لنا"),
        "language": MessageLookupByLibrary.simpleMessage("اللغة"),
        "leaveAComment":
            MessageLookupByLibrary.simpleMessage("اترك تعليقك هنا"),
        "less_price": MessageLookupByLibrary.simpleMessage("الأقل سعراً"),
        "login": MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "mobileValidator":
            MessageLookupByLibrary.simpleMessage("الرجاء التأكد من رقم الهاتف"),
        "mustAddAddress":
            MessageLookupByLibrary.simpleMessage("الرجاء إضافة عنوان."),
        "myFavorites": MessageLookupByLibrary.simpleMessage("المفضلة"),
        "my_account": MessageLookupByLibrary.simpleMessage("حسابي"),
        "my_addresses": MessageLookupByLibrary.simpleMessage("عناويني"),
        "my_points": MessageLookupByLibrary.simpleMessage("نقاطي"),
        "my_registered_addresses":
            MessageLookupByLibrary.simpleMessage("عناويني المسجلة"),
        "mypackage": MessageLookupByLibrary.simpleMessage("باقتي"),
        "name": MessageLookupByLibrary.simpleMessage("الاسم"),
        "nameRequired":
            MessageLookupByLibrary.simpleMessage("الرجاء إدخال الاسم الكامل"),
        "new_password":
            MessageLookupByLibrary.simpleMessage("كلمة المرور الجديدة"),
        "new_user": MessageLookupByLibrary.simpleMessage("مستخدم جديد؟"),
        "newest": MessageLookupByLibrary.simpleMessage("الأحدث"),
        "next": MessageLookupByLibrary.simpleMessage("التالي"),
        "no": MessageLookupByLibrary.simpleMessage("لا"),
        "noAddresses": MessageLookupByLibrary.simpleMessage("لا يوجد عناوين"),
        "noInternet":
            MessageLookupByLibrary.simpleMessage("لا يوجد اتصال بالانترنت"),
        "noReviews":
            MessageLookupByLibrary.simpleMessage("لا يوجد تقييمات بعد"),
        "no_favorite": MessageLookupByLibrary.simpleMessage("لا يوجد مفضلة"),
        "no_favorite_subtitle": MessageLookupByLibrary.simpleMessage(
            "يمكنك رؤية جميع الخدمات المفضلة لديك هنا من مكان واحد."),
        "no_notifications":
            MessageLookupByLibrary.simpleMessage("لا يوجد اشعارات"),
        "no_notifications_subtitle": MessageLookupByLibrary.simpleMessage(
            "سيتم إظهار الإشعارات الخاصة بك بمجرد حصول أي حدث جديد."),
        "no_result": MessageLookupByLibrary.simpleMessage("لا يوجد نتائج"),
        "no_result_subtitle": MessageLookupByLibrary.simpleMessage(
            "سيتم إظهار الخدمات و المحلات التجارية عند توفرهم مباشرةً."),
        "no_search_subtitle": MessageLookupByLibrary.simpleMessage(
            "يمكنك تجربة البحث بكلمات مختلفة عن الاصناف التي تبحث عنها."),
        "not_loggedin_content": MessageLookupByLibrary.simpleMessage(
            "الرجاء تسجيل الدخول للمتابعة ."),
        "not_loggedin_title":
            MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "notes": MessageLookupByLibrary.simpleMessage("ملاحظات"),
        "notifications": MessageLookupByLibrary.simpleMessage("الإشعارات"),
        "numberOfProducts":
            MessageLookupByLibrary.simpleMessage("عدد الخدمات : "),
        "offersNotifications":
            MessageLookupByLibrary.simpleMessage("إشعارات العروض"),
        "oldest": MessageLookupByLibrary.simpleMessage("الأقدم"),
        "onDelivery": MessageLookupByLibrary.simpleMessage("الدفع عند الباب"),
        "options": MessageLookupByLibrary.simpleMessage("خيارات"),
        "or_create_account_with":
            MessageLookupByLibrary.simpleMessage("او انشئ حساب عن طريق"),
        "or_login_with":
            MessageLookupByLibrary.simpleMessage("او سجّل دخول عن طريق"),
        "orderHistory": MessageLookupByLibrary.simpleMessage("طلباتي"),
        "orderNumber": MessageLookupByLibrary.simpleMessage("رقم السلة :"),
        "orderPrice": MessageLookupByLibrary.simpleMessage("سعر السلة : "),
        "order_by": MessageLookupByLibrary.simpleMessage("طلب عبر "),
        "order_date": MessageLookupByLibrary.simpleMessage("تاريخ الطلب"),
        "order_details": MessageLookupByLibrary.simpleMessage("تفاصيل الطلب"),
        "order_tour": MessageLookupByLibrary.simpleMessage(
            "هنا يمكنك تتبع حالة الطلبات الخاصة بك ."),
        "packages": MessageLookupByLibrary.simpleMessage("الباقات"),
        "packages_name": MessageLookupByLibrary.simpleMessage("اسم الباقة"),
        "password": MessageLookupByLibrary.simpleMessage("كلمة المرور"),
        "passwordConfirm":
            MessageLookupByLibrary.simpleMessage("كلمة المرور غير متطابقة"),
        "passwordConfirmValidator":
            MessageLookupByLibrary.simpleMessage("كلمة المرور غير متطابقة."),
        "passwordValidator": MessageLookupByLibrary.simpleMessage(
            "يجب أن تحوي 8 محارف على الأقل"),
        "pay": MessageLookupByLibrary.simpleMessage("ادفع"),
        "paymentMethod": MessageLookupByLibrary.simpleMessage("طريقة الدفع"),
        "payment_process": MessageLookupByLibrary.simpleMessage(
            "حدثت مشكلة اثناء عملية الدفع"),
        "personalInfo":
            MessageLookupByLibrary.simpleMessage("المعلومات الشخصية"),
        "personal_profile":
            MessageLookupByLibrary.simpleMessage("الحساب الشخصي"),
        "pest_control": MessageLookupByLibrary.simpleMessage("مكافحة الحشرات"),
        "phone_number": MessageLookupByLibrary.simpleMessage("رقم الهاتف"),
        "photo_gallery": MessageLookupByLibrary.simpleMessage("معرض الصور"),
        "please_sign_in":
            MessageLookupByLibrary.simpleMessage("الرجاء تسجيل الدخول"),
        "please_sign_up":
            MessageLookupByLibrary.simpleMessage("الرجاء انشاء حساب"),
        "please_subscribe_first": MessageLookupByLibrary.simpleMessage(
            "ليس لديك اشتراك نشط ، يرجى الاشتراك أولا."),
        "point": MessageLookupByLibrary.simpleMessage("نقطة"),
        "premium": MessageLookupByLibrary.simpleMessage("عضوية مميزة"),
        "price": MessageLookupByLibrary.simpleMessage("السعر"),
        "privcy": MessageLookupByLibrary.simpleMessage("سياسة الخصوصية"),
        "product_details": MessageLookupByLibrary.simpleMessage("تفاصيل الطلب"),
        "products": MessageLookupByLibrary.simpleMessage("منتجات"),
        "productsList": MessageLookupByLibrary.simpleMessage("قائمة الخدمات"),
        "profession": MessageLookupByLibrary.simpleMessage("المهنة"),
        "profile": MessageLookupByLibrary.simpleMessage("الملف الشخصي"),
        "profileImage": MessageLookupByLibrary.simpleMessage("الصورة الشخصية"),
        "profile_tour": MessageLookupByLibrary.simpleMessage(
            "حيث تجد المعلومات الشخصية و الطلبات السابقة لديك."),
        "promote_account": MessageLookupByLibrary.simpleMessage("ترقية الحساب"),
        "public_transport": MessageLookupByLibrary.simpleMessage("نقل عام"),
        "put_your_number":
            MessageLookupByLibrary.simpleMessage("رقم هاتفك هنا"),
        "rate": MessageLookupByLibrary.simpleMessage("التقيم"),
        "rateIt": MessageLookupByLibrary.simpleMessage("تقييم"),
        "rateOrder": MessageLookupByLibrary.simpleMessage("قيم الطلب"),
        "rates": MessageLookupByLibrary.simpleMessage("التقيمات"),
        "readyToGo": MessageLookupByLibrary.simpleMessage("أنت جاهز للإنطلاق"),
        "recent_products": MessageLookupByLibrary.simpleMessage("أحدث الخدمات"),
        "register_package": MessageLookupByLibrary.simpleMessage("تسجيل باقة"),
        "related_products":
            MessageLookupByLibrary.simpleMessage("منتجات ذات صلة"),
        "reportDescription": MessageLookupByLibrary.simpleMessage("شرح"),
        "report_account":
            MessageLookupByLibrary.simpleMessage("التبليغ عن الحساب"),
        "reportedSuccessfully":
            MessageLookupByLibrary.simpleMessage("تم التبليغ بنجاح"),
        "representative": MessageLookupByLibrary.simpleMessage("مندوب"),
        "request_service": MessageLookupByLibrary.simpleMessage("اطلب خدمة"),
        "reviews": MessageLookupByLibrary.simpleMessage("تقييمات"),
        "sale": MessageLookupByLibrary.simpleMessage("الخصم"),
        "salesNotifications":
            MessageLookupByLibrary.simpleMessage("إشعارات التخفيضات"),
        "searchHere": MessageLookupByLibrary.simpleMessage("ابحث هنا"),
        "searchHistory": MessageLookupByLibrary.simpleMessage("تاريخ البحث"),
        "search_tour": MessageLookupByLibrary.simpleMessage(
            "للبحث عن أي منتج مع إمكانية الفلترة حسب (السعر,التصنيف,التقييم..)."),
        "security_code": MessageLookupByLibrary.simpleMessage("رمز الأمن"),
        "see_all": MessageLookupByLibrary.simpleMessage("مشاهدة الكل"),
        "selectOptions": MessageLookupByLibrary.simpleMessage(
            "الرجاء تحديد خيارات المنتج قبل إضافته للسلة."),
        "select_country": MessageLookupByLibrary.simpleMessage("إختيار دولة"),
        "send_code": MessageLookupByLibrary.simpleMessage("ارسل الرمز"),
        "send_complaint": MessageLookupByLibrary.simpleMessage("ارسال الشكوى"),
        "send_to": MessageLookupByLibrary.simpleMessage("إرسال الى "),
        "serviceNameRequired":
            MessageLookupByLibrary.simpleMessage("الرجاء إدخال اسم الخدمة"),
        "service_description":
            MessageLookupByLibrary.simpleMessage("وصف الخدمة"),
        "service_name": MessageLookupByLibrary.simpleMessage("اسم الخدمة"),
        "service_photos": MessageLookupByLibrary.simpleMessage("صور الخدمة"),
        "service_pr": MessageLookupByLibrary.simpleMessage("كمزود خدمة"),
        "service_providers":
            MessageLookupByLibrary.simpleMessage("مقدمي الخدمة"),
        "services": MessageLookupByLibrary.simpleMessage("الخدمات"),
        "services_description":
            MessageLookupByLibrary.simpleMessage("وصف الخدمات"),
        "settings": MessageLookupByLibrary.simpleMessage("الضبط"),
        "sex": MessageLookupByLibrary.simpleMessage("الجنس"),
        "shareApp": MessageLookupByLibrary.simpleMessage("مشاركة التطبيق"),
        "share_to_get_points": MessageLookupByLibrary.simpleMessage(
            "شارك التطبيق مع أصدقائك لتحصل على المزيد من النقاط"),
        "shop_now": MessageLookupByLibrary.simpleMessage("تسوق الآن"),
        "show_info": MessageLookupByLibrary.simpleMessage("عرض تفصيل"),
        "show_order": MessageLookupByLibrary.simpleMessage("عرض السلة"),
        "signOut": MessageLookupByLibrary.simpleMessage("تسجيل الخروج"),
        "sign_in_with":
            MessageLookupByLibrary.simpleMessage("  أو سجل الدخول عن طريق  "),
        "sign_up_with":
            MessageLookupByLibrary.simpleMessage("  أو أنشئ حساب عن طريق  "),
        "sign_with_apple":
            MessageLookupByLibrary.simpleMessage("تسجيل الدخول باستخدام Apple"),
        "singup": MessageLookupByLibrary.simpleMessage("أنشئ حساب"),
        "singupDe": MessageLookupByLibrary.simpleMessage(
            "هل انت متاكد انك تريد تسجيل الخروج ؟"),
        "skip": MessageLookupByLibrary.simpleMessage("تخطي"),
        "sort_by": MessageLookupByLibrary.simpleMessage("ترتيب حسب"),
        "special_service_providers":
            MessageLookupByLibrary.simpleMessage("مقدمي الخدمات المميزين"),
        "specialized_in": MessageLookupByLibrary.simpleMessage("متخصصة في"),
        "startNow": MessageLookupByLibrary.simpleMessage("ابدأ الأن"),
        "starttime": MessageLookupByLibrary.simpleMessage(
            "تاريخ بدايةالاشتراك بالباقة :"),
        "state": MessageLookupByLibrary.simpleMessage("محافظة"),
        "submit": MessageLookupByLibrary.simpleMessage("تطبيق"),
        "subscribedSuccessfully":
            MessageLookupByLibrary.simpleMessage("تم الترقية بنجاح"),
        "subscription_request": MessageLookupByLibrary.simpleMessage(
            "لديك بالفعل طلب اشتراك معلق."),
        "success": MessageLookupByLibrary.simpleMessage("نجاح"),
        "tajraa_points": MessageLookupByLibrary.simpleMessage("نقاط تجرة "),
        "tajraa_points_desc": MessageLookupByLibrary.simpleMessage(
            "تستخدم في تخفيض التكلفة على مشترياتك, حيث يمكنك استبدالهم بمنتجات معينة."),
        "technicalSupport": MessageLookupByLibrary.simpleMessage("الدعم الفني"),
        "thanksForTime": MessageLookupByLibrary.simpleMessage(
            "نشكرك على الوقت الذي أمضيته في إنشاء حسابك. الآن هذا هو الجزء الممتع ، دعنا نستكشف التطبيق."),
        "to_tajraa": MessageLookupByLibrary.simpleMessage("في تطبيق تجرة"),
        "total": MessageLookupByLibrary.simpleMessage("الإجمالي"),
        "totalNumber": MessageLookupByLibrary.simpleMessage("العدد الكلي"),
        "total_saving": MessageLookupByLibrary.simpleMessage(
            "كمية التوفير من إستخدام النقاط"),
        "trackOrder": MessageLookupByLibrary.simpleMessage("تعقب الطلب"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("حاول مجدداً"),
        "turkish": MessageLookupByLibrary.simpleMessage("التركية"),
        "types_of_services":
            MessageLookupByLibrary.simpleMessage("انواع الخدمات"),
        "upload_image": MessageLookupByLibrary.simpleMessage("ارفع صورة"),
        "use_coupon": MessageLookupByLibrary.simpleMessage("تطبيق"),
        "used_points": MessageLookupByLibrary.simpleMessage("النقاط المستخدمة"),
        "version_number": MessageLookupByLibrary.simpleMessage("رقم النسخة"),
        "waitings": MessageLookupByLibrary.simpleMessage("قيد الإنتظار"),
        "warning": MessageLookupByLibrary.simpleMessage("تنبيه"),
        "washing": MessageLookupByLibrary.simpleMessage("غسيل"),
        "watch": MessageLookupByLibrary.simpleMessage("مشاهدة"),
        "welcome": MessageLookupByLibrary.simpleMessage("أهلا بك"),
        "whatsapp": MessageLookupByLibrary.simpleMessage("واتساب"),
        "write_description_here":
            MessageLookupByLibrary.simpleMessage("اكتب الشرح هنا"),
        "yes": MessageLookupByLibrary.simpleMessage("نعم"),
        "you_can_now_add_ads": MessageLookupByLibrary.simpleMessage(
            "رائع يمكنك الآن البدء في اضافة إعلان"),
        "you_can_report_an_account": MessageLookupByLibrary.simpleMessage(
            "يمكنك التبليغ عن الحساب اذا قدم لك خدمة سيئة")
      };
}
