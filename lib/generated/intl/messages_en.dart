// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Back_to":
            MessageLookupByLibrary.simpleMessage("Back to the application"),
        "Please_wait": MessageLookupByLibrary.simpleMessage(
            "Your request is still being processed. Please wait. Thank you."),
        "activated": MessageLookupByLibrary.simpleMessage("Activated"),
        "addAddress": MessageLookupByLibrary.simpleMessage("Add address"),
        "addCoupon": MessageLookupByLibrary.simpleMessage("Add a coupon"),
        "addToCart": MessageLookupByLibrary.simpleMessage("Add to cart"),
        "add_address_subtitle": MessageLookupByLibrary.simpleMessage(
            "You can add your address directly to be used in the purchase process easily."),
        "add_an_ad": MessageLookupByLibrary.simpleMessage("Add an ad"),
        "add_service": MessageLookupByLibrary.simpleMessage("Add service"),
        "addedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Added successfully"),
        "addedToCart": MessageLookupByLibrary.simpleMessage(
            "The product has been added to the cart."),
        "address": MessageLookupByLibrary.simpleMessage("Address"),
        "addressDetails":
            MessageLookupByLibrary.simpleMessage("Address in detail"),
        "addressName": MessageLookupByLibrary.simpleMessage("Address name:"),
        "address_name_required":
            MessageLookupByLibrary.simpleMessage("name required."),
        "address_tour": MessageLookupByLibrary.simpleMessage(
            "Here you can add and edit your own addresses."),
        "addresses": MessageLookupByLibrary.simpleMessage("Addresses"),
        "addser": MessageLookupByLibrary.simpleMessage("add service"),
        "advertise_with_us":
            MessageLookupByLibrary.simpleMessage("Advertise with us"),
        "all": MessageLookupByLibrary.simpleMessage("All"),
        "all_services": MessageLookupByLibrary.simpleMessage("All Services"),
        "allcity": MessageLookupByLibrary.simpleMessage("All city"),
        "allstate": MessageLookupByLibrary.simpleMessage("All state"),
        "already_have_a_service": MessageLookupByLibrary.simpleMessage(
            "You already have a service, you cannot post more than one."),
        "and": MessageLookupByLibrary.simpleMessage(" And "),
        "app": MessageLookupByLibrary.simpleMessage("Tajraa"),
        "appNotifications":
            MessageLookupByLibrary.simpleMessage("App notifications"),
        "appVersion": MessageLookupByLibrary.simpleMessage("Version number"),
        "apple": MessageLookupByLibrary.simpleMessage("Apple"),
        "applyFilter": MessageLookupByLibrary.simpleMessage("Apply filter"),
        "arabic": MessageLookupByLibrary.simpleMessage("Arabic"),
        "are_you_sure": MessageLookupByLibrary.simpleMessage("Are you sure."),
        "back": MessageLookupByLibrary.simpleMessage("Back"),
        "blogs": MessageLookupByLibrary.simpleMessage("Blog"),
        "book_now": MessageLookupByLibrary.simpleMessage("Book now"),
        "brand": MessageLookupByLibrary.simpleMessage("Brand"),
        "breaks": MessageLookupByLibrary.simpleMessage("Breaks"),
        "buyNow": MessageLookupByLibrary.simpleMessage("buy now"),
        "by_login": MessageLookupByLibrary.simpleMessage(
            "By login you can enjoy all Tajraa\'s features."),
        "by_login_dialoge": MessageLookupByLibrary.simpleMessage(
            "By login you can use all app features."),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "cancelOrder": MessageLookupByLibrary.simpleMessage("Cancel order"),
        "cancel_order":
            MessageLookupByLibrary.simpleMessage("Cancel the order?"),
        "canceled": MessageLookupByLibrary.simpleMessage("Cancelled"),
        "cardName":
            MessageLookupByLibrary.simpleMessage("The name of the card"),
        "cardNumber": MessageLookupByLibrary.simpleMessage("Card number"),
        "cart": MessageLookupByLibrary.simpleMessage("Cart"),
        "cartEmpty": MessageLookupByLibrary.simpleMessage("The cart is empty!"),
        "cart_empty_subtitle": MessageLookupByLibrary.simpleMessage(
            "Why don\'t you go and fill it with cool cosmetics?"),
        "cart_tour": MessageLookupByLibrary.simpleMessage(
            "Here you will find the products that have been added to the cart to continue the purchase process."),
        "categories": MessageLookupByLibrary.simpleMessage("Categories"),
        "categories_tour": MessageLookupByLibrary.simpleMessage(
            "From here you can access the main categories."),
        "category": MessageLookupByLibrary.simpleMessage("Category"),
        "change_address":
            MessageLookupByLibrary.simpleMessage("Change address"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("Reset password"),
        "change_password_message": MessageLookupByLibrary.simpleMessage(
            "Enter the email address you used to create your account and we\'ll email you a link to reset your password"),
        "change_points_with_products":
            MessageLookupByLibrary.simpleMessage("Exchange Points"),
        "chat_via_phone":
            MessageLookupByLibrary.simpleMessage("Chat via phone"),
        "chat_via_whatsapp":
            MessageLookupByLibrary.simpleMessage("Chat via whatsapp"),
        "checkoutCart": MessageLookupByLibrary.simpleMessage("Purchase Cart"),
        "checkoutSuccess": MessageLookupByLibrary.simpleMessage(
            "Purchase completed successfully"),
        "checkoutSuccess_subtitle": MessageLookupByLibrary.simpleMessage(
            "Thank you. Your request has been placed successfully."),
        "checkout_tour": MessageLookupByLibrary.simpleMessage(
            "To proceed with the purchase via the app and using personal account information (login required)"),
        "checkout_whatsapp_tour_subtitle": MessageLookupByLibrary.simpleMessage(
            "By sending a WhatsApp message to the seller containing the basket information without the need to log in"),
        "checkout_whatsapp_tour_title":
            MessageLookupByLibrary.simpleMessage("Purchase cart via WhatsApp"),
        "choose_a_package":
            MessageLookupByLibrary.simpleMessage("Choose a package"),
        "choose_service":
            MessageLookupByLibrary.simpleMessage("Choose service"),
        "choose_the_package":
            MessageLookupByLibrary.simpleMessage("Choose the package"),
        "city": MessageLookupByLibrary.simpleMessage("City:"),
        "clean": MessageLookupByLibrary.simpleMessage("Clean"),
        "click_twice":
            MessageLookupByLibrary.simpleMessage("Double click to exit"),
        "code": MessageLookupByLibrary.simpleMessage("Code"),
        "comment": MessageLookupByLibrary.simpleMessage("Comment"),
        "commonSearch": MessageLookupByLibrary.simpleMessage("Common search"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("Confirm password"),
        "continueAsGuest":
            MessageLookupByLibrary.simpleMessage("Login as guest"),
        "continueShopping":
            MessageLookupByLibrary.simpleMessage("Continue shopping"),
        "continuee": MessageLookupByLibrary.simpleMessage("Continue"),
        "country": MessageLookupByLibrary.simpleMessage("Country"),
        "coupon": MessageLookupByLibrary.simpleMessage("Coupon"),
        "coupon_accepted":
            MessageLookupByLibrary.simpleMessage("Coupon Applied."),
        "coupon_rejected":
            MessageLookupByLibrary.simpleMessage("Coupon is not valid."),
        "create_account":
            MessageLookupByLibrary.simpleMessage("Create an account"),
        "creditCard": MessageLookupByLibrary.simpleMessage("Credit card"),
        "currency": MessageLookupByLibrary.simpleMessage("Currency"),
        "current_points_balance":
            MessageLookupByLibrary.simpleMessage("current point balance"),
        "danger": MessageLookupByLibrary.simpleMessage("danger"),
        "dateOfOrder": MessageLookupByLibrary.simpleMessage("DateOfOrder: "),
        "deactivated": MessageLookupByLibrary.simpleMessage("Deactivated"),
        "delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "deleteAc": MessageLookupByLibrary.simpleMessage("Delete Account"),
        "deleteAcDe": MessageLookupByLibrary.simpleMessage(
            "Are you sure you want to delete your account?"),
        "delete_all": MessageLookupByLibrary.simpleMessage("Delete All"),
        "delete_tour": MessageLookupByLibrary.simpleMessage(
            "To delete the product from the cart, Swip the product"),
        "deliver_address":
            MessageLookupByLibrary.simpleMessage("Delivery Address"),
        "deliveredOrders":
            MessageLookupByLibrary.simpleMessage("My previous orders"),
        "descriptionRequired":
            MessageLookupByLibrary.simpleMessage("Please enter description"),
        "details": MessageLookupByLibrary.simpleMessage("Details"),
        "discover": MessageLookupByLibrary.simpleMessage("Discover"),
        "do_you_need_help":
            MessageLookupByLibrary.simpleMessage("Do you need help today?"),
        "dont_have_account":
            MessageLookupByLibrary.simpleMessage("don\'t have an account ?"),
        "dont_have_points_enogh": MessageLookupByLibrary.simpleMessage(
            "you don\'t have enough points"),
        "e_mail": MessageLookupByLibrary.simpleMessage("E-mail"),
        "edit": MessageLookupByLibrary.simpleMessage("edit"),
        "editProfile": MessageLookupByLibrary.simpleMessage("Edit Profile"),
        "edit_personal_profile":
            MessageLookupByLibrary.simpleMessage("Edit Personal Profile"),
        "edit_profile_tour": MessageLookupByLibrary.simpleMessage(
            "To modify personal information, click here."),
        "editser":
            MessageLookupByLibrary.simpleMessage("Modification of the service"),
        "emailOrPasswordWrong": MessageLookupByLibrary.simpleMessage(
            "The password or e-mail isn\'t correct"),
        "emailRequired":
            MessageLookupByLibrary.simpleMessage("Please enter an email"),
        "emailValidator":
            MessageLookupByLibrary.simpleMessage("Please enter a valid email"),
        "endtime": MessageLookupByLibrary.simpleMessage(
            "Package subscription expiry date"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enterCouponHere":
            MessageLookupByLibrary.simpleMessage("Enter the code here"),
        "enter_code": MessageLookupByLibrary.simpleMessage(
            "Please enter the code sent to the e-mail"),
        "expiryDate": MessageLookupByLibrary.simpleMessage("Expiration date"),
        "facebook": MessageLookupByLibrary.simpleMessage("Facebook"),
        "favorite_tour": MessageLookupByLibrary.simpleMessage(
            "Here you will find the products you have added to your favourites."),
        "featured_products":
            MessageLookupByLibrary.simpleMessage("New products"),
        "features": MessageLookupByLibrary.simpleMessage("Features"),
        "fill_login_form": MessageLookupByLibrary.simpleMessage(
            "Please fill all your personal info to register and login."),
        "fill_phone_number": MessageLookupByLibrary.simpleMessage(
            "Please fill your phone number, we will send a code to your mobile to change your password"),
        "fill_signup_form": MessageLookupByLibrary.simpleMessage(
            "Please fill all your personal info to register and sign up."),
        "filter": MessageLookupByLibrary.simpleMessage("Filter"),
        "fix": MessageLookupByLibrary.simpleMessage("Fix"),
        "food": MessageLookupByLibrary.simpleMessage("Food"),
        "food_prepare": MessageLookupByLibrary.simpleMessage("Food preparing"),
        "for_general_services":
            MessageLookupByLibrary.simpleMessage("For general services"),
        "forget_password":
            MessageLookupByLibrary.simpleMessage("Forgot your password?"),
        "fullName": MessageLookupByLibrary.simpleMessage("Full name"),
        "google": MessageLookupByLibrary.simpleMessage("Google"),
        "great": MessageLookupByLibrary.simpleMessage("Great"),
        "haveAnAccount":
            MessageLookupByLibrary.simpleMessage("Have an account?"),
        "hello": MessageLookupByLibrary.simpleMessage("Hello"),
        "here": MessageLookupByLibrary.simpleMessage("Here"),
        "high_price": MessageLookupByLibrary.simpleMessage("Highest price"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "home_subtitle_tour": MessageLookupByLibrary.simpleMessage(
            "Here you will find the featured products and the latest offers."),
        "home_title_tour": MessageLookupByLibrary.simpleMessage("Home page"),
        "house": MessageLookupByLibrary.simpleMessage("House"),
        "house_arrange":
            MessageLookupByLibrary.simpleMessage("House arrangement"),
        "house_clean": MessageLookupByLibrary.simpleMessage("House cleaning"),
        "info": MessageLookupByLibrary.simpleMessage("info"),
        "ingredients": MessageLookupByLibrary.simpleMessage("Ingredients"),
        "join_us": MessageLookupByLibrary.simpleMessage("Join us"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "leaveAComment":
            MessageLookupByLibrary.simpleMessage("Leave your comment here"),
        "less_price": MessageLookupByLibrary.simpleMessage("Less price"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "mobileValidator": MessageLookupByLibrary.simpleMessage(
            "Please verify the phone number"),
        "mustAddAddress":
            MessageLookupByLibrary.simpleMessage("Please add an address."),
        "myFavorites": MessageLookupByLibrary.simpleMessage("Favorites"),
        "my_account": MessageLookupByLibrary.simpleMessage("Profile"),
        "my_addresses": MessageLookupByLibrary.simpleMessage("My Addresses"),
        "my_points": MessageLookupByLibrary.simpleMessage("My Points"),
        "my_registered_addresses":
            MessageLookupByLibrary.simpleMessage("Registered Addresses"),
        "mypackage": MessageLookupByLibrary.simpleMessage("My Package"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "nameRequired":
            MessageLookupByLibrary.simpleMessage("Please enter your full name"),
        "new_password": MessageLookupByLibrary.simpleMessage("New password"),
        "new_user": MessageLookupByLibrary.simpleMessage("New user?"),
        "newest": MessageLookupByLibrary.simpleMessage("Newest"),
        "next": MessageLookupByLibrary.simpleMessage("Next"),
        "no": MessageLookupByLibrary.simpleMessage("No"),
        "noAddresses": MessageLookupByLibrary.simpleMessage("No addresses"),
        "noInternet":
            MessageLookupByLibrary.simpleMessage("No internet connection"),
        "noReviews": MessageLookupByLibrary.simpleMessage("No reviews yet"),
        "no_favorite": MessageLookupByLibrary.simpleMessage("No Favorites"),
        "no_favorite_subtitle": MessageLookupByLibrary.simpleMessage(
            "You can see all your favorite products here from one place."),
        "no_notifications":
            MessageLookupByLibrary.simpleMessage("No Notifications"),
        "no_notifications_subtitle": MessageLookupByLibrary.simpleMessage(
            "Your notifications will appear as soon as any new event occurs."),
        "no_result": MessageLookupByLibrary.simpleMessage("No Results"),
        "no_result_subtitle": MessageLookupByLibrary.simpleMessage(
            "Servers will be shown here immediately when available."),
        "no_search_subtitle": MessageLookupByLibrary.simpleMessage(
            "You can try searching with different words for the Categories you are looking for."),
        "not_loggedin_content":
            MessageLookupByLibrary.simpleMessage("Please login to continue."),
        "not_loggedin_title": MessageLookupByLibrary.simpleMessage("Logged in"),
        "notes": MessageLookupByLibrary.simpleMessage("Notes"),
        "notifications": MessageLookupByLibrary.simpleMessage("Notifications"),
        "numberOfProducts":
            MessageLookupByLibrary.simpleMessage("Number of products : "),
        "offersNotifications":
            MessageLookupByLibrary.simpleMessage("Offers notices"),
        "oldest": MessageLookupByLibrary.simpleMessage("Oldest"),
        "onDelivery": MessageLookupByLibrary.simpleMessage("Pay at the door"),
        "options": MessageLookupByLibrary.simpleMessage("Options"),
        "or_create_account_with":
            MessageLookupByLibrary.simpleMessage("or create account with"),
        "or_login_with": MessageLookupByLibrary.simpleMessage("or login with"),
        "orderHistory": MessageLookupByLibrary.simpleMessage("My orders"),
        "orderNumber": MessageLookupByLibrary.simpleMessage("Cart number:"),
        "orderPrice": MessageLookupByLibrary.simpleMessage("Cart price:"),
        "order_by": MessageLookupByLibrary.simpleMessage("Checkout by "),
        "order_date": MessageLookupByLibrary.simpleMessage("Order Date"),
        "order_details": MessageLookupByLibrary.simpleMessage("Order Details"),
        "order_tour": MessageLookupByLibrary.simpleMessage(
            "Here you can track the status of your orders."),
        "packages": MessageLookupByLibrary.simpleMessage("Packages"),
        "packages_name": MessageLookupByLibrary.simpleMessage("packages name"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "passwordConfirm":
            MessageLookupByLibrary.simpleMessage("Password does not match"),
        "passwordConfirmValidator": MessageLookupByLibrary.simpleMessage(
            "The password does not match."),
        "passwordValidator": MessageLookupByLibrary.simpleMessage(
            "Must contain at least 8 characters"),
        "pay": MessageLookupByLibrary.simpleMessage("Pay"),
        "paymentMethod": MessageLookupByLibrary.simpleMessage("Payment method"),
        "payment_process": MessageLookupByLibrary.simpleMessage(
            "There was a problem during the payment process"),
        "personalInfo":
            MessageLookupByLibrary.simpleMessage("Personal information"),
        "personal_profile":
            MessageLookupByLibrary.simpleMessage("Personal Profile"),
        "pest_control": MessageLookupByLibrary.simpleMessage("Pest Control"),
        "phone_number": MessageLookupByLibrary.simpleMessage("Phone number"),
        "photo_gallery": MessageLookupByLibrary.simpleMessage("Photo gallery"),
        "please_sign_in":
            MessageLookupByLibrary.simpleMessage("Please sign in"),
        "please_sign_up":
            MessageLookupByLibrary.simpleMessage("Please create an account"),
        "please_subscribe_first": MessageLookupByLibrary.simpleMessage(
            "You don\'t have an active subscription, please subscribe first."),
        "point": MessageLookupByLibrary.simpleMessage("Point"),
        "premium": MessageLookupByLibrary.simpleMessage("Premium"),
        "price": MessageLookupByLibrary.simpleMessage("Price"),
        "product_details":
            MessageLookupByLibrary.simpleMessage("Product Details"),
        "products": MessageLookupByLibrary.simpleMessage("Products"),
        "productsList": MessageLookupByLibrary.simpleMessage("Product list"),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "profileImage": MessageLookupByLibrary.simpleMessage("Profile"),
        "profile_tour": MessageLookupByLibrary.simpleMessage(
            "Where you will find your personal information and previous requests."),
        "promote_account":
            MessageLookupByLibrary.simpleMessage("Promote account"),
        "public_transport":
            MessageLookupByLibrary.simpleMessage("Public transport"),
        "put_your_number":
            MessageLookupByLibrary.simpleMessage("Put your number"),
        "rate": MessageLookupByLibrary.simpleMessage("Rate"),
        "rateIt": MessageLookupByLibrary.simpleMessage("Rate"),
        "rateOrder": MessageLookupByLibrary.simpleMessage("Order values"),
        "rates": MessageLookupByLibrary.simpleMessage("Reviews"),
        "readyToGo":
            MessageLookupByLibrary.simpleMessage("You\'re ready to go"),
        "recent_products":
            MessageLookupByLibrary.simpleMessage("Latest products"),
        "register_package":
            MessageLookupByLibrary.simpleMessage("Register package"),
        "related_products":
            MessageLookupByLibrary.simpleMessage("Related Products"),
        "reportDescription":
            MessageLookupByLibrary.simpleMessage("Description"),
        "report_account":
            MessageLookupByLibrary.simpleMessage("Report account"),
        "reportedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Reported successfully"),
        "representative":
            MessageLookupByLibrary.simpleMessage("Representative"),
        "request_service":
            MessageLookupByLibrary.simpleMessage("Request Service"),
        "reviews": MessageLookupByLibrary.simpleMessage("Reviews"),
        "sale": MessageLookupByLibrary.simpleMessage("Discount"),
        "salesNotifications":
            MessageLookupByLibrary.simpleMessage("Sales notifications"),
        "searchHere": MessageLookupByLibrary.simpleMessage("Search here"),
        "searchHistory": MessageLookupByLibrary.simpleMessage("Search history"),
        "search_tour": MessageLookupByLibrary.simpleMessage(
            "To search for any product with the ability to filter by (price, rating, rating..)."),
        "security_code": MessageLookupByLibrary.simpleMessage("Security code"),
        "see_all": MessageLookupByLibrary.simpleMessage("see all"),
        "selectOptions": MessageLookupByLibrary.simpleMessage(
            "Select product options before adding it to the cart."),
        "select_country":
            MessageLookupByLibrary.simpleMessage("select country"),
        "send_code": MessageLookupByLibrary.simpleMessage("Send code"),
        "send_complaint":
            MessageLookupByLibrary.simpleMessage("Send complaint"),
        "send_to": MessageLookupByLibrary.simpleMessage("Send to "),
        "serviceNameRequired":
            MessageLookupByLibrary.simpleMessage("Please enter service name"),
        "service_description":
            MessageLookupByLibrary.simpleMessage("Service description"),
        "service_name": MessageLookupByLibrary.simpleMessage("Service name"),
        "service_photos":
            MessageLookupByLibrary.simpleMessage("Service photos"),
        "service_pr":
            MessageLookupByLibrary.simpleMessage("as a service provider"),
        "service_providers": MessageLookupByLibrary.simpleMessage("Service"),
        "services": MessageLookupByLibrary.simpleMessage("Services"),
        "services_description":
            MessageLookupByLibrary.simpleMessage("Services description"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "shareApp": MessageLookupByLibrary.simpleMessage("Share App"),
        "share_to_get_points": MessageLookupByLibrary.simpleMessage(
            "Share the app to get more points"),
        "shop_now": MessageLookupByLibrary.simpleMessage("Shop now"),
        "show_info": MessageLookupByLibrary.simpleMessage("Show info"),
        "show_order": MessageLookupByLibrary.simpleMessage("Order Detials"),
        "signOut": MessageLookupByLibrary.simpleMessage("Sign Out"),
        "sign_in_with":
            MessageLookupByLibrary.simpleMessage(" Or sign in with "),
        "sign_up_with":
            MessageLookupByLibrary.simpleMessage("Or create an account with "),
        "sign_with_apple":
            MessageLookupByLibrary.simpleMessage("Login with Apple"),
        "singup": MessageLookupByLibrary.simpleMessage("Create an account"),
        "singupDe": MessageLookupByLibrary.simpleMessage(
            "Are you sure you want to Sign Out?"),
        "skip": MessageLookupByLibrary.simpleMessage("Skip"),
        "sort_by": MessageLookupByLibrary.simpleMessage("Sort by"),
        "special_service_providers":
            MessageLookupByLibrary.simpleMessage("Special Service Providers"),
        "specialized_in":
            MessageLookupByLibrary.simpleMessage("Specialized in"),
        "startNow": MessageLookupByLibrary.simpleMessage("Start now"),
        "starttime": MessageLookupByLibrary.simpleMessage(
            "The start date of the package subscription:"),
        "state": MessageLookupByLibrary.simpleMessage("Governorate"),
        "submit": MessageLookupByLibrary.simpleMessage("Apply"),
        "subscribedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Account subscribed successfully"),
        "subscription_request": MessageLookupByLibrary.simpleMessage(
            "You already have a pending subscription request."),
        "success": MessageLookupByLibrary.simpleMessage("success"),
        "tajraa_points": MessageLookupByLibrary.simpleMessage("tajraa points "),
        "tajraa_points_desc": MessageLookupByLibrary.simpleMessage(
            "Used to reduce the cost of your purchases, you can exchange it with products."),
        "technicalSupport":
            MessageLookupByLibrary.simpleMessage("Technical Support"),
        "thanksForTime": MessageLookupByLibrary.simpleMessage(
            "Thank you for the time you took to create your account. Now that\'s the fun part, let\'s explore the app."),
        "to_tajraa": MessageLookupByLibrary.simpleMessage("in Tajraa"),
        "total": MessageLookupByLibrary.simpleMessage("Total"),
        "totalNumber": MessageLookupByLibrary.simpleMessage("Total number"),
        "total_saving": MessageLookupByLibrary.simpleMessage(
            "the amount of savings from using points"),
        "trackOrder": MessageLookupByLibrary.simpleMessage("Track the order"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("Try again"),
        "turkish": MessageLookupByLibrary.simpleMessage("Turkish"),
        "types_of_services":
            MessageLookupByLibrary.simpleMessage("Types of services"),
        "upload_image": MessageLookupByLibrary.simpleMessage("Upload image"),
        "use_coupon": MessageLookupByLibrary.simpleMessage("apply"),
        "used_points": MessageLookupByLibrary.simpleMessage("used points"),
        "version_number":
            MessageLookupByLibrary.simpleMessage("Version number"),
        "waitings": MessageLookupByLibrary.simpleMessage("Pending"),
        "warning": MessageLookupByLibrary.simpleMessage("warning"),
        "washing": MessageLookupByLibrary.simpleMessage("Washing"),
        "watch": MessageLookupByLibrary.simpleMessage("Watch"),
        "welcome": MessageLookupByLibrary.simpleMessage("Welcome"),
        "whatsapp": MessageLookupByLibrary.simpleMessage("Whatsapp"),
        "write_description_here":
            MessageLookupByLibrary.simpleMessage("Write description here"),
        "yes": MessageLookupByLibrary.simpleMessage("Yes"),
        "you_can_now_add_ads":
            MessageLookupByLibrary.simpleMessage("Great, now you can add ads"),
        "you_can_report_an_account": MessageLookupByLibrary.simpleMessage(
            "You can report an account if you were serviced badly")
      };
}
