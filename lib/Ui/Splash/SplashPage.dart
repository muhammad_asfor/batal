import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/data/sharedPreferences/SharedPrefHelper.dart';
import 'package:tajra/generated/l10n.dart';
import './/Utils/AppSnackBar.dart';
import './/Utils/SizeConfig.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../injections.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  late HomeSettingsWrapper homeSettingsWrapper;
  final GlobalKey<_SplashContentState> splashKey =
      GlobalKey<_SplashContentState>();

  @override
  void initState() {
    homeSettingsWrapper = HomeSettingsWrapper(
      successCallback: goToHomePage,
      failureCallback: failledToGetSettings,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AuthWrapper(
        onTokenReady: (isafterLogout, {token}) {
          if (!isafterLogout) {
            homeSettingsWrapper.getSettings(countryId: 166);
          }
        },
        onErrorInToken: (error) =>
            AppSnackBar.show(context, error, ToastType.Error),
        authBloc: sl<AuthBloc>(),
        child: homeSettingsWrapper
          ..child = SplashContent(
            key: splashKey,
          ));
  }

  failledToGetSettings(String error) {
    AppSnackBar.show(context, error, ToastType.Error);
  }

  goToHomePage() {
    splashKey.currentState?.setState(() {
      splashKey.currentState?.done = true;
    });
  }
}

class SplashContent extends StatefulWidget {
  SplashContent({
    Key? key,
  }) : super(key: key);

  @override
  _SplashContentState createState() => _SplashContentState();
}

class _SplashContentState extends State<SplashContent> {
  bool done = false;

  @override
  Widget build(BuildContext context) {
    if (done) {
      WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
        if (sl<AuthBloc>().isFirstLaunch) {
          Navigator.pushReplacementNamed(context, '/onboard');
        } else if (sl<AuthBloc>().isGuest) {
          Navigator.pushReplacementNamed(context, '/base');
        } else {
          Navigator.pushReplacementNamed(context, '/base');
        }
      });
    }
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [AppStyle.whiteColor, Color(0xFFE3E3E3)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                height: 150,
                width: 150,
                child: Center(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: Image.asset('assets/images/lo.png'),
                ))),
            SizedBox(
              height: 24,
            ),
            Text(
              S.of(context).for_general_services,
              style: TextStyle(
                color: AppStyle.primaryColor,
                fontSize: SizeConfig.w(22),
                fontFamily: 'HelveticaNeueLTArabicBold',
                height: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// class SplashPage extends StatefulWidget {
//   SplashPage({Key? key}) : super(key: key);

//   @override
//   _SplashPageState createState() => _SplashPageState();
// }

// class _SplashPageState extends State<SplashPage> {
//   late HomeSettingsWrapper homeSettingsWrapper;
//   @override
//   void initState() {
//     homeSettingsWrapper = HomeSettingsWrapper(
//       successCallback: goToHomePage,
//       failureCallback: failledToGetSettings,
//     );
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//     return AuthWrapper(
//         onTokenReady: (isafterLogout, {token}) {
//           if (!isafterLogout) {
//             homeSettingsWrapper.getSettings();
//           }
//         },
//         onErrorInToken: (error) =>
//             AppSnackBar.show(context, error, ToastType.Error),
//         authBloc: sl<AuthBloc>(),
//         child: homeSettingsWrapper
//           ..child = Scaffold(
//             body: Stack(
//               children: [
//                 Container(
//                   height: double.infinity,
//                   width: double.infinity,
//                   decoration: BoxDecoration(
//                       gradient: LinearGradient(
//                           begin: Alignment.topCenter,
//                           end: Alignment.bottomCenter,
//                           colors: [
//                         AppStyle.secondaryLight,
//                         AppStyle.secondaryDark
//                       ])),
//                 ),
//                 Positioned(
//                   top: -SizeConfig.h(92),
//                   left: -SizeConfig.h(90),
//                   child: SizedBox(
//                     child: SimpleShadow(
//                       offset: Offset(0, 3),
//                       sigma: 6,
//                       color: Colors.black.withOpacity(0.85),
//                       child: SvgPicture.asset(
//                         "assets/splashTop.svg",
//                         color: AppStyle.secondaryColor,
//                         height: SizeConfig.h(420),
//                         width: SizeConfig.h(600),
//                       ),
//                     ),
//                   ),
//                 ),
//                 Positioned(
//                   top: SizeConfig.h(500),
//                   left: -SizeConfig.w(120),
//                   child: SizedBox(
//                     height: SizeConfig.h(420),
//                     width: SizeConfig.w(600),
//                     child: SimpleShadow(
//                       offset: Offset(0, 3),
//                       sigma: 6,
//                       color: Colors.black.withOpacity(0.85),
//                       child: SvgPicture.asset(
//                         "assets/splashBottom.svg",
//                         color: AppStyle.secondaryColor,
//                         height: SizeConfig.h(420),
//                         width: SizeConfig.w(600),
//                       ),
//                     ),
//                   ),
//                 ),
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         Image.asset(
//                           "assets/logo.png",
//                           width: SizeConfig.h(172),
//                           height: SizeConfig.h(98),
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               ],
//             ),
//           ));
//   }

//   failledToGetSettings(String error) {
//     AppSnackBar.show(context, error, ToastType.Error);
//   }

//   goToHomePage() {
//     if (sl<AuthBloc>().isFirstLaunch) {
//       Navigator.pushReplacementNamed(context, '/onboard');
//     } else if (sl<AuthBloc>().isGuest) {
//       Navigator.pushReplacementNamed(context, '/base');
//     } else {
//       Navigator.pushReplacementNamed(context, '/base');
//     }
//   }
// }
