import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/Ui/Notifications/NotificationsPage.dart';
import '../Notifications/bloc/notifications_bloc.dart';
import '../ServiceProviders/ServiceProvidersPage.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';
import '/Ui/Home/HomePage.dart';
import '/Ui/Settings/SettingsPage.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '../../injections.dart';
import 'package:flutter_facebook_keyhash/flutter_facebook_keyhash.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'widgets/base_tutorial.dart';

class BasePage extends StatefulWidget {
  BasePage({Key? key}) : super(key: key);

  @override
  _BasePageState createState() => _BasePageState();
}

bool showedBaseTour = false;

class _BasePageState extends State<BasePage> with TickerProviderStateMixin {
  int pageIndex = 0;
  double bottomIndicatorPosition = 20;
  Category? category;

  // TutorialCoachMark? tutorialCoachMark;
  // List<TargetFocus>? targets;
  // void printKeyHash() async {
  //   String? key = await FlutterFacebookKeyhash.getFaceBookKeyHash ??
  //       'Unknown platform version';
  //   print(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;');
  //   print(key);
  //   print(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;');
  // }

  bool addOrEdit = false;
  bool veiw = false;
  bool isvew = false;
  String? id;
  var unreadNotifications;
  @override
  void initState() {
    super.initState();

    // printKeyHash();
    if (!sl<AuthBloc>().isGuest) {
      Getpackages(sl()).call(NoParams()).then((value) => value.fold((l) {
            setState(() {
              veiw = true;
            });
          }, (r) {
            print(r);
            if (r.data != null) addOrEdit = true;
            setState(() {
              veiw = true;
            });
          }));
    }

    // setState(() {
    //   if (sl<AuthBloc>().isGuest) bottomIndicatorPosition = 29;
    // });
    GetUserPost(sl()).call(NoParams()).then((value) => value.fold((l) {
          setState(() {
            isvew = true;
            id = null;
          });
        }, (r) {
          setState(() {
            isvew = true;

            id = r.id.toString();
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    unreadNotifications =
        sl<HomesettingsBloc>().settings?.unreadNotifications ?? 0;
    SizeConfig().init(context);
    DateTime? currentBackPressTime;
    Future<bool> onWillPop() async {
      if (pageIndex != 0) {
        setState(() {
          oldPageIndex = pageIndex;
          pageIndex = 0;
          bottomIndicatorPosition = 20;
        });
        return Future.value(false);
      } else {
        DateTime now = DateTime.now();
        if (currentBackPressTime == null ||
            now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
          currentBackPressTime = now;
          AppSnackBar.showToast(context, S.of(context).click_twice);

          return Future.value(false);
        }

        return Future.value(true);
      }
    }

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          extendBody: true,
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Padding(
            padding: EdgeInsets.only(top: SizeConfig.w(40)),
            child: GestureDetector(
              onTap: () async {
                if (sl<AuthBloc>().isGuest) {
                  await showLoginDialoge(context);
                } else {
                  if (veiw) {
                    sl<HomesettingsBloc>().add(GetSettings(countryId: 166));
                    addOrEdit
                        ? await Navigator.pushNamed(context, "/MyPackagePage")
                        : await Navigator.pushNamed(context, '/cheakpage');
                  }
                }
              },
              child: Container(
                height: SizeConfig.w(60),
                width: SizeConfig.w(60),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [AppStyle.primaryColor, AppStyle.secondaryColor],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                  border: Border.all(color: AppStyle.whiteColor, width: 6),
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.15),
                      blurRadius: 5,
                      spreadRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                padding: EdgeInsets.all(SizeConfig.w(15)),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: AppStyle.whiteColor, width: 1.5),
                  ),
                  child: Icon(
                    sl<AuthBloc>().isGuest
                        ? Icons.add
                        : isvew
                            ? id == null
                                ? Icons.add
                                : Icons.edit
                            : null,
                    color: AppStyle.whiteColor,
                    size: SizeConfig.w(10),
                  ),
                ),
              ),
            ),
          ),
          bottomNavigationBar: buildBottomNavigation(context),
          body: IndexedStack(
            sizing: StackFit.expand,
            alignment: Alignment.center,
            index: pageIndex,
            children: [
              HomePage(
                onServiceTap: (Category? cat) {
                  setState(() {
                    pageIndex = 1;
                    bottomIndicatorPosition = 96;
                    category = cat;
                  });
                },
              ),
              ServiceProvidersPage(
                category: category,
                state: sl<SharedPreferences>().getString("state") ?? "",
              ),
              NotificationsPage(),
              SettingsPage(),
            ],
          )),
    );
  }

  int oldPageIndex = 0;
  final NotificationsBLoc blocNotifications = NotificationsBLoc();

  Widget buildBottomNavigation(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          SizedBox(
            height: SizeConfig.w(70),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              showUnselectedLabels: true,
              selectedLabelStyle: TextStyle(
                  fontSize: SizeConfig.w(10), color: AppStyle.primaryColor),
              unselectedLabelStyle: TextStyle(
                  fontSize: SizeConfig.w(10), color: AppStyle.primaryColor),
              onTap: (int index) {
                if (index != 2) {
                  if (sl<AuthBloc>().isGuest && index > 1) {
                    showLoginDialoge(context);
                  } else {
                    // print(
                    //     ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;');
                    // print(
                    //     ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;rrr;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;');
                    // print(index);
                    // print(
                    //     ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;rr;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;');
                    setState(() {
                      if (!sl<AuthBloc>().isGuest && index == 3) {
                        blocNotifications.add(LoadEvent(""));
                      }

                      pageIndex = index > 2 ? index - 1 : index;
                      bottomIndicatorPosition = index == 0
                          ? 20
                          : index == 1
                              ? 96
                              : index == 3
                                  ? 245
                                  : 320;
                    });
                  }
                }
              },
              items: [
                BottomNavigationBarItem(
                  icon: Column(
                    children: [
                      SvgPicture.asset(
                        'assets/icons/home.svg',
                        color: pageIndex == 0
                            ? AppStyle.primaryColor
                            : AppStyle.greyTextColor,
                      ),
                      SizedBox(height: SizeConfig.w(10)),
                    ],
                  ),
                  label: S.of(context).home,
                ),
                BottomNavigationBarItem(
                  icon: Column(
                    children: [
                      Image.asset(
                        'assets/icons/handshake.png',
                        color: pageIndex == 1
                            ? AppStyle.primaryColor
                            : AppStyle.greyTextColor,
                      ),
                      SizedBox(height: SizeConfig.w(10)),
                    ],
                  ),
                  label: S.of(context).service_providers,
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.add,
                    color: Colors.transparent,
                  ),
                  label: '',
                ),
                unreadNotifications == 0
                    ? BottomNavigationBarItem(
                        icon: Column(
                          children: [
                            SvgPicture.asset(
                              'assets/icons/notification.svg',
                              color: pageIndex == 2
                                  ? AppStyle.primaryColor
                                  : AppStyle.greyTextColor,
                            ),
                            SizedBox(height: SizeConfig.w(10)),
                          ],
                        ),
                        label: S.of(context).notifications,
                      )
                    : BottomNavigationBarItem(
                        icon: Badge(
                          position: BadgePosition.topStart(
                            start: SizeConfig.h(5),
                            top: SizeConfig.h(1),
                          ),
                          badgeContent: Text(
                            unreadNotifications.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: SizeConfig.h(11)),
                          ),
                          child: Column(
                            children: [
                              SvgPicture.asset(
                                'assets/icons/notification.svg',
                                color: pageIndex == 2
                                    ? AppStyle.primaryColor
                                    : AppStyle.greyTextColor,
                              ),
                              SizedBox(height: SizeConfig.w(10)),
                            ],
                          ),
                        ),
                        label: S.of(context).notifications,
                      ),
                BottomNavigationBarItem(
                  icon: Column(
                    children: [
                      SvgPicture.asset(
                        'assets/icons/setting.svg',
                        color: pageIndex == 3
                            ? AppStyle.primaryColor
                            : AppStyle.greyTextColor,
                      ),
                      SizedBox(height: SizeConfig.w(10)),
                    ],
                  ),
                  label: S.of(context).settings,
                ),
              ],
            ),
          ),
          if (sl<HomesettingsBloc>().settings?.user?.isTechnician ?? false)
            Container(
              height: SizeConfig.w(70),
              width: SizeConfig.w(90),
              color: Colors.transparent,
            ),
          AnimatedPositioned(
            bottom: SizeConfig.w(67),
            right: SizeConfig.w(bottomIndicatorPosition),
            duration: Duration(milliseconds: 300),
            child: Container(
              height: 2,
              width: 40,
              color: AppStyle.lightColor,
            ),
          ),
        ],
      ),
    );
    // return Stack(
    //   children: [
    //     BottomAppBar(
    //       notchMargin: 8.0,
    //       shape: CircularNotchedRectangle(),
    //       child: Container(
    //         height: SizeConfig.h(67),
    //         child: Row(
    //           mainAxisSize: MainAxisSize.max,
    //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //           children: <Widget>[
    //             Padding(
    //               padding: Localizations.localeOf(context).languageCode != "ar"
    //                   ? EdgeInsets.only(left: 28.0)
    //                   : EdgeInsets.only(right: 28.0),
    //               child: ScaleTransition(
    //                 scale: homeScaleAnimation,
    //                 key: homeNavKey,
    //                 child: IconButton(
    //                   iconSize: SizeConfig.h(34),
    //                   padding: EdgeInsets.zero,
    //                   icon: Column(
    //                     mainAxisAlignment: MainAxisAlignment.center,
    //                     children: [
    //                       SvgPicture.asset("assets/home.svg",
    //                           width: SizeConfig.h(18),
    //                           color: pageIndex == 0
    //                               ? AppStyle.primaryColor
    //                               : AppStyle.secondaryColor,
    //                           height: SizeConfig.h(18)),
    //                       if (pageIndex == 0)
    //                         Text(
    //                           S.of(context).home,
    //                           style: AppStyle.vexa11.copyWith(
    //                             color: pageIndex == 0
    //                                 ? AppStyle.primaryColor
    //                                 : AppStyle.secondaryColor,
    //                           ),
    //                         )
    //                     ],
    //                   ),
    //                   onPressed: () {
    //                     searchAnimationController.reverse();
    //                     profileAnimationController.reverse();
    //                     chatAnimationController.reverse();
    //
    //                     homeAnimationController.forward();
    //                     setState(() {
    //                       oldPageIndex = pageIndex;
    //
    //                       pageIndex = 0;
    //                     });
    //                   },
    //                 ),
    //               ),
    //             ),
    //             Padding(
    //               padding: Localizations.localeOf(context).languageCode == "ar"
    //                   ? EdgeInsets.only(left: 28.0)
    //                   : EdgeInsets.only(right: 28.0),
    //               child: ScaleTransition(
    //                 scale: categoryScaleAnimation,
    //                 key: categoriesNavKey,
    //                 child: IconButton(
    //                   padding: EdgeInsets.zero,
    //                   iconSize: SizeConfig.h(50),
    //                   icon: Column(
    //                     mainAxisAlignment: MainAxisAlignment.center,
    //                     children: [
    //                       SvgPicture.asset("assets/category.svg",
    //                           width: SizeConfig.h(18),
    //                           color: pageIndex == 1
    //                               ? AppStyle.primaryColor
    //                               : AppStyle.secondaryColor,
    //                           height: SizeConfig.h(18)),
    //                       if (pageIndex == 1)
    //                         Text(
    //                           S.of(context).categories,
    //                           style: AppStyle.vexa11.copyWith(
    //                             color: pageIndex == 1
    //                                 ? AppStyle.primaryColor
    //                                 : AppStyle.secondaryColor,
    //                           ),
    //                         )
    //                     ],
    //                   ),
    //                   onPressed: () {
    //                     searchAnimationController.forward();
    //                     profileAnimationController.reverse();
    //                     chatAnimationController.reverse();
    //
    //                     homeAnimationController.reverse();
    //                     setState(() {
    //                       oldPageIndex = pageIndex;
    //
    //                       pageIndex = 1;
    //                     });
    //                   },
    //                 ),
    //               ),
    //             ),
    //             Padding(
    //               padding: Localizations.localeOf(context).languageCode != "ar"
    //                   ? EdgeInsets.only(left: 28.0)
    //                   : EdgeInsets.only(right: 28.0),
    //               child: ScaleTransition(
    //                 key: favNavKey,
    //                 scale: discountScaleAnimation,
    //                 child: IconButton(
    //                   padding: EdgeInsets.zero,
    //                   iconSize: SizeConfig.h(34),
    //                   icon: Column(
    //                     mainAxisAlignment: MainAxisAlignment.center,
    //                     children: [
    //                       Icon(
    //                         Icons.favorite,
    //                         color: pageIndex == 3
    //                             ? AppStyle.primaryColor
    //                             : AppStyle.secondaryColor,
    //                         size: SizeConfig.h(25),
    //                         // height: SizeConfig.h(18),
    //                       ),
    //                       if (pageIndex == 3)
    //                         Text(
    //                           S.of(context).myFavorites,
    //                           maxLines: 1,
    //                           style: AppStyle.vexa11.copyWith(
    //                             color: pageIndex == 3
    //                                 ? AppStyle.primaryColor
    //                                 : AppStyle.secondaryColor,
    //                           ),
    //                         )
    //                     ],
    //                   ),
    //                   onPressed: () {
    //                     if (sl<AuthBloc>().isGuest) {
    //                       showLoginDialoge(context);
    //                     } else {
    //                       searchAnimationController.reverse();
    //                       profileAnimationController.reverse();
    //                       chatAnimationController.forward();
    //
    //                       homeAnimationController.reverse();
    //
    //                       setState(() {
    //                         oldPageIndex = pageIndex;
    //
    //                         pageIndex = 3;
    //                       });
    //
    //                       BlocProvider.of<FavoriteBloc>(context)
    //                           .add(LoadEvent(""));
    //                     }
    //                   },
    //                 ),
    //               ),
    //             ),
    //             Padding(
    //               padding: Localizations.localeOf(context).languageCode == "ar"
    //                   ? EdgeInsets.only(left: 28.0)
    //                   : EdgeInsets.only(right: 28.0),
    //               child: ScaleTransition(
    //                 key: profileNavKey,
    //                 scale: profileScaleAnimation,
    //                 child: IconButton(
    //                   padding: EdgeInsets.zero,
    //                   iconSize: SizeConfig.h(34),
    //                   icon: Column(
    //                     mainAxisAlignment: MainAxisAlignment.center,
    //                     children: [
    //                       SvgPicture.asset(
    //                         "assets/profile.svg",
    //                         color: pageIndex == 4
    //                             ? AppStyle.primaryColor
    //                             : AppStyle.secondaryColor,
    //                         width: SizeConfig.h(18),
    //                         height: SizeConfig.h(18),
    //                       ),
    //                       if (pageIndex == 4)
    //                         Text(
    //                           S.of(context).my_account,
    //                           style: AppStyle.vexa11.copyWith(
    //                             color: pageIndex == 4
    //                                 ? AppStyle.primaryColor
    //                                 : AppStyle.secondaryColor,
    //                           ),
    //                         )
    //                     ],
    //                   ),
    //                   onPressed: () {
    //                     if (sl<AuthBloc>().isGuest) {
    //                       showLoginDialoge(context);
    //                     } else {
    //                       searchAnimationController.reverse();
    //                       profileAnimationController.forward();
    //                       chatAnimationController.reverse();
    //
    //                       homeAnimationController.reverse();
    //                       setState(() {
    //                         oldPageIndex = pageIndex;
    //
    //                         pageIndex = 4;
    //                       });
    //                       // if (sl<AuthBloc>().isFirstLaunch &&
    //                       //     !ProfileTutorial.showedProfileTutorial) {
    //                       //   Future.delayed(const Duration(milliseconds: 800))
    //                       //       .then((v) {
    //                       //     ProfileTutorial.initTargets(context);
    //                       //     ProfileTutorial.showTutorial(
    //                       //       context: context,
    //                       //     );
    //                       //     ProfileTutorial.showedProfileTutorial = true;
    //                       //   });
    //                       // }
    //                     }
    //                   },
    //                 ),
    //               ),
    //             )
    //           ],
    //         ),
    //       ),
    //     ),
    //
    //     // buildBottomIndecator(),
    //   ],
    // );
  }
}
