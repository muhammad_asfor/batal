import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_preview/image_preview.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/App.dart';
import 'package:tajra/App/Widgets/AppLoader.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/Ui/worker_profile/blocs/bloc/worker_profile_bloc.dart';
import 'package:tajra/Ui/worker_profile/blocs/report_bloc/report_bloc.dart';
import 'package:tajra/Ui/worker_profile/widgets/custom_radio_tile.dart';
import 'package:tajra/Ui/worker_profile/widgets/user_comment_widget.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tajra/data/sharedPreferences/SharedPrefHelper.dart';
import 'package:tajra/inner_layer/reports/domain/entities/report.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../generated/l10n.dart';

class WorkerProfilePage extends StatefulWidget {
  final String id;
  const WorkerProfilePage({Key? key, required this.id}) : super(key: key);

  @override
  _WorkerProfilePageState createState() => _WorkerProfilePageState();
}

class _WorkerProfilePageState extends State<WorkerProfilePage>
    with SingleTickerProviderStateMixin {
  final WorkerProfileBloc bloc = WorkerProfileBloc();
  final ReportBloc reportBloc = ReportBloc();
  final ScrollController scrollController = ScrollController();
  int? groupValue;
  List<Report>? reports;

  @override
  void initState() {
    bloc.add(GetDetails(widget.id));
    getLanguage();
    super.initState();
  }

  String selectedLanguage = "";
  getLanguage() async {
    selectedLanguage =
        await sl<PrefsHelper>().loadLangFromSharedPref() ?? App.defaultLanguage;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: bloc,
        builder: (context, WorkerProfileState state) {
          if (state is LoadingDetails) {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (state is ErrorInDetails) {
            AppSnackBar.show(context, state.error, ToastType.Error);
          }
          if (state is DetailsReady) {
            return Scaffold(
              appBar: AppBar(
                // toolbarHeight: _visible ? 56 : 0,
                elevation: 0,
                backgroundColor: Colors.transparent,
                leading: Padding(
                  padding: const EdgeInsets.all(12),
                  child: GestureDetector(
                    onTap: () {
                      if (sl<AuthBloc>().isGuest) {
                        showLoginDialoge(context);
                      } else {
                        final commentController = TextEditingController();
                        if (reports == null) reportBloc.add(GetReports());
                        showModalBottomSheet(
                            isScrollControlled: true,
                            context: context,
                            builder: (context) {
                              return Container(
                                height: SizeConfig.h(600),
                                child: StatefulBuilder(
                                    builder: (context, modalState) {
                                  return BlocBuilder(
                                      bloc: reportBloc,
                                      builder: (context, ReportState state) {
                                        if (state is ReportsReady) {
                                          reports = state.reports;
                                        }
                                        if (state is ReportedSuccessfully) {
                                          if (state.success) {
                                            AppSnackBar.show(
                                                context,
                                                S
                                                    .of(context)
                                                    .reportedSuccessfully,
                                                ToastType.Success);
                                            Navigator.pop(context);
                                            reports = null;
                                          }
                                        }
                                        if (state is ErrorInReports) {
                                          AppSnackBar.show(context, state.error,
                                              ToastType.Error);
                                        }
                                        return Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: SizeConfig.w(14)),
                                          child: ListView(
                                            shrinkWrap: true,
                                            children: [
                                              SizedBox(
                                                  height: SizeConfig.h(20)),
                                              Text(
                                                S.of(context).report_account,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'HelveticaNeueLTArabicBold',
                                                  color: AppStyle.primaryColor,
                                                  height: 1,
                                                  fontSize: SizeConfig.h(14),
                                                ),
                                              ),
                                              SizedBox(
                                                  height: SizeConfig.h(10)),
                                              Text(
                                                S
                                                    .of(context)
                                                    .you_can_report_an_account,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'HelveticaNeueLTArabicRoman',
                                                  color: AppStyle.primaryColor,
                                                  height: 1,
                                                  fontSize: SizeConfig.h(12),
                                                ),
                                              ),
                                              SizedBox(
                                                  height: SizeConfig.h(20)),
                                              if (state is LoadingReports)
                                                AppLoader(),
                                              if (reports != null)
                                                ...reports!
                                                    .map((e) => Column(
                                                          children: [
                                                            CustomRadioTile(
                                                              value: e.id,
                                                              groupValue:
                                                                  groupValue ??
                                                                      reports![
                                                                              0]
                                                                          .id,
                                                              reason:
                                                                  e.title ?? '',
                                                              onChanged: (int
                                                                  newValue) {
                                                                modalState(() {
                                                                  groupValue =
                                                                      newValue;
                                                                });
                                                              },
                                                            ),
                                                            SizedBox(
                                                                height:
                                                                    SizeConfig
                                                                        .h(10)),
                                                          ],
                                                        ))
                                                    .toList(),
                                              TextFormField(
                                                controller: commentController,
                                                decoration: InputDecoration(
                                                  border: OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: AppStyle.greyColor,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            3),
                                                  ),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: AppStyle.greyColor,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            3),
                                                  ),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: AppStyle.greyColor,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            3),
                                                  ),
                                                  hintText: S
                                                      .of(context)
                                                      .write_description_here,
                                                  hintStyle: TextStyle(
                                                    fontFamily:
                                                        'HelveticaNeueLTArabicBold',
                                                    color:
                                                        AppStyle.primaryColor,
                                                    height: 1,
                                                    fontSize: SizeConfig.h(12),
                                                  ),
                                                ),
                                                minLines: 4,
                                                maxLines: 4,
                                              ),
                                              SizedBox(
                                                  height: SizeConfig.h(20)),
                                              (state is LoadingReport)
                                                  ? AppLoader()
                                                  : GestureDetector(
                                                      onTap: () {
                                                        if (reports != null) {
                                                          reportBloc.add(ReportItem(
                                                              reportTypeId:
                                                                  groupValue ??
                                                                      reports![
                                                                              0]
                                                                          .id,
                                                              itemId: int.parse(
                                                                  widget.id),
                                                              comment:
                                                                  commentController
                                                                      .text));
                                                        }
                                                      },
                                                      child: Container(
                                                        width: double.maxFinite,
                                                        decoration:
                                                            BoxDecoration(
                                                          gradient: LinearGradient(
                                                              colors: [
                                                                AppStyle
                                                                    .primaryColor,
                                                                AppStyle
                                                                    .secondaryColor,
                                                              ],
                                                              begin: Alignment
                                                                  .topLeft,
                                                              end: Alignment
                                                                  .bottomRight),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                        ),
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                vertical:
                                                                    SizeConfig
                                                                        .w(14)),
                                                        child: Text(
                                                          S
                                                              .of(context)
                                                              .send_complaint,
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'HelveticaNeueLTArabicBold',
                                                            fontSize:
                                                                SizeConfig.w(
                                                                    15),
                                                            color: AppStyle
                                                                .whiteColor,
                                                            height: 1,
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                    ),
                                              SizedBox(
                                                height: MediaQuery.of(context)
                                                    .viewInsets
                                                    .bottom,
                                              ),
                                            ],
                                          ),
                                        );
                                      });
                                }),
                              );
                            });
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14),
                        border:
                            Border.all(color: AppStyle.lightColor, width: 1.5),
                      ),
                      padding: EdgeInsets.all(6),
                      child: SvgPicture.asset('assets/icons/megaphone.svg'),
                    ),
                  ),
                ),

                actions: [
                  GestureDetector(
                      onTap: () {
                        actionShare(state.product.publicLink ?? "",
                            SizeConfig.screenWidth);
                      },
                      child: Container(
                        padding: EdgeInsets.all(SizeConfig.h(5)),
                        child: Center(
                          child: Icon(
                            Icons.share_outlined,
                            size: SizeConfig.h(18),
                          ),
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppStyle.secondaryColor.withOpacity(0.8),
                        ),
                        height: SizeConfig.h(32),
                        width: SizeConfig.h(32),
                      )),
                  if (state.product.user!.isTechnician!)
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(
                              color: AppStyle.greenColor, width: 1.5),
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Row(
                          children: [
                            SvgPicture.asset('assets/icons/star.svg'),
                            Text(
                              S.of(context).premium,
                              style: TextStyle(
                                color: AppStyle.greenColor,
                                fontFamily: 'HelveticaNeueLTArabicBold',
                                height: 1,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
              body: ListView(
                shrinkWrap: true,
                controller: scrollController,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.w(20), vertical: 10),
                    child: Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Container(
                            height: SizeConfig.w(86),
                            decoration: BoxDecoration(
                              border: Border.all(color: AppStyle.greyColor),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Expanded(flex: 6, child: Container()),
                                Expanded(
                                  flex: 7,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        state.product.user?.name ?? '',
                                        style: TextStyle(
                                          overflow: TextOverflow.fade,
                                          fontFamily:
                                              'HelveticaNeueLTArabicBold',
                                          fontSize: SizeConfig.w(18),
                                          color: AppStyle.textColor,
                                          height: 1,
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          SvgPicture.asset(
                                              'assets/icons/location.svg'),
                                          SizedBox(width: SizeConfig.w(10)),
                                          Expanded(
                                            child: Text(
                                              '${state.product.user?.countryText} - ${state.product.user?.stateText}',
                                              style: TextStyle(
                                                overflow: TextOverflow.ellipsis,
                                                fontFamily:
                                                    'HelveticaNeueLTArabicRoman',
                                                color: AppStyle.secondaryDark,
                                                fontSize: SizeConfig.w(14),
                                                height: 1,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        selectedLanguage == "en"
                            ? Positioned(
                                right: 200,
                                top: 1,
                                bottom: 1,
                                child: Container(
                                  height: SizeConfig.w(135),
                                  width: SizeConfig.w(135),
                                  decoration: BoxDecoration(
                                      color: AppStyle.pinkColor,
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(70),
                                        bottomLeft: Radius.circular(70),
                                        bottomRight: Radius.circular(70),
                                      ),
                                      image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: NetworkImage(
                                              state.product.coverImage ?? ''))),
                                ),
                              )
                            : Positioned(
                                child: Container(
                                  height: SizeConfig.w(135),
                                  width: SizeConfig.w(135),
                                  decoration: BoxDecoration(
                                      color: AppStyle.pinkColor,
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(70),
                                        bottomLeft: Radius.circular(70),
                                        bottomRight: Radius.circular(70),
                                      ),
                                      image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: NetworkImage(
                                              state.product.coverImage ?? ''))),
                                ),
                              ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(25)),
                    child: Text(
                      S.of(context).services_description,
                      style: TextStyle(
                        color: AppStyle.darkTextColor,
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        fontSize: SizeConfig.h(16),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(25)),
                    child: Text(
                      state.product.description ?? '',
                      style: TextStyle(
                        color: AppStyle.textColor,
                        fontFamily: 'HelveticaNeueLTArabicRoman',
                        fontSize: SizeConfig.h(14),
                        height: 1.2,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(25)),
                    child: Text(
                      S.of(context).photo_gallery,
                      style: TextStyle(
                        color: AppStyle.darkTextColor,
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        fontSize: SizeConfig.h(16),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 100,
                    child: ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      scrollDirection: Axis.horizontal,
                      itemCount: state.product.imagesBag?.length ?? 0,
                      shrinkWrap: true,
                      itemBuilder: (context, i) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GestureDetector(
                            onTap: () {
                              openImagesPage(
                                Navigator.of(context),
                                imgUrls: state.product.imagesBag ?? [],
                              );
                            },
                            child: Container(
                              height: 85,
                              width: 85,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                  image: NetworkImage(
                                      state.product.imagesBag?[i] ?? ''),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: state.product.comments
                              ?.map((e) => UserCommentWidget(
                                    comment: e,
                                  ))
                              .toList() ??
                          [],
                    ),
                  ),
                ],
              ),
              bottomNavigationBar: (state.product.user?.countryCode != null &&
                      state.product.user?.mobile != null)
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(20),
                        ),
                        color: AppStyle.whiteColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            offset: Offset(0, 0),
                            spreadRadius: 5,
                            blurRadius: 5,
                          ),
                        ],
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.w(30),
                        vertical: SizeConfig.w(8),
                      ),
                      child: Row(children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () async {
                              final url =
                                  "tel://+${state.product.user!.countryCode!.substring(1) + (state.product.user!.mobile!.substring(state.product.user!.mobile![0] == '0' ? 1 : 0))}";
                              await launch(url);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      AppStyle.primaryColor,
                                      AppStyle.secondaryColor
                                    ],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                  ),
                                  borderRadius: BorderRadius.circular(10)),
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Icon(
                                      Icons.phone,
                                      color: AppStyle.whiteColor,
                                    ),
                                  ),
                                  // SizedBox(width: SizeConfig.w(10)),
                                  Expanded(
                                    flex: 3,
                                    child: Text(
                                      S.of(context).chat_via_phone,
                                      style: TextStyle(
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        fontSize: SizeConfig.w(12),
                                        color: AppStyle.whiteColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: SizeConfig.w(10),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () async {
                              final url =
                                  "https://wa.me/${state.product.user!.countryCode!.substring(1) + (state.product.user!.mobile!.substring(state.product.user!.mobile![0] == '0' ? 1 : 0))}";
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      AppStyle.primaryColor,
                                      AppStyle.secondaryColor
                                    ],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                  ),
                                  borderRadius: BorderRadius.circular(10)),
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                      child: SvgPicture.asset(
                                    'assets/whatsapp-svgrepo-com.svg',
                                    color: Colors.white,
                                    height: SizeConfig.w(20),
                                    width: SizeConfig.w(20),
                                  )),
                                  Expanded(
                                    flex: 3,
                                    child: Text(
                                      S.of(context).chat_via_whatsapp,
                                      style: TextStyle(
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        fontSize: SizeConfig.w(12),
                                        color: AppStyle.whiteColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ]),
                    )
                  : null,
            );
          }
          return Container();
        });
  }
}
