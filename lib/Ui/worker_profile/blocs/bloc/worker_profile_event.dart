part of 'worker_profile_bloc.dart';

@immutable
abstract class WorkerProfileEvent {}

class GetDetails extends WorkerProfileEvent {
  final String id;
  GetDetails(this.id);
}
