import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';

import '../../../../injections.dart';

part 'worker_profile_event.dart';
part 'worker_profile_state.dart';

class WorkerProfileBloc
    extends Bloc<WorkerProfileEvent, WorkerProfileState> {
  WorkerProfileBloc() : super(WorkerProfileInitial()) {
    on<GetDetails>(_onGetDetails);
  }

  _onGetDetails(GetDetails event, Emitter<WorkerProfileState> emit) async {
    emit(LoadingDetails());
    final useCase = GetProductDetails(sl());
    final result = await useCase.call(GetProductDetailsParams(id: event.id));
    result.fold((l) {
      emit(ErrorInDetails(l.errorMessage));
    }, (r) {
      emit(DetailsReady(r));
    });
  }
}
