part of 'worker_profile_bloc.dart';

@immutable
abstract class WorkerProfileState {}

class WorkerProfileInitial extends WorkerProfileState {}

class DetailsReady extends WorkerProfileState {
  final Product product;
  DetailsReady(this.product);
}

class LoadingDetails extends WorkerProfileState {}

class ErrorInDetails extends WorkerProfileState {
  final String error;
  ErrorInDetails(this.error);
}
