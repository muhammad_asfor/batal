import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:tajra/inner_layer/reports/domain/entities/report.dart';
import 'package:tajra/inner_layer/reports/domain/usecases/get_reports_use_case.dart';
import 'package:tajra/inner_layer/reports/domain/usecases/report_use_case.dart';

import '../../../../injections.dart';

part 'report_event.dart';
part 'report_state.dart';

class ReportBloc
    extends Bloc<ReportEvent, ReportState> {
  ReportBloc() : super(ReportInitial()) {
    on<GetReports>(_onGetReports);
    on<ReportItem>(_onReportItem);
  }

  _onGetReports(GetReports event, Emitter<ReportState> emit) async {
    emit(LoadingReports());
    final useCase = GetReportsUseCase(sl());
    final result = await useCase.call(NoParams());
    result.fold((l) {
      emit(ErrorInReports(l.errorMessage));
    }, (r) {
      emit(ReportsReady(r));
    });
  }

  _onReportItem(ReportItem event, Emitter<ReportState> emit) async {
    emit(LoadingReport());
    final useCase = ReportUseCase(sl());
    final result = await useCase.call(ReportParams(reportTypeId: event.reportTypeId, itemId: event.itemId, comment: event.comment));
    result.fold((l) {
      emit(ErrorInReports(l.errorMessage));
    }, (r) {
      emit(ReportedSuccessfully(r));
    });
  }
}
