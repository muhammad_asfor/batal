part of 'report_bloc.dart';

@immutable
abstract class ReportEvent {}

class GetReports extends ReportEvent {}
class ReportItem extends ReportEvent {
  final int reportTypeId;
  final int itemId;
  final String comment;

  ReportItem(
      {required this.reportTypeId,
        required this.itemId,
        required this.comment});

}