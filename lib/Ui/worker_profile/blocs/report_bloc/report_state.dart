part of 'report_bloc.dart';

@immutable
abstract class ReportState {}

class ReportInitial extends ReportState {}

class ReportsReady extends ReportState {
  final List<Report> reports;
  ReportsReady(this.reports);
}

class LoadingReports extends ReportState {}
class LoadingReport extends ReportState {}
class ReportedSuccessfully extends ReportState {
  final bool success;

  ReportedSuccessfully(this.success);
}

class ErrorInReports extends ReportState {
  final String error;
  ErrorInReports(this.error);
}
