import 'package:flutter/material.dart';

import '../../../Utils/Style.dart';

class KindOfSpecializationWidget extends StatelessWidget {
  final String imagePath;
  final String title;

  const KindOfSpecializationWidget(
      {Key? key, required this.imagePath, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: AppStyle.greyColor),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(imagePath),
            Text(
              title,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                fontSize: 12,
                color: AppStyle.textColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
