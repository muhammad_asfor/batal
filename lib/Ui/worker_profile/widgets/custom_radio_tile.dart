import 'package:flutter/material.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomRadioTile extends StatefulWidget {
  final int value;
  final int groupValue;
  final String reason;
  final Function(int) onChanged;
  final Color? borderColor;

  const CustomRadioTile(
      {Key? key,
      required this.value,
      required this.groupValue,
      required this.reason,
      required this.onChanged,
      this.borderColor})
      : super(key: key);

  @override
  _CustomRadioTileState createState() => _CustomRadioTileState();
}

class _CustomRadioTileState extends State<CustomRadioTile> {
  late int value;

  @override
  void initState() {
    value = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onChanged(value);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: widget.borderColor ?? AppStyle.greyColor),
          borderRadius: BorderRadius.circular(3),
        ),
        child: Row(
          children: [
            Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: AppStyle.greyColor,
              ),
              child: Radio(
                  value: value,
                  groupValue: widget.groupValue,
                  activeColor: AppStyle.lightColor,
                  onChanged: (int? currentValue) {
                    widget.onChanged(value);
                  }),
            ),
            Text(
              widget.reason,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: AppStyle.primaryColor,
                height: 1,
                fontSize: SizeConfig.h(12),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
