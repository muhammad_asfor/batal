import 'package:flutter/material.dart';
import 'package:progiom_cms/core.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class UserCommentWidget extends StatelessWidget {
  final Comment comment;
  const UserCommentWidget({Key? key, required this.comment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: SizeConfig.w(36),
              width: SizeConfig.w(36),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
                image: DecorationImage(
                  image: NetworkImage(comment.user?.coverImage ?? ''),
                ),
              ),
            ),
            SizedBox(width: 10),
            Text(
              comment.user?.name ?? '',
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                fontSize: SizeConfig.w(16),
                color: AppStyle.textColor,
              ),
            ),
          ],
        ),
        Text(
          comment.comment ?? '',
          style: TextStyle(
            fontFamily: 'HelveticaNeueLTArabicRoman',
            fontSize: SizeConfig.w(14),
            color: AppStyle.textColor,
          ),
        ),
      ],
    );
  }
}
