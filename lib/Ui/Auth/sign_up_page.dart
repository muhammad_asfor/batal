import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/Ui/Auth/widgets/custom_underlined_text_field_widget.dart';
import 'package:tajra/Ui/Auth/widgets/social_login_button_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/data/repository/Repository.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final bloc = sl<AuthBloc>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  String? countryCode = '';
  String? mobile = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          padding: EdgeInsets.all(SizeConfig.w(30)),
          shrinkWrap: true,
          children: [
            SizedBox(height: SizeConfig.w(30)),
            SvgPicture.asset('assets/icons/house.svg'),
            SizedBox(height: SizeConfig.w(10)),
            Text(
              S.of(context).create_account,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: AppStyle.darkTextColor,
                height: 1,
                fontSize: SizeConfig.w(22),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(20)),
            Text(
              S.of(context).fill_signup_form,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                color: AppStyle.secondaryDark,
                height: 1,
                fontSize: SizeConfig.w(14),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(30)),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                border: Border.all(
                  color: AppStyle.greyColor,
                  width: 2,
                ),
              ),
              padding: EdgeInsets.all(SizeConfig.w(20)),
              child: Column(
                children: [
                  CustomUnderlinedTextFieldWidget(
                    title: S.of(context).name,
                    svgPath: 'assets/icons/user.svg',
                    controller: nameController,
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  CustomUnderlinedTextFieldWidget(
                      title: S.of(context).e_mail,
                      svgPath: 'assets/icons/user.svg',
                      controller: emailController,
                      validator: (v) {
                        if (v != null) {
                          if (!validEmail(v)) {
                            return S.of(context).emailValidator;
                          }
                        }
                        return null;
                      }),
                  SizedBox(height: SizeConfig.w(20)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).phone_number,
                        style: TextStyle(
                          fontFamily: 'HelveticaNeueLTArabicBold',
                          color: Color(0xFF1F1F39),
                          height: 1,
                          fontSize: SizeConfig.h(12),
                        ),
                      ),
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: IntlPhoneField(
                          searchText: S.of(context).searchHere,
                          initialCountryCode: 'OM',
                          dropdownTextStyle: TextStyle(
                            fontFamily: 'HelveticaNeueLTArabicRoman',
                            color: AppStyle.textColor,
                            height: 1,
                            fontSize: SizeConfig.h(14),
                          ),
                          style: TextStyle(
                            fontFamily: 'HelveticaNeueLTArabicRoman',
                            color: AppStyle.textColor,
                            height: 1,
                            fontSize: SizeConfig.h(14),
                          ),
                          showCountryFlag: countryCode == '+963' ? false : true,
                          onCountryChanged: (c) {
                            setState(() {
                              countryCode = '+' + c.dialCode;
                            });
                          },
                          onChanged: (PhoneNumber phoneNumber) {
                            setState(() {
                              countryCode = phoneNumber.countryCode;
                              mobile = '0' + phoneNumber.number;
                            });
                          },
                          controller: phoneController,
                          invalidNumberMessage: S.of(context).mobileValidator,
                          decoration: InputDecoration(
                            hintText: S.of(context).put_your_number,
                            hintStyle: TextStyle(
                              fontFamily: 'HelveticaNeueLTArabicRoman',
                              color: AppStyle.greyTextColor,
                              height: 1,
                              fontSize: SizeConfig.h(14),
                            ),
                            suffixIcon: Padding(
                              padding: EdgeInsets.fromLTRB(SizeConfig.w(12),
                                  SizeConfig.w(14), 0, SizeConfig.w(14)),
                              child:
                                  SvgPicture.asset('assets/icons/mobile.svg'),
                            ),
                            contentPadding:
                                EdgeInsets.only(top: SizeConfig.w(17)),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppStyle.greyColor),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppStyle.greyColor),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: AppStyle.greyColor),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  CustomUnderlinedTextFieldWidget(
                    title: S.of(context).password,
                    svgPath: 'assets/icons/key.svg',
                    validator: (v) {
                      if (v != null && !validPassword(v))
                        return S.of(context).passwordValidator;
                      return null;
                    },
                    controller: passwordController,
                    canObscure: true,
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  CustomUnderlinedTextFieldWidget(
                    title: S.of(context).confirm_password,
                    svgPath: 'assets/icons/key.svg',
                    validator: (v) {
                      if (v != null && passwordController.text != v)
                        return S.of(context).passwordConfirmValidator;
                      return null;
                    },
                    canObscure: true,
                  ),
                  SizedBox(height: SizeConfig.w(30)),
                  BlocListener(
                    bloc: bloc,
                    listener: (context, AuthState state) {
                      if (state is ErrorSignUp) {
                        AppSnackBar.show(context, state.error, ToastType.Error);
                      }
                      if (state is SignUpSuccess) {
                        sl<Repository>().setFcmToken();
                        Navigator.pushReplacementNamed(context, '/base');
                      }
                    },
                    child: BlocBuilder(
                        bloc: bloc,
                        builder: (context, AuthState state) {
                          return state is LoadingSignUp
                              ? Center(
                                  child: CircularProgressIndicator(),
                                )
                              : GestureDetector(
                                  onTap: () {
                                    FocusScope.of(context).unfocus();
                                    if (formKey.currentState?.validate() ??
                                        false) {
                                      bloc.add(SignUpEvent({
                                        "name": nameController.text,
                                        "email": emailController.text,
                                        "country_code": countryCode,
                                        "mobile": mobile,
                                        "password": passwordController.text,
                                        "password_confirmation":
                                            passwordController.text,
                                      }));
                                    }
                                  },
                                  child: Container(
                                    width: double.maxFinite,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          colors: [
                                            AppStyle.primaryColor,
                                            AppStyle.secondaryColor,
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        vertical: SizeConfig.w(15)),
                                    child: Text(
                                      S.of(context).create_account,
                                      style: TextStyle(
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        fontSize: SizeConfig.w(14),
                                        color: AppStyle.whiteColor,
                                        height: 1,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                );
                        }),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.w(30)),
            Text(
              S.of(context).or_create_account_with,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                fontSize: SizeConfig.w(14),
                color: Colors.black,
                height: 1,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(30)),
            SocialLoginButtonWidget(
                isfacebook: 0,
                text: S.of(context).facebook,
                svgPath: 'assets/icons/facebook.svg'),
            SizedBox(height: SizeConfig.w(10)),
            SocialLoginButtonWidget(
                isfacebook: 1,
                text: S.of(context).google,
                svgPath: 'assets/icons/google.svg'),
            SizedBox(height: SizeConfig.w(10)),
            if (Platform.isIOS)
              SocialLoginButtonWidget(
                  isfacebook: 2,
                  text: S.of(context).apple,
                  svgPath: 'assets/icons/apple-logo.svg'),
            SizedBox(height: SizeConfig.w(10)),
            GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text(
                  S.of(context).haveAnAccount,
                  style: AppStyle.yaroCut14,
                  textAlign: TextAlign.center,
                ))
          ],
        ),
      ),
    );
  }
}
