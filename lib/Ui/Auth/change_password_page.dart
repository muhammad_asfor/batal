import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tajra/Ui/Auth/widgets/custom_underlined_text_field_widget.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          padding: EdgeInsets.all(SizeConfig.w(30)),
          shrinkWrap: true,
          children: [
            SizedBox(height: SizeConfig.w(30)),
            SvgPicture.asset('assets/icons/key.svg', height: SizeConfig.w(60),),
            SizedBox(height: SizeConfig.w(10)),
            Text(
              S.of(context).change_password,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: AppStyle.darkTextColor,
                height: 1,
                fontSize: SizeConfig.w(22),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(20)),
            Text(
              S.of(context).fill_phone_number,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                color: AppStyle.secondaryDark,
                height: 1,
                fontSize: SizeConfig.w(14),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(30)),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                border: Border.all(
                  color: AppStyle.greyColor,
                  width: 2,
                ),
              ),
              padding: EdgeInsets.all(SizeConfig.w(20)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomUnderlinedTextFieldWidget(
                    title: S.of(context).phone_number,
                    svgPath: 'assets/icons/mobile.svg',
                    hint: S.of(context).put_your_number,
                  ),
                  SizedBox(height: SizeConfig.w(30)),
                  Container(
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        AppStyle.primaryColor,
                        AppStyle.secondaryColor,
                      ], begin: Alignment.topLeft, end: Alignment.bottomRight),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    padding: EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
                    child: Text(
                      S.of(context).send_code,
                      style: TextStyle(
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        fontSize: SizeConfig.w(14),
                        color: AppStyle.whiteColor,
                        height: 1,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
