import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:tajra/Ui/Auth/widgets/custom_underlined_text_field_widget.dart';
import 'package:tajra/Utils/AppSnackBar.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';
import 'forgotPassword/CodePage.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String email = "";
  bool isLoading = false;
  bool emailNotVerified = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          padding: EdgeInsets.all(SizeConfig.w(30)),
          shrinkWrap: true,
          children: [
            SizedBox(height: SizeConfig.w(30)),
            SvgPicture.asset(
              'assets/icons/key.svg',
              height: SizeConfig.w(60),
            ),
            SizedBox(height: SizeConfig.w(10)),
            Text(
              S.of(context).change_password,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: AppStyle.darkTextColor,
                height: 1,
                fontSize: SizeConfig.w(22),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(20)),
            Text(
              S.of(context).fill_phone_number,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                color: AppStyle.secondaryDark,
                height: 1,
                fontSize: SizeConfig.w(14),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(30)),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                border: Border.all(
                  color: AppStyle.greyColor,
                  width: 2,
                ),
              ),
              padding: EdgeInsets.all(SizeConfig.w(20)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomUnderlinedTextFieldWidget(
                    autofocus: true,
                    onChanged: (v) {
                      setState(() {
                        if ((RegExp(
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(v))) {
                          email = v;
                          emailNotVerified = false;
                        } else {
                          emailNotVerified = true;
                        }
                      });
                    },
                    title: S.of(context).e_mail,
                    svgPath: 'assets/icons/mobile.svg',
                    hint: 'user@example.com',
                  ),
                  SizedBox(height: SizeConfig.w(30)),
                  isLoading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : GestureDetector(
                          onTap: emailNotVerified
                              ? () {}
                              : () {
                                  resetPassword();
                                },
                          child: Container(
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                    AppStyle.primaryColor,
                                    AppStyle.secondaryColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: SizeConfig.w(15)),
                            child: Text(
                              S.of(context).send_code,
                              style: TextStyle(
                                fontFamily: 'HelveticaNeueLTArabicBold',
                                fontSize: SizeConfig.w(14),
                                color: AppStyle.whiteColor,
                                height: 1,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  resetPassword() async {
    setState(() {
      isLoading = true;
    });
    final result = await RequestResetPassword(sl())
        .call(RequestResetPasswordParams(email: email));
    setState(() {
      isLoading = false;
    });
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (_) => CodePage(
                    email: email,
                  )));
    });
  }
}
