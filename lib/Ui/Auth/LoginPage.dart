import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/Ui/Auth/widgets/custom_underlined_text_field_widget.dart';
import 'package:tajra/Ui/Auth/widgets/social_login_button_widget.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/data/repository/Repository.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final bloc = sl<AuthBloc>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          padding: EdgeInsets.all(SizeConfig.w(30)),
          shrinkWrap: true,
          children: [
            SizedBox(height: SizeConfig.w(30)),
            SvgPicture.asset('assets/icons/house.svg'),
            SizedBox(height: SizeConfig.w(10)),
            Text(
              S.of(context).login,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: AppStyle.darkTextColor,
                height: 1,
                fontSize: SizeConfig.w(22),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(20)),
            Text(
              S.of(context).fill_login_form,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                color: AppStyle.secondaryDark,
                height: 1,
                fontSize: SizeConfig.w(14),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(30)),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                border: Border.all(
                  color: AppStyle.greyColor,
                  width: 2,
                ),
              ),
              padding: EdgeInsets.all(SizeConfig.w(20)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomUnderlinedTextFieldWidget(
                      title: S.of(context).e_mail +
                          ' / ' +
                          S.of(context).phone_number,
                      svgPath: 'assets/icons/mobile.svg',
                      // hint: S.of(context).put_your_number,
                      controller: emailController,
                      validator: (v) {
                        if (v != null) {
                          if (v.contains(new RegExp(r'[A-Za-z]'))) {
                            if (!validEmail(v)) {
                              return S.of(context).emailValidator;
                            }
                          } else {
                            if (!validPhoneNumber(v)) {
                              return S.of(context).mobileValidator;
                            }
                          }
                        }
                        return null;
                      }),
                  SizedBox(height: SizeConfig.w(20)),
                  CustomUnderlinedTextFieldWidget(
                    title: S.of(context).password,
                    controller: passwordController,
                    validator: (v) {
                      if (v != null && !validPassword(v))
                        return S.of(context).passwordValidator;
                      return null;
                    },
                    svgPath: 'assets/icons/key.svg',
                    canObscure: true,
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/forgotPassword');
                    },
                    child: Text(
                      S.of(context).forget_password,
                      style: TextStyle(
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        fontSize: SizeConfig.w(12),
                        color: AppStyle.primaryColor,
                        height: 1,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: SizeConfig.w(30)),
                  BlocListener(
                    bloc: bloc,
                    listener: (context, AuthState state) {
                      if (state is ErrorInLogin) {
                        AppSnackBar.show(
                            context,
                            S.of(context).emailOrPasswordWrong,
                            ToastType.Error);
                      }
                      if (state is LoginSuccess) {
                        sl<Repository>().setFcmToken();
                        Navigator.pushReplacementNamed(context, '/base');
                      }
                    },
                    child: BlocBuilder(
                        bloc: bloc,
                        builder: (context, AuthState state) {
                          return state is LoadingLogin
                              ? Center(
                                  child: CircularProgressIndicator(),
                                )
                              : GestureDetector(
                                  onTap: () {
                                    FocusScope.of(context).unfocus();
                                    if (formKey.currentState?.validate() ??
                                        false) {
                                      bloc.add(LoginEvent(
                                          email: emailController.text,
                                          password: passwordController.text));
                                    }
                                  },
                                  child: Container(
                                    width: double.maxFinite,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          colors: [
                                            AppStyle.primaryColor,
                                            AppStyle.secondaryColor,
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        vertical: SizeConfig.w(15)),
                                    child: Text(
                                      S.of(context).login,
                                      style: TextStyle(
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        fontSize: SizeConfig.w(14),
                                        color: AppStyle.whiteColor,
                                        height: 1,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                );
                        }),
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushReplacementNamed(context, '/base');
                    },
                    child: Container(
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        // border: Border.all(color: AppStyle.secondaryDark),
                        color: AppStyle.greyColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
                      child: Text(
                        S.of(context).continueAsGuest,
                        style: TextStyle(
                          fontFamily: 'HelveticaNeueLTArabicBold',
                          fontSize: SizeConfig.w(14),
                          color: AppStyle.secondaryDark,
                          height: 1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.w(30)),
            Text(
              S.of(context).or_login_with,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                fontSize: SizeConfig.w(14),
                color: Colors.black,
                height: 1,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.w(30)),
            SocialLoginButtonWidget(
                isfacebook: 0,
                text: S.of(context).facebook,
                svgPath: 'assets/icons/facebook.svg'),
            SizedBox(height: SizeConfig.w(10)),
            SocialLoginButtonWidget(
                isfacebook: 1,
                text: S.of(context).google,
                svgPath: 'assets/icons/google.svg'),
            SizedBox(height: SizeConfig.w(10)),
            if (Platform.isIOS)
              SocialLoginButtonWidget(
                  isfacebook: 2,
                  text: S.of(context).apple,
                  svgPath: 'assets/icons/apple-logo.svg'),
            SizedBox(height: SizeConfig.w(10)),
            GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/signUp");
                },
                child: Text(
                  S.of(context).dont_have_account,
                  style: AppStyle.yaroCut14,
                  textAlign: TextAlign.center,
                ))
          ],
        ),
      ),
    );
  }
}
