import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/data/repository/Repository.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class SocialLoginButtonWidget extends StatelessWidget {
  final String text;
  final String svgPath;
  final Color? svgColor;
  final int? isfacebook;
  const SocialLoginButtonWidget(
      {Key? key,
      required this.text,
      required this.svgPath,
      required this.isfacebook,
      this.svgColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (isfacebook == 0) {
          final result = await sl<Repository>().getFacebookToken();
          result.fold((l) {
            AppSnackBar.show(context, l.errorMessage, ToastType.Error);
          }, (r) {
            sl<AuthBloc>().add(
                LoginSocialEvent(provider: SocialLoginProvider.face, token: r));
          });
        } else if (isfacebook == 1) {
          final result = await sl<Repository>().getGoogleToken();
          result.fold((l) {
            AppSnackBar.show(context, l.errorMessage, ToastType.Error);
          }, (r) {
            sl<AuthBloc>().add(LoginSocialEvent(
                provider: SocialLoginProvider.google, token: r));
          });
        } else {
          final result = await sl<Repository>().logInByApple();
          result.fold((l) {
            AppSnackBar.show(context, l.errorMessage, ToastType.Error);
          }, (r) {
            sl<AuthBloc>().add(LoginSocialEvent(
                provider: SocialLoginProvider.google, token: r));
          });
        }
      },
      child: Container(
        width: double.maxFinite,
        decoration: BoxDecoration(
          color: AppStyle.whiteColor,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              spreadRadius: 1,
              blurRadius: 4,
              offset: Offset(0, 5),
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(10)),
              child: SvgPicture.asset(
                svgPath,
                color: svgColor,
              ),
            ),
            Expanded(
              child: Text(
                text,
                style: TextStyle(
                  fontFamily: 'HelveticaNeueLTArabicBold',
                  fontSize: SizeConfig.w(14),
                  height: 1,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(10)),
              child: SvgPicture.asset(
                svgPath,
                color: Colors.transparent,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
