import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomUnderlinedTextFieldWidget extends StatefulWidget {
  final String title;
  final String svgPath;
  final String? hint;
  final bool canObscure;
  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final bool autofocus;

  const CustomUnderlinedTextFieldWidget(
      {Key? key,
      required this.title,
      required this.svgPath,
      this.hint,
      this.canObscure = false,
      this.controller, this.validator, this.autofocus = false, this.onChanged})
      : super(key: key);

  @override
  State<CustomUnderlinedTextFieldWidget> createState() =>
      _CustomUnderlinedTextFieldWidgetState();
}

class _CustomUnderlinedTextFieldWidgetState
    extends State<CustomUnderlinedTextFieldWidget> {
  late bool obscure;

  @override
  void initState() {
    obscure = widget.canObscure;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            fontFamily: 'HelveticaNeueLTArabicBold',
            color: Color(0xFF1F1F39),
            height: 1,
            fontSize: SizeConfig.h(12),
          ),
        ),
        TextFormField(
          autofocus: widget.autofocus,
          onChanged: widget.onChanged,
          validator: widget.validator,
          controller: widget.controller,
          scrollPadding: EdgeInsets.zero,
          style: TextStyle(
            fontFamily: 'HelveticaNeueLTArabicRoman',
            color: AppStyle.textColor,
            height: 1,
            fontSize: SizeConfig.h(14),
          ),
          obscureText: obscure,
          decoration: InputDecoration(
            suffixIcon: widget.canObscure
                ? Padding(
                    padding: EdgeInsets.all(SizeConfig.w(12)),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          obscure = !obscure;
                        });
                      },
                      child: SvgPicture.asset(obscure
                          ? 'assets/icons/eye_off.svg'
                          : 'assets/icons/eye_on.svg'),
                    ),
                  )
                : null,
            hintText: widget.hint,
            hintStyle: TextStyle(
              fontFamily: 'HelveticaNeueLTArabicRoman',
              color: AppStyle.greyTextColor,
              height: 1,
              fontSize: SizeConfig.h(14),
            ),
            prefixIcon: Padding(
              padding: EdgeInsets.fromLTRB(
                  SizeConfig.w(12), SizeConfig.w(14), 0, SizeConfig.w(14)),
              child: SvgPicture.asset(widget.svgPath),
            ),
            contentPadding: EdgeInsets.only(top: SizeConfig.w(17)),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: AppStyle.greyColor),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppStyle.greyColor),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppStyle.greyColor),
            ),
          ),
        ),
      ],
    );
  }
}
