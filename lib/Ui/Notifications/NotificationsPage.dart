import 'package:flutter/material.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/notifications.dart';
import 'package:tajra/App/Widgets/AppErrorWidget.dart';
import 'package:tajra/App/Widgets/AppLoader.dart';
import 'package:tajra/App/Widgets/EmptyPlacholder.dart';
import '/Ui/Notifications/bloc/notifications_bloc.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '/generated/l10n.dart';
import 'package:timeago/timeago.dart' as timeago;

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  final Color unReadColor = const Color(0xFFEAEAEA);
  final NotificationsBLoc bloc = NotificationsBLoc();

  @override
  void initState() {
    if (!sl<AuthBloc>().isGuest) bloc.add(LoadEvent(""));
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          S.of(context).notifications,
          style: TextStyle(
            color: AppStyle.darkTextColor,
            fontSize: SizeConfig.w(18),
            fontFamily: 'HelveticaNeueLTArabicBold',
            height: 1,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          if (state is ErrorState) {
            return AppErrorWidget(text: state.error);
          }
          if (state is LoadingState) {
            return AppLoader();
          }
          if (state is SuccessState<List<ServerNotification>>) {
            if (state.items.isEmpty) {
              return EmptyPlacholder(
                title: S.of(context).no_notifications,
                imageName: "assets/noNotifications.png",
                subtitle: S.of(context).no_notifications_subtitle,
                // actionTitle: S.of(context).continueShopping,
                // onActionTap: () {
                //   Navigator.pop(context);
                // },
              );
            }
            return ListView.builder(
                controller: bloc.scrollController,
                padding: EdgeInsets.only(top: SizeConfig.h(5)),
                // itemCount: state.hasReachedMax
                //     ? state.items.length
                //     : state.items.length + 1,
                itemCount: state.items.length,
                itemBuilder: (context, index) {
                  // if (index >= state.items.length)
                  //   return Center(
                  //     child: AppLoader(),
                  //   );
                  // final noti = state.items[index];
                  // final color = noti.notificationType == "danger"
                  //     ? AppStyle.redColor
                  //     : noti.notificationType == "info"
                  //         ? AppStyle.secondaryColor
                  //         : noti.notificationType == "success"
                  //             ? AppStyle.greenColor
                  //             : AppStyle.warningColor;
                  if (index == state.items.length - 1) {
                    return SizedBox(height: AppStyle.bottomNavHieght);
                  }
                  return Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.w(32),
                        vertical: SizeConfig.w(10)),
                    child: GestureDetector(
                      onTap: () {
                        // if (!noti.isRead) {
                        //   SetNotificationAsRead(sl()).call(
                        //       SetNotificationAsReadParams(
                        //           notificationId: noti.id));
                        // }
                        //
                        // if (noti.entityType == "post") {
                        //   Navigator.pushNamed(
                        //     context,
                        //     "/productDetails",
                        //     arguments: {
                        //       "id": noti.entityId.toString(),
                        //       "goToOptions": false
                        //     },
                        //   );
                        // }
                        // if (noti.entityType == "order") {
                        //   Navigator.pushNamed(context, "/orderDetails",
                        //       arguments: noti.entityId);
                        // }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.h(24),
                            vertical: SizeConfig.h(14)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                            color: index < 3
                                ? AppStyle.greyButtonColor.withOpacity(0.7)
                                : Colors.transparent,
                            border:
                                Border.all(color: AppStyle.greyButtonColor)),
                        width: SizeConfig.screenWidth,
                        child: Row(
                          children: [
                            state.items[index].image != null
                                ? Image.network(
                                    state.items[index].image!,
                                  )
                                : Image.asset(
                                    'assets/images/batal_place_holder.png',
                                  ),
                            SizedBox(width: SizeConfig.w(15)),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    state.items[index].notificationTitle,
                                    style: AppStyle.vexa16.copyWith(
                                        color: Color(0xFF1F1F39),
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        height: 1),
                                  ),
                                  SizedBox(height: SizeConfig.w(20)),
                                  Text(
                                    timeago.format(DateTime.parse(
                                        state.items[index].createdAt)),
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                        color: AppStyle.secondaryDark,
                                        fontFamily: 'Poppins-Medium',
                                        fontSize: SizeConfig.w(12),
                                        height: 1),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                });
          }
          return Container();
        },
      ),
    );
  }
}
