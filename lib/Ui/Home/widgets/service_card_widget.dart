import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tajra/Utils/SizeConfig.dart';

import '../../../Utils/Style.dart';

class ServiceCardWidget extends StatefulWidget {
  final String svgPath;
  final bool isNetwork;
  final String service;
  final bool isFocused;
  final Function onTap;

  const ServiceCardWidget(
      {Key? key,
      required this.svgPath,
      required this.service,
      this.isNetwork = true,
      required this.isFocused,
      required this.onTap})
      : super(key: key);

  @override
  State<ServiceCardWidget> createState() => _ServiceCardWidgetState();
}

class _ServiceCardWidgetState extends State<ServiceCardWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),
          border: Border.all(
              color:
                  widget.isFocused ? AppStyle.lightColor : AppStyle.greyColor,
              width: 2),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            widget.isNetwork
                ? Image.network(
                    widget.svgPath,
                    height: 50,
                  )
                : SvgPicture.asset(widget.svgPath),
            Text(
              widget.service,
              style: TextStyle(
                  fontFamily: 'HelveticaNeueLTArabicBold',
                  fontSize: 10,
                  color: widget.isFocused
                      ? AppStyle.primaryColor
                      : AppStyle.greyTextColor),
            ),
          ],
        ),
      ),
    );
  }
}
