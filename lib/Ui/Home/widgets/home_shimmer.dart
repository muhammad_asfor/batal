import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import '/App/Widgets/ShimmerProductCard.dart';
import './/Utils/SizeConfig.dart';

class HomePageShimmer extends StatelessWidget {
  const HomePageShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        Container(
          height: SizeConfig.w(250),
          child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 5.0,
              mainAxisSpacing: 5.0,
            ),
              padding: EdgeInsets.all(10),
              physics: NeverScrollableScrollPhysics(),
              itemCount: 6,
              itemBuilder: (context, index) {
                return Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15),
                      ),
                    ),
                  ),
                );
              }),
        ),
        SizedBox(
          height: SizeConfig.h(23),
        ),
        Container(
          margin: EdgeInsets.only(
            bottom: SizeConfig.h(14),
          ),
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(24)),
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  height: SizeConfig.h(15),
                  color: Colors.grey,
                  width: SizeConfig.h(75),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: SizeConfig.h(250),
          width: double.infinity,
          child: Row(
            children: [
              Expanded(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.h(7)),
                              width: SizeConfig.h(150),
                              height: SizeConfig.h(200),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    bottomLeft: Radius.circular(15),
                                    bottomRight: Radius.circular(15),
                                  ),
                                  color: Colors.grey),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: SizeConfig.h(8),
                          ),
                        ],
                      );
                    }),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
