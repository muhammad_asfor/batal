import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:tajra/App/App.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/Ui/Home/widgets/service_card_widget.dart';
import 'package:tajra/data/sharedPreferences/SharedPrefHelper.dart';
import '/Ui/Home/widgets/home_shimmer.dart';
import '/generated/l10n.dart';
import './/App/Widgets/CustomAppBar.dart';
import './/App/Widgets/ServiceProviderCard.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import '../../injections.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  final Function onServiceTap;

  const HomePage({Key? key, required this.onServiceTap}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentCarouselIndex = 0;
  int serviceIndex = 0;
  int countryId = 166;
  CountryModel? selectedCountry;
  CityModel? selectedCity;
  StatesModel? selectedState;

  int _current = 0;
  final CarouselController _controller = CarouselController();

  final PageController pageController = PageController();
  String selectedLanguage = "ar";
  String? selectedCurrency;
  bool addOrEdit = false;
  bool veiw = false;
  @override
  void initState() {
    super.initState();
    getLanguage();
    UniLinks.initDynamicLinks((id) {
      Navigator.pushNamed(context, "/workerProfile", arguments: id);
    }, (id) {
      Navigator.pushNamed(context, "/workerProfile", arguments: id);
    });
    if (!sl<AuthBloc>().isGuest) {
      Getpackages(sl())
          .call(NoParams())
          .then((value) => value.fold((l) {}, (r) {
                if (r.data != null) addOrEdit = true;
                setState(() {
                  veiw = true;
                });
              }));
    }
  }

  getLanguage() async {
    selectedLanguage =
        await sl<PrefsHelper>().loadLangFromSharedPref() ?? App.defaultLanguage;
    selectedCurrency = await sl<PrefsHelper>().loadCurrencyFromSharedPref();
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var settings = sl<HomesettingsBloc>().settings!;
    return SafeArea(
      child: Scaffold(
        body: RefreshIndicator(
          onRefresh: () {
            sl<HomesettingsBloc>().add(GetSettings(countryId: countryId));
            return Future.delayed(Duration(seconds: 1)).then((value) {});
          },
          child: BlocBuilder(
            bloc: sl<HomesettingsBloc>(),
            builder: (context, state) {
              if (state is LoadingSettings) {
                return Column(
                  children: [
                    CustomAppBar(
                      name: '',
                      country: selectedCountry,
                      state: selectedState,
                      onChangeCountry: (Map<String, dynamic> data) {
                        selectedCountry = data['country'];
                        selectedCity = data['city'];
                        selectedState = data['state'];
                      },
                    ),
                    HomePageShimmer()
                  ],
                );
              }
              settings = sl<HomesettingsBloc>().settings!;
              return SingleChildScrollView(
                child: Column(
                  children: [
                    CustomAppBar(
                      name: settings.user?.name ?? '',
                      country: selectedCountry,
                      state: selectedState,
                      onChangeCountry: (Map<String, dynamic> data) {
                        selectedCountry = data['country'];
                        selectedCity = data['city'];
                        selectedState = data['state'];
                      },
                    ),
                    SizedBox(
                      height: SizeConfig.h(15),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        CarouselWithIndicatorState(settings),
                        buildSectionLabel(S.of(context).services, () {},
                            hasSeeAll: false),
                        buildCategoriesSection(settings),
                        if (settings.featuredServices?.isNotEmpty ?? false)
                          veiw
                              ? addOrEdit
                                  ? Container()
                                  : Column(
                                      children: [
                                        buildSectionLabel(
                                            S
                                                .of(context)
                                                .special_service_providers,
                                            () {},
                                            hasSeeAll: false),
                                        SizedBox(height: SizeConfig.w(20)),
                                        // buildProductsSection(settings.featuredServices!),
                                      ],
                                    )
                              : Container(),
                        veiw
                            ? addOrEdit
                                ? Container()
                                : SizedBox(height: 30)
                            : Container(),
                        veiw
                            ? addOrEdit
                                ? Container()
                                : Directionality(
                                    textDirection: selectedLanguage == "en"
                                        ? TextDirection.ltr
                                        : TextDirection.rtl,
                                    child: Container(
                                      height: 165,
                                      width: 311,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(15),
                                          bottomLeft: Radius.circular(15),
                                          bottomRight: Radius.circular(15),
                                        ),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/home_slider_image.png'),
                                        ),
                                      ),
                                      child: Directionality(
                                        textDirection: selectedLanguage == "en"
                                            ? TextDirection.ltr
                                            : TextDirection.rtl,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            // Container(),
                                            selectedLanguage != "en"
                                                ? Container(
                                                    width: SizeConfig.w(120),
                                                  )
                                                : Container(),

                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 20),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Text(
                                                    S.of(context).join_us,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          'HelveticaNeueLTArabicRoman',
                                                      fontSize: 14,
                                                      color:
                                                          AppStyle.whiteColor,
                                                      height: 1,
                                                    ),
                                                  ),
                                                  Text(
                                                    S.of(context).service_pr,
                                                    style: TextStyle(
                                                        fontFamily:
                                                            'HelveticaNeueLTArabicBold',
                                                        fontSize: 18,
                                                        color:
                                                            AppStyle.whiteColor,
                                                        height: 1),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      if (sl<AuthBloc>()
                                                          .isGuest) {
                                                        showLoginDialoge(
                                                            context);
                                                      } else {
                                                        Navigator.pushNamed(
                                                            context,
                                                            '/cheakpage');
                                                      }
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: AppStyle
                                                            .greyButtonColor
                                                            .withOpacity(0.7),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 10,
                                                              horizontal: 30),
                                                      child: Text(
                                                        S
                                                            .of(context)
                                                            .promote_account,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'HelveticaNeueLTArabicBold',
                                                            fontSize: 10,
                                                            color: AppStyle
                                                                .purpleColor,
                                                            height: 1),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                            : Container(),
                      ],
                    ),
                    veiw
                        ? addOrEdit
                            ? SizedBox(height: SizeConfig.h(100))
                            : SizedBox(height: SizeConfig.h(100))
                        : SizedBox(height: SizeConfig.h(30)),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildCategoriesSection(SettingsModel settings) {
    return GridView.count(
      shrinkWrap: true,
      padding: EdgeInsets.all(SizeConfig.w(20)),
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 3,
      crossAxisSpacing: SizeConfig.w(10),
      mainAxisSpacing: SizeConfig.w(10),
      children: [
        ServiceCardWidget(
          svgPath: 'assets/icons/all_services.svg',
          isNetwork: false,
          service: S.of(context).all_services,
          isFocused: serviceIndex == 0 ? true : false,
          onTap: () {
            setState(() {
              widget.onServiceTap(null);
            });
          },
        ),
        ...settings.categories
                ?.map((e) => ServiceCardWidget(
                      svgPath: e.coverImage,
                      service: e.title,
                      isFocused: serviceIndex != 0 ? true : false,
                      onTap: () {
                        setState(() {
                          widget.onServiceTap(e);
                        });
                      },
                    ))
                .toList() ??
            [],
      ],
    );
  }

  Container buildProductsSection(List<Service> services) {
    return Container(
      height: SizeConfig.h(260),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(3)),
                scrollDirection: Axis.horizontal,
                itemCount: services.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: ServiceProviderCard(
                      service: services[index],
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget buildCategoryCard(Category category) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/categoryProducts",
            arguments: {"category": category, "id": category.id});
      },
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: SizeConfig.h(7)),
            width: SizeConfig.h(61),
            height: SizeConfig.h(61),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0.0, 3.0),
                      blurRadius: 6,
                      color: Colors.black12)
                ]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: CachedNetworkImage(
                imageUrl: category.coverImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig.h(8),
          ),
          SizedBox(
            width: SizeConfig.h(61),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: Text(
                  category.title,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: AppStyle.vexa12,
                ))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget CarouselWithIndicatorState(SettingsModel settings) {
    var imgList = [...settings.slides!];
    final List<Widget> imageSliders = imgList
        .map((item) => Container(
              width: 1000,
              child: Padding(
                padding: EdgeInsets.only(right: 4, left: 4),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Image.network(
                    item.coverImage!,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ))
        .toList();

    return Column(children: [
      Container(
        height: SizeConfig.h(185),
        width: 1000,
        child: CarouselSlider(
          items: imageSliders,
          carouselController: _controller,
          options: CarouselOptions(
              autoPlay: true,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: imgList.asMap().entries.map((entry) {
          return GestureDetector(
            onTap: () => _controller.animateToPage(entry.key),
            child: Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: (Theme.of(context).brightness == Brightness.dark
                          ? AppStyle.primaryColor
                          : AppStyle.primaryColor)
                      .withOpacity(_current == entry.key ? 0.7 : 0.4)),
            ),
          );
        }).toList(),
      ),
    ]);
  }

  Container buildSectionLabel(String title, Function() onTap,
      {bool? hasSeeAll}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(24)),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: AppStyle.vexa14.copyWith(
                fontWeight: FontWeight.bold,
                color: AppStyle.darkTextColor,
                fontFamily: 'HelveticaNeueLTArabicBold'),
          ),
          if (hasSeeAll ?? true)
            GestureDetector(
              onTap: onTap,
              child: Text(
                S.of(context).see_all,
                style: AppStyle.vexaLight12,
              ),
            )
        ],
      ),
    );
  }
}
