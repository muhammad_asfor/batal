import 'package:flutter/material.dart';

import 'package:progiom_cms/homeSettings.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';

import '../../injections.dart';

class OnBoardingScreen extends StatefulWidget {
  OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle.whiteColor,
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                flex: 13,
                child: PageView(
                    controller: pageController,
                    children: sl<HomesettingsBloc>()
                            .settings
                            ?.onboards
                            .map((e) => buildOnBoarding(e))
                            .toList() ??
                        []),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SmoothPageIndicator(
                    controller: pageController,
                    count:
                        sl<HomesettingsBloc>().settings?.onboards.length ?? 1,
                    effect: ExpandingDotsEffect(
                        dotHeight: 6,
                        dotWidth: 20,
                        expansionFactor: 2,
                        activeDotColor: AppStyle.lightColor,
                        dotColor: AppStyle.greyColor),
                  ),
                ),
              ),
              Expanded(flex: 2, child: Container())
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.h(40), vertical: SizeConfig.h(25)),
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushReplacementNamed(context, "/base");
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                        decoration: BoxDecoration(
                          color: AppStyle.greyLight,
                          borderRadius: BorderRadius.circular(SizeConfig.h(20)),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              S.of(context).skip,
                              style: AppStyle.vexa14
                                  .copyWith(color: AppStyle.greyDark),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: SizeConfig.w(20)),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        if ((pageController.page!) ==
                            (sl<HomesettingsBloc>().settings?.onboards.length ??
                                    1) -
                                1)
                          Navigator.pushReplacementNamed(context, "/base");
                        else
                          pageController.animateToPage(
                              (pageController.page!.toInt()) + 1,
                              duration: Duration(milliseconds: 400),
                              curve: Curves.fastOutSlowIn);
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(SizeConfig.h(20)),
                          gradient: LinearGradient(
                            colors: [
                              AppStyle.primaryColor,
                              AppStyle.secondaryColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              S.of(context).next,
                              style: AppStyle.vexa14
                                  .copyWith(color: AppStyle.whiteColor),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildOnBoarding(Onboards onboard) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          flex: 6,
          child: Image.network(
            onboard.coverImage ?? "",
            height: SizeConfig.h(250),
            width: SizeConfig.h(250),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              onboard.title ?? "",
              style: AppStyle.vexa20,
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              onboard.description ?? "",
              style: AppStyle.vexa16
                  .copyWith(color: AppStyle.secondaryDark, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(child: Container()),
      ],
    );
  }
}
