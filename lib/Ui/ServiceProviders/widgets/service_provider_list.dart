import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/App/Widgets/AppErrorWidget.dart';
import 'package:tajra/App/Widgets/AppLoader.dart';
import 'package:tajra/App/Widgets/EmptyPlacholder.dart';
import 'package:tajra/App/Widgets/Products_shimmer_grid.dart';
import 'package:tajra/Ui/ServiceProviders/bloc/category_bloc.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';
import '../../../generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ServiceProviderList extends StatefulWidget {
  final String categoryId;

  const ServiceProviderList({Key? key, required this.categoryId})
      : super(key: key);

  @override
  State<ServiceProviderList> createState() => _ServiceProviderListState();
}

class _ServiceProviderListState extends State<ServiceProviderList> {
  late final CategoryBloc bloc;

  String categoryId = '';

  @override
  void didUpdateWidget(covariant ServiceProviderList oldWidget) {
    if (widget.categoryId != categoryId) bloc.add(LoadEvent(widget.categoryId));
    categoryId = widget.categoryId;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    bloc = CategoryBloc(
        GetProducatsByCategoryParams(categoryId: widget.categoryId));
    bloc.add(LoadEvent(widget.categoryId));
    categoryId = widget.categoryId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          return state is LoadingState
              ? ProductsShimmerGrid(
                  returnCustomScrollView: false,
                )
              : state is ErrorState
                  ? AppErrorWidget(text: state.error)
                  : state is SuccessState<List<Product>>
                      ? state.items.isEmpty
                          ? Container(
                              child: EmptyPlacholder(
                                title: S.of(context).no_result,
                                imageName: "assets/noSearch.png",
                                subtitle: S.of(context).no_result_subtitle,
                              ),
                            )
                          : ListView.builder(
                              controller: bloc.scrollController,
                              padding: EdgeInsets.fromLTRB(
                                  SizeConfig.w(20),
                                  SizeConfig.w(10),
                                  SizeConfig.w(20),
                                  AppStyle.bottomNavHieght),
                              shrinkWrap: true,
                              itemCount: state.items.length,
                              itemBuilder: (context, index) {
                                return Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: SizeConfig.w(6)),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            bottomLeft: Radius.circular(16),
                                            bottomRight: Radius.circular(16),
                                          ),
                                          color: Colors.transparent,
                                          border: Border.all(
                                            color: (state.items[index].user!
                                                        .grade ==
                                                    2)
                                                ? Color(0xFF1270E3)
                                                : (state.items[index].user!
                                                            .grade ==
                                                        3)
                                                    ? Color(0xFF2CBFC7)
                                                    : (state.items[index].user!
                                                                .grade ==
                                                            4)
                                                        ? Color(0xFFF5317F)
                                                        : AppStyle.greyColor,
                                            // color: Color(0xFF29ABE2),
                                            width: 2,
                                          ),
                                        ),
                                        padding:
                                            EdgeInsets.all(SizeConfig.h(12)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 64,
                                                  width: 64,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(30),
                                                      bottomLeft:
                                                          Radius.circular(30),
                                                      bottomRight:
                                                          Radius.circular(30),
                                                    ),
                                                    // color: Color(0xFFacedfb),
                                                    image: DecorationImage(
                                                      image: NetworkImage(state
                                                              .items[index]
                                                              .coverImage ??
                                                          ''),
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(width: 10),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width:
                                                              SizeConfig.w(100),
                                                          child: Text(
                                                            state
                                                                    .items[
                                                                        index]
                                                                    .user
                                                                    ?.name ??
                                                                '',
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  'HelveticaNeueLTArabicBold',
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              color: AppStyle
                                                                  .textColor,
                                                              fontSize: 14,
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                            width: SizeConfig.w(
                                                                5)),
                                                        if (state
                                                            .items[index]
                                                            .user!
                                                            .isTechnician!)
                                                          SvgPicture.asset(
                                                              'assets/icons/check.svg'),
                                                      ],
                                                    ),
                                                    Text(
                                                      state.items[index]
                                                              .categoryText ??
                                                          '',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'HelveticaNeueLTArabicRoman',
                                                        color: AppStyle
                                                            .greyTextColor,
                                                        fontSize: 10,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.pushNamed(
                                                  context,
                                                  "/workerProfile",
                                                  arguments: state
                                                      .items[index].id
                                                      .toString(),
                                                );
                                              },
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: AppStyle.primaryColor,
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        SizeConfig.w(30),
                                                    vertical: SizeConfig.h(15)),
                                                child: Text(
                                                  S.of(context).show_info,
                                                  style: TextStyle(
                                                    color: AppStyle.whiteColor,
                                                    fontSize: SizeConfig.w(10),
                                                    fontFamily:
                                                        'HelveticaNeueLTArabicBold',
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    if (state.items.length == index + 1)
                                      BlocBuilder(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          if (state is SuccessState) if (!state
                                              .hasReachedMax)
                                            return Center(
                                              child: AppLoader(),
                                            );
                                          return Container();
                                        },
                                      ),
                                  ],
                                );
                              })
                      : Container();
        });
  }
}
