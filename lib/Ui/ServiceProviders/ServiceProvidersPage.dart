import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Ui/ServiceProviders/widgets/service_provider_list.dart';
import '../Home/widgets/service_card_widget.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

import '../../injections.dart';
import 'bloc/category_bloc.dart';

class ServiceProvidersPage extends StatefulWidget {
  final Category? category;
  final String? state;
  ServiceProvidersPage({Key? key, this.category, this.state}) : super(key: key);

  @override
  _ServiceProvidersPageState createState() => _ServiceProvidersPageState();
}

class _ServiceProvidersPageState extends State<ServiceProvidersPage>
    with TickerProviderStateMixin {
  late TabController tabController;
  final CategoryBloc bloc =
      CategoryBloc(GetProducatsByCategoryParams(categoryId: ""));
  late SettingsModel settings;
  late List<Category>? categories;
  bool isLoading = true;

  // late bool isAll;
  late Category? category;
  late String? state;

  @override
  void didUpdateWidget(covariant ServiceProvidersPage oldWidget) {
    print(widget.state);
    setState(() {
      category = widget.category;
      state = widget.state;
      tabController = TabController(
          length: (category?.subCategories?.length ?? 0) + 1, vsync: this);
    });
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    settings = sl<HomesettingsBloc>().settings!;
    GetCategories(sl()).call(NoParams()).then((value) => value.fold((l) {
          setState(() {
            isLoading = false;
            categories = settings.categories;
          });
        }, (r) {
          setState(() {
            isLoading = false;
            categories = r;
          });
        }));
    category = widget.category;
    // tabController =
    //     TabController(length: settings.categories?.length ?? 0, vsync: this);
    // tabController.addListener(() {
    //   if (tabController.index == 0) {
    //     bloc.add(LoadEvent(""));
    //   } else {
    //     bloc.add(LoadEvent(
    //         "${settings.categories?[tabController.index - 1].id ?? ''}"));
    //   }
    // });
    // bloc.add(LoadEvent(""));
    super.initState();
  }

  Future<bool> onWillPop() async {
    if (category == null) {
      return Future.value(true);
    } else {
      setState(() {});
      category = null;
      return Future.value(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              // S.of(context).service_providers,
              category == null ? S.of(context).all_services : category!.title,
              style: TextStyle(
                color: AppStyle.darkTextColor,
                fontSize: SizeConfig.w(18),
                fontFamily: 'HelveticaNeueLTArabicBold',
                height: 1,
              ),
            ),
            leading: category == null
                ? null
                : IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      setState(() {
                        category = null;
                      });
                    },
                  ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : category == null
                  ? GridView.count(
                      shrinkWrap: true,
                      padding: EdgeInsets.only(
                          top: SizeConfig.w(20),
                          left: SizeConfig.w(20),
                          right: SizeConfig.w(20),
                          bottom: SizeConfig.w(100)),
                      // physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 3,
                      crossAxisSpacing: SizeConfig.w(10),
                      mainAxisSpacing: SizeConfig.w(10),
                      children: categories
                              ?.map((e) => ServiceCardWidget(
                                    svgPath: e.coverImage,
                                    service: e.title,
                                    isFocused: true,
                                    onTap: () {
                                      setState(() {
                                        category = e;
                                        tabController = TabController(
                                            length: category!
                                                    .subCategories!.length +
                                                1,
                                            vsync: this);
                                        // // widget.onServiceTap(e.id.toString());
                                      });
                                    },
                                  ))
                              .toList() ??
                          [],
                    )
                  : Column(
                      children: [
                        TabBar(
                            isScrollable: true,
                            unselectedLabelColor: AppStyle.greyTextColor,
                            indicatorSize: TabBarIndicatorSize.label,
                            labelColor: AppStyle.primaryColor,
                            indicatorColor: AppStyle.lightColor,
                            labelStyle: TextStyle(
                              color: AppStyle.primaryColor,
                              fontSize: SizeConfig.w(14),
                              fontFamily: 'HelveticaNeueLTArabicBold',
                              height: 1,
                            ),
                            controller: tabController,
                            tabs: [
                              Tab(
                                text: S.of(context).all,
                              ),
                              ...category?.subCategories
                                      ?.map((e) => Tab(
                                            text: e.title,
                                          ))
                                      .toList() ??
                                  [],
                            ]),
                        Expanded(
                          child: TabBarView(
                            controller: tabController,
                            children: [
                              ServiceProviderList(
                                categoryId: category!.id.toString(),
                                // service: state.items[index],
                              ),
                              ...category?.subCategories
                                      ?.map((e) => ServiceProviderList(
                                            categoryId: e.id.toString(),
                                            // service: state.items[index],
                                          ))
                                      .toList() ??
                                  []
                            ],
                          ),
                        ),
                      ],
                    )),
    );
  }
}
