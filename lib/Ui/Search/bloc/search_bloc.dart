import 'dart:async';
import 'package:dartz/dartz.dart';

import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';

import '../../../injections.dart';

class SearchBLoc extends SimpleLoaderBloc<List<Category>> {
  int page = 0;
  String query = "";
  int? categoryId;
  int? countryId;
  int? stateId;
  int? cityId;

  SearchBLoc() : super(eventParams: "");

  @override
  Future<Either<Failure, List<Category>>> load(SimpleBlocEvent event) async {
    if (event is LoadEvent) {
      page = 0;
      query = event.params['name'];
      countryId = event.params['countryId'];
      stateId = event.params['stateId'];
      cityId = event.params['cityId'];
    }
    page++;

    return GetListCategories(sl()).call(SearchCategoryParams(
      cityId: cityId,
      countryId: countryId,
      stateId: stateId,
      q: query,
    ));
  }
}
