import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LocationDialoge.dart';
import 'package:tajra/Ui/Settings/widgets/custom_rectangle_drop_down_widget.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/data/sharedPreferences/SharedPrefHelper.dart';
import 'package:tajra/generated/l10n.dart';
import 'package:dio/dio.dart';

class SearchBar extends StatefulWidget {
  final TextEditingController controller;
  final CountryModel? country;
  final StatesModel? state;
  final CityModel? city;
  final Function onTap;
  final Function onFieldSubmitted;
  final bool isHome;

  const SearchBar(
      {Key? key,
      required this.controller,
      required this.onTap,
      required this.onFieldSubmitted,
      this.country,
      this.state,
      this.city,
      this.isHome = true})
      : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

CountryModel? selectedCountry;
List<CountryModel>? countries;

StatesModel? selectedState;
List<StatesModel> states = [];

CityModel? selectedCity;
List<CityModel> cities = [];
// final searchController = TextEditingController();
final countryController = TextEditingController();
final stateController = TextEditingController();
final cityController = TextEditingController();
bool Application = false;

class _SearchBarState extends State<SearchBar> {
  getSelectedLocation() async {
    final result = await GetCountries(sl()).call(NoParams());
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      setState(() {
        countries = r;
        selectedCountry = countries![0];
        countryController.text = selectedCountry!.title!;
        getStatesOfCurrentCountry(context, null);
      });
    });
    setState(() {
      isLoading = false;
    });
  }

  bool isLoading = true;

  @override
  void initState() {
    getSelectedLocation();

    super.initState();
  }

  // String query = "";
  restart(
    String country,
    String state,
    String city,
  ) async {
    await sl<PrefsHelper>().saveFilter(
      country,
      state,
      city,
    );
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      onFieldSubmitted: (String x) {
        widget.onFieldSubmitted({
          'country': selectedCountry ?? "",
          'state': selectedState ?? "",
          'city': selectedCity ?? "",
        });
      },
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
          prefixIcon: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(15)),
            child: SvgPicture.asset('assets/icons/search.svg'),
          ),
          suffixIcon: GestureDetector(
            onTap: () {
              showModalBottomSheet(
                  isScrollControlled: true,
                  context: context,
                  builder: (context) {
                    return StatefulBuilder(builder: (context, modalState) {
                      return Container(
                        height: SizeConfig.h(400),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: SizeConfig.w(14)),
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              SizedBox(height: SizeConfig.h(20)),
                              Container(
                                  child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    S.of(context).filter,
                                    style: TextStyle(
                                      fontFamily: 'HelveticaNeueLTArabicBold',
                                      color: AppStyle.primaryColor,
                                      height: 1,
                                      fontSize: SizeConfig.h(14),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      setState(() async {
                                        cities = [];
                                        Application = false;
                                        selectedState = null;

                                        selectedCity = null;

                                        stateController.text = '';
                                        cityController.text = '';
                                        Navigator.pop(context);

                                        if (widget.isHome) {
                                          await restart(" ", " ", " ");
                                          sl<HomesettingsBloc>()
                                              .add(GetSettings());
                                        }

                                        Navigator.of(context)
                                            .pushNamedAndRemoveUntil(
                                                '/base',
                                                (Route<dynamic> route) =>
                                                    false);
                                      });
                                    },
                                    child: Text(
                                      S.of(context).delete_all,
                                      style: TextStyle(
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        color: Colors.red,
                                        height: 1,
                                        fontSize: SizeConfig.h(14),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                              SizedBox(height: SizeConfig.h(10)),
                              Text(
                                S.of(context).searchHere,
                                style: TextStyle(
                                  fontFamily: 'HelveticaNeueLTArabicRoman',
                                  color: AppStyle.primaryColor,
                                  height: 1,
                                  fontSize: SizeConfig.h(12),
                                ),
                              ),
                              SizedBox(height: SizeConfig.h(20)),
                              TextFormField(
                                readOnly: true,
                                controller: countryController,
                                onTap: () {
                                  FocusScope.of(context).unfocus();
                                  showDialog(
                                          context: context,
                                          builder: (_) => LocationDialoge())
                                      .then((value) {
                                    if (value != null) {
                                      modalState(() {
                                        states = [];
                                        selectedState = null;
                                        stateController.text = '';
                                        cityController.text = '';
                                        selectedCountry = value;
                                        countryController.text =
                                            selectedCountry!.title!;
                                        print(
                                            'selected country ${selectedCountry?.emoji}');
                                      });
                                      getStatesOfCurrentCountry(
                                          context, modalState);
                                    }
                                  });
                                },
                                decoration: InputDecoration(
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      modalState(() {
                                        selectedCity = null;
                                        selectedState = null;
                                        cities = [];
                                        states = [];
                                        selectedCountry = null;
                                        countryController.text = '';
                                        stateController.text = '';
                                        cityController.text = '';
                                      });
                                    },
                                    child: Icon(
                                      Icons.cancel_outlined,
                                      color: AppStyle.disabledColor,
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: AppStyle.greyColor,
                                    ),
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: AppStyle.greyColor,
                                    ),
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: AppStyle.greyColor,
                                    ),
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                  hintText: S.of(context).country,
                                  hintStyle: TextStyle(
                                    fontFamily: 'HelveticaNeueLTArabicBold',
                                    color: AppStyle.primaryColor,
                                    height: 1,
                                    fontSize: SizeConfig.h(12),
                                  ),
                                ),
                              ),
                              SizedBox(height: SizeConfig.w(10)),
                              CustomRectangleDropdownWidget<StatesModel>(
                                  title: S.of(context).state,
                                  value: selectedState,
                                  titleColor: states.isEmpty
                                      ? AppStyle.disabledColor
                                      : AppStyle.textColor,
                                  onChanged: (v) {
                                    modalState(() {
                                      selectedState = v;
                                    });
                                    if (v.name != "كل عُمان") {
                                      getCitiesOfCurrentCountry(
                                          context, modalState);
                                    } else {
                                      cities.clear();
                                    }
                                  },
                                  children: states.isEmpty
                                      ? null
                                      : states.map((e) {
                                          return DropdownMenuItem<StatesModel>(
                                              value: e,
                                              child: Text(
                                                (e.name ?? ""),
                                                style: AppStyle.vexa12.copyWith(
                                                    fontSize: SizeConfig.h(14)),
                                              ));
                                        }).toList()),
                              SizedBox(height: SizeConfig.w(10)),
                              CustomRectangleDropdownWidget<CityModel>(
                                  title: S.of(context).city,
                                  value: selectedCity,
                                  titleColor: cities.isEmpty
                                      ? AppStyle.disabledColor
                                      : AppStyle.textColor,
                                  onChanged: (v) {
                                    modalState(() {
                                      selectedCity = v;
                                    });
                                  },
                                  children: cities.isEmpty
                                      ? null
                                      : cities.map((e) {
                                          return DropdownMenuItem<CityModel>(
                                              value: e,
                                              child: Text(
                                                (e.name ?? ""),
                                                style: AppStyle.vexa12.copyWith(
                                                    fontSize: SizeConfig.h(14)),
                                              ));
                                        }).toList()),
                              SizedBox(height: SizeConfig.h(20)),
                              GestureDetector(
                                onTap: () async {
                                  if (widget.isHome) {
                                    await restart(
                                      selectedCountry?.id.toString() ?? " ",
                                      selectedState!.id == 999999999999999999
                                          ? " "
                                          : selectedState!.id.toString(),
                                      selectedCity != null
                                          ? selectedCity!.id ==
                                                  1111111111111111111
                                              ? " "
                                              : selectedCity!.id.toString()
                                          : "",
                                    );

                                    sl<HomesettingsBloc>().add(GetSettings());
                                  }

                                  setState(() {
                                    Application = true;
                                  });
                                  widget.onTap({
                                    'country': selectedCountry,
                                    'state': selectedState,
                                    'city': selectedCity,
                                  });

                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/base', (Route<dynamic> route) => false);
                                },
                                child: Container(
                                  width: double.maxFinite,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                          AppStyle.primaryColor,
                                          AppStyle.secondaryColor,
                                        ],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      vertical: SizeConfig.w(14)),
                                  child: Text(
                                    S.of(context).submit,
                                    style: TextStyle(
                                      fontFamily: 'HelveticaNeueLTArabicBold',
                                      fontSize: SizeConfig.w(15),
                                      color: AppStyle.whiteColor,
                                      height: 1,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).viewInsets.bottom,
                              ),
                            ],
                          ),
                        ),
                      );
                    });
                  });
            },
            child: selectedCountry == null
                ? Icon(Icons.location_on_outlined)
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Application
                          ? Container(
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.green, width: 2),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              child: Text(
                                selectedCountry?.emoji ?? '',
                                style: TextStyle(
                                  fontSize: SizeConfig.w(20),
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )
                          : Container(
                              padding: EdgeInsets.all(7),
                              child: Text(
                                selectedCountry?.emoji ?? '',
                                style: TextStyle(
                                  fontSize: SizeConfig.w(20),
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                    ],
                  ),
          ),
          hintText: S.of(context).searchHere,
          contentPadding: EdgeInsets.only(top: SizeConfig.h(20)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          fillColor: AppStyle.whiteColor,
          filled: true),
    );
  }

  Future<void> getStatesOfCurrentCountry(
      BuildContext context, modalState) async {
    final result =
        await GetStates(sl()).call(GetStatesParams(selectedCountry!.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      // if (r.isEmpty) {
      //   getCitiesOfCurrentCountry(context, null);
      // }
      if (modalState == null) {
        setState(() {
          states.clear();
          states.add(StatesModel(
              name: "كل عُمان", title: "كل عُمان", id: 999999999999999999));

          states.addAll(r);
        });
      } else {
        modalState(() {
          states.clear();
          states.add(StatesModel(
              name: "كل عُمان", title: "كل عُمان", id: 999999999999999999));

          states.addAll(r);
        });
      }
    });
  }

  Future<void> getCitiesOfCurrentCountry(
      BuildContext context, modalState) async {
    final result = await GetCities(sl()).call(GetCitiesParams(
        countryId: selectedCountry!.id, stateId: selectedState?.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (modalState == null) {
        setState(() {
          cities.clear();
          cities.add(CityModel(
              name: "كل المدن", title: "كل المدن", id: 1111111111111111111));
          cities.addAll(r);
          selectedCity =
              widget.city ?? (cities.isNotEmpty ? cities.first : null);
        });
      } else {
        modalState(() {
          cities.clear();
          cities.add(CityModel(
              name: "كل المدن", title: "كل المدن", id: 1111111111111111111));
          cities.addAll(r);
          selectedCity =
              widget.city ?? (cities.isNotEmpty ? cities.first : null);
        });
      }
    });
  }
}
