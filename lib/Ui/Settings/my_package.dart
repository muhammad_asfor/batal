import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:tajra/Ui/Settings/widgets/custom_navigate_widget.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '../../injections.dart';

class MyPackagePage extends StatefulWidget {
  MyPackagePage({Key? key}) : super(key: key);

  @override
  _MyPackagePageState createState() => _MyPackagePageState();
}

String? starttime;
String? endtime;
String? namePackge;
// List<Product> categories = [];
bool isnotem = false;
bool isView = false;
bool isvew = false;
String? id;

class _MyPackagePageState extends State<MyPackagePage> {
  @override
  void initState() {
    super.initState();
    if (!sl<AuthBloc>().isGuest) {
      Getpackages(sl()).call(NoParams()).then((value) => value.fold((l) {
            setState(() {
              print(l);
              isView = true;
            });
          }, (r) {
            setState(() {
              isView = true;
              if (r.data != null) {
                isnotem = true;
              }
              starttime = r.data!.startDate!.split("T")[0];
              endtime = r.data!.endDate!.split("T")[0];
              namePackge = r.data!.package_name!;
            });
          }));
    }
    GetUserPost(sl()).call(NoParams()).then((value) => value.fold((l) {
          setState(() {
            isvew = true;
            id = null;
          });
        }, (r) {
          setState(() {
            isvew = true;

            id = r.id.toString();
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            S.of(context).mypackage,
            style: TextStyle(
              color: AppStyle.darkTextColor,
              fontSize: SizeConfig.w(18),
              fontFamily: 'HelveticaNeueLTArabicBold',
              height: 1,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: !isView
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(20)),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    SizedBox(
                      height: SizeConfig.h(50),
                    ),
                    Text(
                      S.of(context).packages_name,
                      style: TextStyle(
                        color: AppStyle.darkTextColor,
                        fontSize: SizeConfig.w(18),
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        height: 1,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(20),
                    ),
                    Text(
                      namePackge ?? "",
                      style: TextStyle(
                        color: AppStyle.greenColor,
                        fontSize: SizeConfig.w(18),
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        height: 1,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(50),
                    ),
                    Text(
                      S.of(context).starttime,
                      style: TextStyle(
                        color: AppStyle.darkTextColor,
                        fontSize: SizeConfig.w(18),
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        height: 1,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(20),
                    ),
                    Text(
                      starttime ?? "",
                      style: TextStyle(
                        color: AppStyle.greenColor,
                        fontSize: SizeConfig.w(18),
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        height: 1,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(50),
                    ),
                    Text(
                      S.of(context).endtime,
                      style: TextStyle(
                        color: AppStyle.darkTextColor,
                        fontSize: SizeConfig.w(18),
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        height: 1,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(20),
                    ),
                    Text(
                      endtime ?? "",
                      style: TextStyle(
                        color: AppStyle.greenColor,
                        fontSize: SizeConfig.w(18),
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        height: 1,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(150),
                    ),
                    CustomNavigateWidget(
                      title: isvew
                          ? id == null
                              ? S.of(context).addser
                              : S.of(context).editser
                          : '',
                      color: AppStyle.greenColor,
                      onTap: () {
                        Navigator.pushNamed(context, "/cheakpage",
                            arguments: false);
                      },
                    ),
                    SizedBox(
                      height: SizeConfig.h(50),
                    ),
                    CustomNavigateWidget(
                      title: S.of(context).edit_personal_profile,
                      color: AppStyle.greenColor,
                      onTap: () {
                        Navigator.popAndPushNamed(context, "/personalProfile",
                            arguments: false);
                      },
                    ),
                  ],
                )));
  }
}
