import 'package:flutter/material.dart';
import 'package:tajra/Ui/Settings/widgets/custom_titled_text_field.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';

class PaymentPage extends StatefulWidget {
  const PaymentPage({Key? key}) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  String cardNumber = '';
  final cardController = TextEditingController();
  final expiryController = TextEditingController();
  final securityController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).paymentMethod,
          style: TextStyle(
            color: AppStyle.darkTextColor,
            fontSize: SizeConfig.w(12),
            fontFamily: 'HelveticaNeueLTArabicBold',
            height: 1,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: AppStyle.primaryColor,
            size: 15,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(SizeConfig.w(30)),
        child: GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/paymentDone');
          },
          child: Container(
            width: double.maxFinite,
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                AppStyle.primaryColor,
                AppStyle.secondaryColor,
              ], begin: Alignment.topLeft, end: Alignment.bottomRight),
              borderRadius: BorderRadius.circular(5),
            ),
            padding: EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
            child: Text(
              S.of(context).register_package,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                fontSize: SizeConfig.w(12),
                color: AppStyle.whiteColor,
                height: 1,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(SizeConfig.w(30)),
        shrinkWrap: true,
        children: [
          Container(
            height: SizeConfig.w(186),
            width: SizeConfig.w(245),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                image: AssetImage('assets/images/credit_card.png'),
                fit: BoxFit.fill,
              ),
            ),
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(20)),
            child: Directionality(
              textDirection: TextDirection.ltr,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Finaci',
                    style: TextStyle(
                      color: AppStyle.whiteColor,
                      fontSize: SizeConfig.w(12),
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      height: 1,
                    ),
                  ),
                  Text(
                    cardNumber,
                    style: TextStyle(
                      color: AppStyle.whiteColor,
                      fontSize: SizeConfig.w(20),
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      height: 1,
                      letterSpacing: 5,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Card Holder name',
                            style: TextStyle(
                              color: AppStyle.whiteColor,
                              fontSize: SizeConfig.w(12),
                              fontFamily: 'HelveticaNeueLTArabicRoman',
                              height: 1,
                            ),
                          ),
                          SizedBox(height: SizeConfig.w(10)),
                          Text(
                            'Austin Hammond',
                            style: TextStyle(
                              color: AppStyle.whiteColor,
                              fontSize: SizeConfig.w(12),
                              fontFamily: 'HelveticaNeueLTArabicBold',
                              height: 1,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Expiry Date',
                            style: TextStyle(
                              color: AppStyle.whiteColor,
                              fontSize: SizeConfig.w(12),
                              fontFamily: 'HelveticaNeueLTArabicRoman',
                              height: 1,
                            ),
                          ),
                          SizedBox(height: SizeConfig.w(10)),
                          Text(
                            '02/30',
                            style: TextStyle(
                              color: AppStyle.whiteColor,
                              fontSize: SizeConfig.w(12),
                              fontFamily: 'HelveticaNeueLTArabicBold',
                              height: 1,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: SizeConfig.w(20)),
          CustomTitledTextField(
            controller: cardController,
            title: S.of(context).cardNumber,
            name: '',
            svgPath: 'assets/icons/visa.svg',
            onChanged: (String value) {
              setState(() {
                String x = '';
                value.runes.forEach((int rune) {
                  var character = new String.fromCharCode(rune);
                  x += '*';
                });
                cardNumber = x.replaceAllMapped(
                    RegExp(r".{4}"), (match) => "${match.group(0)} ");
              });
            },
          ),
          SizedBox(height: SizeConfig.w(20)),
          Row(
            children: [
              Expanded(
                child: CustomTitledTextField(
                  controller: expiryController,
                  title: S.of(context).expiryDate,
                  name: '01/22',
                ),
              ),
              SizedBox(width: SizeConfig.w(10)),
              Expanded(
                child: CustomTitledTextField(
                  controller: securityController,
                  title: S.of(context).security_code,
                  name: '36262',
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
