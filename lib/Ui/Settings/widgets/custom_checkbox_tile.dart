import 'package:flutter/material.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomCheckboxTile extends StatefulWidget {
  final String title;

  const CustomCheckboxTile({Key? key, required this.title}) : super(key: key);

  @override
  State<CustomCheckboxTile> createState() => _CustomCheckboxTileState();
}

class _CustomCheckboxTileState extends State<CustomCheckboxTile> {
  bool value = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          value = !value;
        });
      },
      child: Row(
        children: [
          Checkbox(
              shape:
                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
              value: value,
              onChanged: (bool? newValue) {
                setState(() {
                  value = !value;
                });
              }),
          Expanded(
            child: Text(
              widget.title,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                fontSize: SizeConfig.w(12),
                color: AppStyle.darkTextColor,
                height: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
