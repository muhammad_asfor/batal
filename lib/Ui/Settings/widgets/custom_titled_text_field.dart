import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomTitledTextField extends StatefulWidget {
  final String title;
  final String? name;
  final bool isNumber;
  final String? svgPath;
  final Function(String)? onChanged;
  final TextEditingController controller;
  final String? Function(String?)? validator;
  final Function? onClick;
  final bool isGreyedOut;

  const CustomTitledTextField({
    Key? key,
    required this.title,
    this.name,
    this.isNumber = false,
    this.svgPath,
    required this.controller,
    this.validator,
    this.onChanged,
    this.onClick,
    this.isGreyedOut = false,
  }) : super(key: key);

  @override
  State<CustomTitledTextField> createState() => _CustomTitledTextFieldState();
}

class _CustomTitledTextFieldState extends State<CustomTitledTextField> {
  final textController = TextEditingController();

  @override
  void initState() {
    textController.text = widget.name ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onClick != null
          ? () {
              widget.onClick!();
            }
          : null,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: AppStyle.greyColor, width: 2),
          borderRadius: BorderRadius.circular(6),
        ),
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(10)),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: SizeConfig.w(15)),
                  Text(
                    widget.title,
                    style: TextStyle(
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      color: widget.isGreyedOut
                          ? AppStyle.disabledColor
                          : AppStyle.primaryColor,
                      height: 1,
                      fontSize: SizeConfig.h(12),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    validator: widget.validator,
                    readOnly: widget.onClick != null ? true : false,
                    textDirection:
                        widget.isNumber ? TextDirection.ltr : TextDirection.rtl,
                    controller: widget.controller,
                    scrollPadding: EdgeInsets.zero,
                    onChanged: (String? x) {
                      if (widget.onChanged != null) widget.onChanged!(x ?? '');
                    },
                    style: TextStyle(
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      color: AppStyle.primaryColor,
                      height: 1,
                      fontSize: SizeConfig.h(14),
                    ),
                    decoration: InputDecoration(
                      isCollapsed: true,
                      contentPadding: EdgeInsets.symmetric(vertical: 8),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                    ),
                  ),
                  SizedBox(height: SizeConfig.w(15)),
                ],
              ),
            ),
            if (widget.svgPath != null) SvgPicture.asset(widget.svgPath!),
          ],
        ),
      ),
    );
  }
}
