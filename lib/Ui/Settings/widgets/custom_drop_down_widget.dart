import 'package:flutter/material.dart';
import 'package:tajra/generated/l10n.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomDropdownWidget<T> extends StatefulWidget {
  final String title;
  final List<T> children;
  final T initialValue;
  final Function(dynamic) onChanged;

  const CustomDropdownWidget(
      {Key? key,
      required this.title,
      required this.children,
      required this.initialValue,
      required this.onChanged})
      : super(key: key);

  @override
  _CustomDropdownWidgetState createState() => _CustomDropdownWidgetState();
}

class _CustomDropdownWidgetState extends State<CustomDropdownWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            fontFamily: 'HelveticaNeueLTArabicBold',
            color: AppStyle.textColor,
            fontSize: SizeConfig.w(14),
            height: 1,
          ),
        ),
        DropdownButtonFormField(
          value: widget.initialValue,
          style: TextStyle(
            fontFamily: 'HelveticaNeueLTArabicRoman',
            fontSize: SizeConfig.w(16),
            color: AppStyle.textColor,
            height: 1,
          ),
          items: widget.children.map((value) {
            return new DropdownMenuItem(
                value: value,
                child: Text(value == 'ar'
                    ? S.of(context).arabic
                    : value == 'en'
                        ? S.of(context).english
                        : value == 'tr'
                            ? S.of(context).turkish
                            : value.title));
          }).toList(),
          onChanged: (newValue) {
            widget.onChanged(newValue);
          },
          decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppStyle.greyColor))),
          icon: Icon(
            Icons.keyboard_arrow_down,
            color: AppStyle.primaryColor,
          ),
          // value: _category,
        )
      ],
    );
  }
}
