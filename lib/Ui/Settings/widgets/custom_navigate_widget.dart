import 'package:flutter/material.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomNavigateWidget extends StatelessWidget {
  final String title;
  final bool showIcon;
  final Function onTap;
  final Color? color;

  const CustomNavigateWidget(
      {Key? key,
      required this.title,
      this.showIcon = true,
      required this.onTap,
      this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: color ?? AppStyle.greyColor),
          borderRadius: BorderRadius.circular(6),
        ),
        padding:
            EdgeInsets.symmetric(vertical: 20, horizontal: SizeConfig.w(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: color ?? AppStyle.primaryColor,
                height: 1,
                fontSize: SizeConfig.h(14),
              ),
            ),
            if (showIcon)
              Icon(
                Icons.arrow_forward_ios_sharp,
                color: color ?? AppStyle.primaryColor,
                size: SizeConfig.w(15),
              ),
          ],
        ),
      ),
    );
  }
}
