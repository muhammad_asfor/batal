import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';
import '../../../generated/l10n.dart';

class CustomSwitchTile extends StatefulWidget {
  final String title;
  final bool initialValue;

  const CustomSwitchTile(
      {Key? key, required this.title, this.initialValue = true})
      : super(key: key);

  @override
  _CustomSwitchTileState createState() => _CustomSwitchTileState();
}

class _CustomSwitchTileState extends State<CustomSwitchTile> {
  late bool value;

  @override
  void initState() {
    value = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            fontFamily: 'HelveticaNeueLTArabicBold',
            color: AppStyle.textColor,
            fontSize: SizeConfig.w(14),
            height: 1,
          ),
        ),
        SizedBox(height: SizeConfig.w(20)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              value ? S.of(context).activated : S.of(context).deactivated,
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                fontSize: SizeConfig.w(16),
                color: AppStyle.textColor,
                height: 1,
              ),
            ),
            FlutterSwitch(
              height: SizeConfig.w(20),
              width: SizeConfig.w(33.33),
              activeColor: AppStyle.lightColor,
              value: value,
              borderRadius: 7,
              onToggle: (bool? x) {
                // setState(() {
                //   value = !value;
                // });
              },
              activeToggleColor: Colors.transparent,
              inactiveToggleColor: Colors.transparent,
              toggleSize: 20,
              padding: 0,
              activeIcon: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6)),
              ),
              inactiveIcon: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6)),
              ),
            ),
          ],
        ),
        SizedBox(height: SizeConfig.w(10)),
        Divider(
          color: AppStyle.greyColor,
        ),
      ],
    );
  }
}
