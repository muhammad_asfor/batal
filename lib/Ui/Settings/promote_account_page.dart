import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LocationDialoge.dart';
import 'package:tajra/Ui/Settings/widgets/custom_rectangle_drop_down_widget.dart';
import 'package:tajra/Ui/Settings/widgets/custom_titled_text_field.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';
import 'blocs/address_bloc/address_bloc.dart';

class PromoteAccountPage extends StatefulWidget {
  const PromoteAccountPage({Key? key}) : super(key: key);

  @override
  _PromoteAccountPageState createState() => _PromoteAccountPageState();
}

List<CountryModel>? countries;

class _PromoteAccountPageState extends State<PromoteAccountPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  late final TextEditingController fullNameController;
  final addressController = TextEditingController();
  final AddressBloc bloc = AddressBloc();
  CountryModel? selectedCountry;
  StatesModel? selectedState;
  CityModel? selectedCity;
  final List<StatesModel> states = [];
  final List<CityModel> cities = [];

  String? countryCode = '';
  String? mobile = '';

  final TextEditingController phoneController = TextEditingController();
  PhoneNumber? phoneNumber;
  getSelectedLocation() async {
    final result = await GetCountries(sl()).call(NoParams());
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      setState(() {
        countries = r;
        selectedCountry = countries![0];

        getStatesOfCurrentCountry(context);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getSelectedLocation();

    fullNameController = TextEditingController(
        text: sl<HomesettingsBloc>().settings?.user?.name ?? "");
    if (sl<HomesettingsBloc>().settings?.user?.countryCode != null) {
      final countryCode = CountryPickerUtils.getCountryByPhoneCode(
              sl<HomesettingsBloc>()
                      .settings
                      ?.user
                      ?.countryCode
                      ?.substring(1) ??
                  '')
          .isoCode;
      // print('cc is ${cc.numeric}');
      phoneNumber = PhoneNumber(
          countryISOCode: countryCode,
          countryCode: sl<HomesettingsBloc>().settings?.user?.countryCode ?? '',
          number: sl<HomesettingsBloc>().settings?.user?.mobile ?? '');
    }
    phoneController.text =
        (sl<HomesettingsBloc>().settings?.user?.mobile == null
            ? ''
            : sl<HomesettingsBloc>().settings?.user?.mobile!.substring(1))!;
    // phoneController.text =
    //     sl<HomesettingsBloc>().settings?.user?.mobile!.substring(1) ?? '';
    countryCode = phoneNumber == null ? "" : phoneNumber!.countryCode;
    mobile = phoneNumber == null ? "" : '0' + phoneNumber!.number;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).promote_account,
          style: TextStyle(
            color: AppStyle.darkTextColor,
            fontSize: SizeConfig.w(18),
            fontFamily: 'HelveticaNeueLTArabicBold',
            height: 1,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: AppStyle.primaryColor,
            size: 15,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(20)),
          child: ListView(
            shrinkWrap: true,
            children: [
              CustomTitledTextField(
                controller: fullNameController,
                validator: (v) {
                  if (v != null) {
                    if (v == '') {
                      return S.of(context).nameRequired;
                    }
                  }
                  return null;
                },
                title: S.of(context).fullName,
                // name: 'نجدت العقاد',
              ),
              SizedBox(height: SizeConfig.w(10)),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: AppStyle.greyColor, width: 2),
                  borderRadius: BorderRadius.circular(6),
                ),
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(10)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: SizeConfig.w(15)),
                    Text(
                      S.of(context).phone_number,
                      style: TextStyle(
                        fontFamily: 'HelveticaNeueLTArabicBold',
                        color: AppStyle.primaryColor,
                        height: 1,
                        fontSize: SizeConfig.h(12),
                      ),
                    ),
                    SizedBox(height: 10),
                    Directionality(
                      textDirection: TextDirection.ltr,
                      child: IntlPhoneField(
                        initialCountryCode: phoneNumber?.countryISOCode,
                        searchText: S.of(context).searchHere,

                        dropdownTextStyle: TextStyle(
                          fontFamily: 'HelveticaNeueLTArabicRoman',
                          color: AppStyle.textColor,
                          height: 1,
                          fontSize: SizeConfig.h(14),
                        ),
                        style: TextStyle(
                          fontFamily: 'HelveticaNeueLTArabicRoman',
                          color: AppStyle.textColor,
                          height: 1,
                          fontSize: SizeConfig.h(14),
                        ),
                        // showCountryFlag: countryCode == '+963' ? false : true,
                        onCountryChanged: (c) {
                          setState(() {
                            countryCode = '+' + c.dialCode;
                          });
                        },
                        onChanged: (PhoneNumber phoneNumber) {
                          setState(() {
                            countryCode = phoneNumber.countryCode;
                            mobile = '0' + phoneNumber.number;
                          });
                        },
                        controller: phoneController,

                        invalidNumberMessage: S.of(context).mobileValidator,
                        decoration: InputDecoration(
                          hintText: S.of(context).put_your_number,
                          hintStyle: TextStyle(
                            fontFamily: 'HelveticaNeueLTArabicRoman',
                            color: AppStyle.greyTextColor,
                            height: 1,
                            fontSize: SizeConfig.h(14),
                          ),
                          contentPadding:
                              EdgeInsets.only(top: SizeConfig.w(17)),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppStyle.greyColor),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppStyle.greyColor),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppStyle.greyColor),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SizeConfig.w(10)),
              GestureDetector(
                onTap: () {
                  FocusScope.of(context).unfocus();
                  showDialog(
                      context: context,
                      builder: (_) => LocationDialoge()).then((value) {
                    if (value != null) {
                      setState(() {
                        selectedState = null;
                        selectedCity = null;
                        selectedCountry = value;
                      });
                      getStatesOfCurrentCountry(context);
                    }
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: AppStyle.greyColor),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.w(20), horizontal: SizeConfig.w(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        S.of(context).country +
                            ' ' +
                            (selectedCountry != null
                                ? (selectedCountry!.title ?? "")
                                : ""),
                        style: TextStyle(
                          fontFamily: 'HelveticaNeueLTArabicBold',
                          color: AppStyle.textColor,
                          fontSize: SizeConfig.w(12),
                          height: 1,
                        ),
                      ),
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: AppStyle.primaryColor,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.w(10)),
              CustomRectangleDropdownWidget<StatesModel>(
                  title: S.of(context).state,
                  value: selectedState,
                  titleColor: states.isEmpty
                      ? AppStyle.disabledColor
                      : AppStyle.textColor,
                  onChanged: (v) {
                    setState(() {
                      selectedState = v;
                    });
                    getCitiesOfCurrentCountry(context);
                  },
                  children: states.isEmpty
                      ? null
                      : states.map((e) {
                          return DropdownMenuItem<StatesModel>(
                              value: e,
                              child: Text(
                                (e.name ?? ""),
                                style: AppStyle.vexa12
                                    .copyWith(fontSize: SizeConfig.h(14)),
                              ));
                        }).toList()),
              SizedBox(height: SizeConfig.w(10)),
              CustomRectangleDropdownWidget<CityModel>(
                  title: S.of(context).city,
                  value: selectedCity,
                  titleColor: cities.isEmpty
                      ? AppStyle.disabledColor
                      : AppStyle.textColor,
                  onChanged: (v) {
                    setState(() {
                      selectedCity = v;
                    });
                  },
                  children: cities.isEmpty
                      ? null
                      : cities.map((e) {
                          return DropdownMenuItem<CityModel>(
                              value: e,
                              child: Text(
                                (e.name ?? ""),
                                style: AppStyle.vexa12
                                    .copyWith(fontSize: SizeConfig.h(14)),
                              ));
                        }).toList()),
              SizedBox(height: SizeConfig.w(10)),
              GestureDetector(
                onTap: () {
                  if (formKey.currentState?.validate() ?? false) {
                    Navigator.popAndPushNamed(context, '/packages', arguments: {
                      'name': fullNameController.text,
                      'countryCode': countryCode,
                      'mobile': mobile!.substring(1),
                      'countryId': selectedCountry?.id,
                      'stateId': selectedState?.id,
                      'cityId': selectedCity?.id,
                    });
                  }
                },
                child: Container(
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      AppStyle.primaryColor,
                      AppStyle.secondaryColor,
                    ], begin: Alignment.topLeft, end: Alignment.bottomRight),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
                  child: Text(
                    S.of(context).choose_a_package,
                    style: TextStyle(
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      fontSize: SizeConfig.w(14),
                      color: AppStyle.whiteColor,
                      height: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getStatesOfCurrentCountry(BuildContext context) async {
    final result =
        await GetStates(sl()).call(GetStatesParams(selectedCountry!.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (r.isEmpty) {}
      setState(() {
        states.clear();
        states.addAll(r);
        selectedCity = null;
        if (sl<HomesettingsBloc>().settings?.user?.state_id != null)
          states.forEach((element) {
            if (element.id == sl<HomesettingsBloc>().settings?.user?.state_id) {
              selectedState = element;
              getCitiesOfCurrentCountry(context);
            }
          });
      });
    });
  }

  Future<void> getCitiesOfCurrentCountry(BuildContext context) async {
    final result = await GetCities(sl()).call(GetCitiesParams(
        countryId: selectedCountry!.id, stateId: selectedState?.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      setState(() {
        cities.clear();
        cities.addAll(r);

        selectedCity = cities.isNotEmpty ? cities.first : null;
        if (sl<HomesettingsBloc>().settings?.user?.city_id != null)
          cities.forEach((element) {
            if (element.id == sl<HomesettingsBloc>().settings?.user?.city_id) {
              selectedCity = element;
            }
          });
      });
    });
  }
}
