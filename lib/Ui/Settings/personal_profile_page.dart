import 'dart:io';

import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/AppLoader.dart';
import 'package:tajra/App/Widgets/LocationDialoge.dart';
import 'package:tajra/App/Widgets/MainButton.dart';
import 'package:tajra/Ui/Search/widgets/search_bar.dart';
import 'package:tajra/Ui/Settings/widgets/custom_rectangle_drop_down_widget.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/generated/l10n.dart';

class PersonalProfilePage extends StatefulWidget {
  const PersonalProfilePage({Key? key}) : super(key: key);

  @override
  State<PersonalProfilePage> createState() => _PersonalProfilePageState();
}

List<CountryModel>? countries;

class _PersonalProfilePageState extends State<PersonalProfilePage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  late final TextEditingController nameController;
  final TextEditingController phoneController = TextEditingController();
  PhoneNumber? phoneNumber;
  XFile? imageFileList;
  bool isLoading = false;
  Future<void> _onImageButtonPressed(ImageSource source,
      {BuildContext? context}) async {
    try {
      final XFile? pickedFileList = await _picker.pickImage(source: source);

      setState(() {
        if (pickedFileList != null) imageFileList = pickedFileList;
      });
    } catch (e) {}
  }

  CountryModel? selectedCountry;
  StatesModel? selectedState;
  CityModel? selectedCity;
  final List<StatesModel> states = [];
  final List<CityModel> cities = [];

  getSelectedLocation() async {
    final result = await GetCountries(sl()).call(NoParams());
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      setState(() {
        countries = r;
        selectedCountry = countries![0];

        getStatesOfCurrentCountry(context);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getSelectedLocation();
    nameController = TextEditingController(
        text: sl<HomesettingsBloc>().settings?.user?.name ?? "");
    if (sl<HomesettingsBloc>().settings?.user?.countryCode != null) {
      final countryCode = CountryPickerUtils.getCountryByPhoneCode(
              sl<HomesettingsBloc>()
                      .settings
                      ?.user
                      ?.countryCode
                      ?.substring(1) ??
                  '')
          .isoCode;
      // print('cc is ${cc.numeric}');
      phoneNumber = PhoneNumber(
          countryISOCode: countryCode,
          countryCode: sl<HomesettingsBloc>().settings?.user?.countryCode ?? '',
          number: sl<HomesettingsBloc>().settings?.user?.mobile ?? '');
    }
    phoneController.text =
        (sl<HomesettingsBloc>().settings?.user?.mobile == null
            ? ''
            : sl<HomesettingsBloc>().settings?.user?.mobile!.substring(1))!;
  }

  final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).personal_profile,
          style: TextStyle(
            color: AppStyle.darkTextColor,
            fontSize: SizeConfig.w(18),
            fontFamily: 'HelveticaNeueLTArabicBold',
            height: 1,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: AppStyle.primaryColor,
            size: 15,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.h(28),
          ),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 200,
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.h(80),
                        foregroundImage: imageFileList != null
                            ? FileImage(File(imageFileList!.path))
                            : null,
                        backgroundImage: imageFileList == null
                            ? (sl<HomesettingsBloc>()
                                        .settings
                                        ?.user
                                        ?.coverImage !=
                                    null)
                                ? NetworkImage((sl<HomesettingsBloc>()
                                        .settings
                                        ?.user
                                        ?.coverImage) ??
                                    "")
                                : null
                            : null,
                      ),
                      Positioned(
                        bottom: SizeConfig.h(25),
                        child: Container(
                          height: SizeConfig.h(35),
                          width: SizeConfig.h(35),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.white),
                          child: IconButton(
                            onPressed: () {
                              _onImageButtonPressed(
                                ImageSource.gallery,
                                context: context,
                              );
                            },
                            icon: Icon(
                              Icons.add_a_photo_outlined,
                              color: AppStyle.primaryColor,
                              size: SizeConfig.h(21),
                            ),
                            padding: EdgeInsets.zero,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    Text(
                      S.of(context).fullName,
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: SizeConfig.h(60),
                      child: TextFormField(
                        validator: (v) {
                          if (v != null) {
                            if (v.isEmpty) {
                              return S.of(context).nameRequired;
                            }
                          }
                          return null;
                        },
                        controller: nameController,
                        style: AppStyle.vexa14.copyWith(color: Colors.black),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16))),
                      ),
                    ))
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Row(
                  children: [
                    Text(
                      S.of(context).phone_number,
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Directionality(
                  textDirection: TextDirection.ltr,
                  child: IntlPhoneField(
                    initialCountryCode: phoneNumber?.countryISOCode,
                    searchText: S.of(context).searchHere,
                    dropdownTextStyle: TextStyle(fontSize: SizeConfig.w(12)),
                    style: TextStyle(fontSize: SizeConfig.w(12)),
                    showCountryFlag:
                        phoneNumber?.countryCode == '+963' ? false : true,
                    onCountryChanged: (c) {
                      setState(() {
                        phoneNumber?.countryCode = '+' + c.dialCode;
                      });
                    },
                    onChanged: (PhoneNumber phone) {
                      setState(() {
                        phoneNumber?.countryCode = phone.countryCode;
                        phoneNumber?.number = '0' + phone.number;
                      });
                    },
                    controller: phoneController,
                    invalidNumberMessage: S.of(context).mobileValidator,
                    decoration: InputDecoration(
                        hintTextDirection: TextDirection.rtl,
                        alignLabelWithHint: true,
                        suffixIcon: Icon(Icons.phone_android),
                        labelText: S.of(context).phone_number,
                        contentPadding: EdgeInsets.symmetric(
                          // vertical: SizeConfig.h(2),
                          horizontal: SizeConfig.w(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 1,
                              style: BorderStyle.solid,
                              color: AppStyle.primaryColor),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(16),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 1,
                              style: BorderStyle.solid,
                              color: AppStyle.disabledColor),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(16),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 1,
                              style: BorderStyle.solid,
                              color: AppStyle.primaryColor),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(16),
                          ),
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                        labelStyle: TextStyle(
                          fontSize: SizeConfig.h(14),
                        ),
                        errorStyle: TextStyle(fontSize: SizeConfig.h(14)),
                        fillColor: Colors.white70),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Row(
                  children: [
                    Text(
                      S.of(context).e_mail,
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: SizeConfig.h(60),
                      child: TextFormField(
                        readOnly: true,
                        initialValue:
                            sl<HomesettingsBloc>().settings?.user?.email ?? "",
                        style: AppStyle.vexa14.copyWith(color: Colors.black),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppStyle.disabledColor,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16))),
                      ),
                    ))
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(50),
                ),
                SizedBox(height: SizeConfig.w(10)),
                GestureDetector(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                    showDialog(
                        context: context,
                        builder: (_) => LocationDialoge()).then((value) {
                      if (value != null) {
                        setState(() {
                          selectedState = null;
                          selectedCity = null;
                          selectedCountry = value;
                        });
                        getStatesOfCurrentCountry(context);
                      }
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: AppStyle.greyColor),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.w(20),
                        horizontal: SizeConfig.w(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          S.of(context).country +
                              ' ' +
                              (selectedCountry != null
                                  ? (selectedCountry!.title ?? "")
                                  : ""),
                          style: TextStyle(
                            fontFamily: 'HelveticaNeueLTArabicBold',
                            color: AppStyle.textColor,
                            fontSize: SizeConfig.w(12),
                            height: 1,
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_down,
                          color: AppStyle.primaryColor,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: SizeConfig.w(10)),
                CustomRectangleDropdownWidget<StatesModel>(
                    title: S.of(context).state,
                    value: selectedState,
                    titleColor: states.isEmpty
                        ? AppStyle.disabledColor
                        : AppStyle.textColor,
                    onChanged: (v) {
                      setState(() {
                        selectedState = v;
                      });
                      getCitiesOfCurrentCountry(context);
                    },
                    children: states.isEmpty
                        ? null
                        : states.map((e) {
                            return DropdownMenuItem<StatesModel>(
                                value: e,
                                child: Text(
                                  (e.name ?? ""),
                                  style: AppStyle.vexa12
                                      .copyWith(fontSize: SizeConfig.h(14)),
                                ));
                          }).toList()),
                SizedBox(height: SizeConfig.w(10)),
                CustomRectangleDropdownWidget<CityModel>(
                    title: S.of(context).city,
                    value: selectedCity,
                    titleColor: cities.isEmpty
                        ? AppStyle.disabledColor
                        : AppStyle.textColor,
                    onChanged: (v) {
                      setState(() {
                        selectedCity = v;
                      });
                    },
                    children: cities.isEmpty
                        ? null
                        : cities.map((e) {
                            return DropdownMenuItem<CityModel>(
                                value: e,
                                child: Text(
                                  (e.name ?? ""),
                                  style: AppStyle.vexa12
                                      .copyWith(fontSize: SizeConfig.h(14)),
                                ));
                          }).toList()),
                SizedBox(height: SizeConfig.w(30)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: SizeConfig.h(50),
                    ),
                    if (isLoading)
                      AppLoader()
                    else
                      Expanded(
                          child: MainButton(
                              isOutlined: false,
                              onTap: () {
                                if (formKey.currentState?.validate() ?? false) {
                                  editData();
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    S.of(context).editProfile,
                                    style: AppStyle.vexa14.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ))),
                    SizedBox(
                      width: SizeConfig.h(50),
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.w(30)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  editData() async {
    setState(() {
      isLoading = true;
    });
    var data;
    if (imageFileList == null) {
      data = {
        "name": nameController.text,
        "country_code": phoneNumber?.countryCode,
        "mobile": phoneNumber?.number,
        "image[]": sl<HomesettingsBloc>().settings?.user?.coverImage,
        "country_id": selectedCountry?.id ?? null,
        "state_id": selectedState?.id ?? null,
        "city_id": selectedCity?.id ?? null,
        "bool": false
      };
    } else {
      data = {
        "name": nameController.text,
        "country_code": phoneNumber?.countryCode,
        "mobile": phoneNumber?.number,
        "image[]": File(imageFileList!.path),
        "country_id": selectedCountry?.id ?? null,
        "state_id": selectedState?.id ?? null,
        "city_id": selectedCity?.id ?? null,
        "bool": true
      };
    }

    final result = await UpdateProfile(sl()).call(UpdateProfileParams(data));
    setState(() {
      isLoading = false;
    });
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      sl<HomesettingsBloc>().settings?.user?.name = nameController.text;
      sl<HomesettingsBloc>().add(GetSettings(countryId: 166));
      Navigator.pop(context, true);
    });
  }

  Future<void> getStatesOfCurrentCountry(BuildContext context) async {
    final result =
        await GetStates(sl()).call(GetStatesParams(selectedCountry!.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (r.isEmpty) {}
      setState(() {
        states.clear();
        states.addAll(r);
        selectedCity = null;
        if (sl<HomesettingsBloc>().settings?.user?.state_id != null)
          states.forEach((element) {
            if (element.id == sl<HomesettingsBloc>().settings?.user?.state_id) {
              selectedState = element;
              getCitiesOfCurrentCountry(context);
            }
          });
      });
    });
  }

  Future<void> getCitiesOfCurrentCountry(BuildContext context) async {
    final result = await GetCities(sl()).call(GetCitiesParams(
        countryId: selectedCountry!.id, stateId: selectedState?.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      setState(() {
        cities.clear();
        cities.addAll(r);

        selectedCity = cities.isNotEmpty ? cities.first : null;
        if (sl<HomesettingsBloc>().settings?.user?.city_id != null)
          cities.forEach((element) {
            if (element.id == sl<HomesettingsBloc>().settings?.user?.city_id) {
              selectedCity = element;
            }
          });
      });
    });
  }
}
