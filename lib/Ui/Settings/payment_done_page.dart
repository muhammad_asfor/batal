import 'package:flutter/material.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/generated/l10n.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';

class PaymentDonePage extends StatelessWidget {
  const PaymentDonePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    sl<HomesettingsBloc>().add(GetSettings(countryId: 166));
    Future<bool> onWillPop() async {
      await Navigator.pushNamedAndRemoveUntil(
          context, '/base', (route) => false);
      return Future.value(true);
    }

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('assets/images/done.png'),
                  SizedBox(height: SizeConfig.w(20)),
                  Text(
                    S.of(context).great,
                    style: TextStyle(
                      color: AppStyle.darkTextColor,
                      fontSize: SizeConfig.w(22),
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      height: 1,
                    ),
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  Text(
                    S.of(context).you_can_now_add_ads,
                    style: TextStyle(
                      color: AppStyle.textColor,
                      fontSize: SizeConfig.w(16),
                      fontFamily: 'HelveticaNeueLTArabicRoman',
                      height: 1,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(
                  SizeConfig.w(40), 0, SizeConfig.w(40), SizeConfig.w(100)),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, '/addService');
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.h(22)),
                  decoration: BoxDecoration(
                    color: AppStyle.greyButtonColor,
                    borderRadius: BorderRadius.circular(SizeConfig.h(20)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        S.of(context).add_an_ad,
                        style: TextStyle(
                          color: AppStyle.primaryColor,
                          fontSize: SizeConfig.w(14),
                          fontFamily: 'HelveticaNeueLTArabicBold',
                          height: 1,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
