import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';

class PageDataPage extends StatelessWidget {
  final PageModel page;
  const PageDataPage({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          page.title,
          style: TextStyle(
            color: AppStyle.darkTextColor,
            fontSize: SizeConfig.w(18),
            fontFamily: 'HelveticaNeueLTArabicBold',
            height: 1,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: AppStyle.primaryColor,
            size: 15,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.all(SizeConfig.w(20)),
        child: SingleChildScrollView(
          child: Html(
            data: page.description ?? '',
            style: {
              "*": Style.fromTextStyle(TextStyle(
                color: AppStyle.darkTextColor,
                fontSize: SizeConfig.w(18),
                fontFamily: 'HelveticaNeueLTArabicRoman',
                height: 2,
              ))
            },
          ),
        ),
      ),
    );
  }
}
