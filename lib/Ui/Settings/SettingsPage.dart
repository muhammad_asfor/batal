import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Ui/Settings/blocs/bloc/settings_bloc.dart';
import 'package:tajra/Ui/Settings/widgets/custom_drop_down_widget.dart';
import 'package:tajra/Ui/Settings/widgets/custom_navigate_widget.dart';
import 'package:tajra/Ui/Settings/widgets/custom_switch_tile.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';
import 'package:url_launcher/url_launcher.dart';
import '/App/App.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dio/dio.dart';

import '/data/sharedPreferences/SharedPrefHelper.dart';
import '/generated/l10n.dart';
import '../../injections.dart';
import 'package:restart_app/restart_app.dart';
part 'widgets/profile_tutorial.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

Map? preferences;

class _SettingsPageState extends State<SettingsPage> {
  bool showLanguages = false;
  List<String> languages = [];
  final SettingsBloc settingsBloc = SettingsBloc();

  String selectedLanguage = "ar";
  String? selectedCurrency;
  bool addOrEdit = false;
  bool veiw = false;
  deleteAc() async {
    try {
      final result = await sl<Dio>().post("api/user/remove");
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    if (!sl<AuthBloc>().isGuest) settingsBloc.add(GetPagesEvent());
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      languages = [
        'ar',
        'en',
      ];
    });
    getPreferences();
    getLanguage();
    if (!sl<AuthBloc>().isGuest) {
      Getpackages(sl())
          .call(NoParams())
          .then((value) => value.fold((l) {}, (r) {
                if (r.data != null) addOrEdit = true;
                setState(() {
                  veiw = true;
                });
              }));
    }
  }

  getPreferences() async {
    final result = await GetPreferences(sl()).call(NoParams());
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (mounted)
        setState(() {
          preferences = r;
        });
    });
  }

  getLanguage() async {
    selectedLanguage =
        await sl<PrefsHelper>().loadLangFromSharedPref() ?? App.defaultLanguage;
    selectedCurrency = await sl<PrefsHelper>().loadCurrencyFromSharedPref();
    if (mounted) setState(() {});
  }

  restart(var v) async {
    selectedLanguage = await v;
    await sl<PrefsHelper>().saveLangToSharedPref(v);
    App.setLocale(context, selectedLanguage);
  }

  @override
  Widget build(BuildContext context) {
    final homeModel = sl<HomesettingsBloc>().settings!;
    if (selectedCurrency == null &&
        homeModel.currencies != null &&
        homeModel.currencies!.isNotEmpty)
      selectedCurrency = homeModel.currencies![0].code;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).settings,
          style: TextStyle(
            color: AppStyle.darkTextColor,
            fontSize: SizeConfig.w(18),
            fontFamily: 'HelveticaNeueLTArabicBold',
            height: 1,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(20)),
        child: BlocBuilder(
            bloc: settingsBloc,
            builder: (context, SettingsState state) {
              return ListView(
                shrinkWrap: true,
                children: [
                  CustomDropdownWidget<String>(
                    initialValue: selectedLanguage,
                    title: S.of(context).language,
                    children: languages,
                    onChanged: (dynamic v) async {
                      await restart(v);
                      Restart.restartApp();
                    },
                  ),
                  SizedBox(height: SizeConfig.w(20)),
                  if (homeModel.currencies!.isNotEmpty &&
                      homeModel.currencies!.length != 1)
                    CustomDropdownWidget<Currency>(
                      initialValue: homeModel.currencies?.firstWhere(
                              (element) => element.code == selectedCurrency) ??
                          homeModel.currencies![0],
                      title: S.of(context).currency,
                      children: homeModel.currencies!,
                      onChanged: (v) {},
                    ),
                  SizedBox(height: SizeConfig.w(20)),
                  CustomSwitchTile(title: S.of(context).notifications),
                  SizedBox(height: SizeConfig.w(20)),
                  CustomNavigateWidget(
                    title: S.of(context).personal_profile,
                    showIcon: false,
                    onTap: () {
                      Navigator.pushNamed(context, "/personalProfile");
                    },
                  ),
                  SizedBox(height: SizeConfig.w(10)),
                  if (sl<HomesettingsBloc>().settings?.user?.isTechnician ==
                      false)
                    CustomNavigateWidget(
                      title: S.of(context).promote_account,
                      onTap: () {
                        Navigator.pushNamed(context, "/cheakpage");
                      },
                    ),
                  if (veiw)
                    if (addOrEdit)
                      CustomNavigateWidget(
                        title: S.of(context).mypackage,
                        onTap: () {
                          Navigator.pushNamed(context, "/MyPackagePage");
                        },
                      ),
                  SizedBox(height: SizeConfig.w(10)),
                  if (state is PagesReady)
                    ...state.pages
                        .map((e) => Column(
                              children: [
                                CustomNavigateWidget(
                                  title: e.title,
                                  onTap: () async {
                                    if (e.title == 'اعلن لدينا') {
                                      final url = "https://wa.me/96899719002";
                                      // if (await canLaunch(url)) {
                                      await launch(url);
                                      // } else {
                                      //   throw 'Could not launch $url';
                                      // }
                                    } else {
                                      Navigator.pushNamed(context, "/pageData",
                                          arguments: e);
                                    }
                                  },
                                ),
                                SizedBox(height: SizeConfig.h(10)),
                              ],
                            ))
                        .toList(),
                  CustomNavigateWidget(
                    title: S.of(context).signOut,
                    color: AppStyle.redColor,
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) => Center(
                                child: SizedBox(
                                  height: SizeConfig.h(275),
                                  width: SizeConfig.h(350),
                                  child: Material(
                                    borderRadius: BorderRadius.circular(15),
                                    child: Padding(
                                      padding: const EdgeInsets.all(12.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                "assets/logo.png",
                                                height: SizeConfig.h(100),
                                                width: SizeConfig.h(100),
                                              ),
                                            ],
                                          ),
                                          Spacer(flex: 1),
                                          Text(
                                            S.of(context).signOut,
                                            style: AppStyle.vexa20
                                                .copyWith(color: Colors.black),
                                          ),
                                          SizedBox(
                                            height: SizeConfig.h(12),
                                          ),
                                          Text(
                                            S.of(context).singupDe,
                                            style: AppStyle.vexa16.copyWith(
                                                color: Colors.black54),
                                          ),
                                          Spacer(
                                            flex: 2,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    S.of(context).back,
                                                    style: AppStyle.vexa14
                                                        .copyWith(
                                                            color:
                                                                Colors.black54),
                                                  )),
                                              TextButton(
                                                  onPressed: () async {
                                                    sl<AuthBloc>()
                                                        .add(LogoutEvent());
                                                    sl<HomesettingsBloc>()
                                                        .settings
                                                        ?.user = null;
                                                    // Navigator.pop(context);
                                                    Navigator
                                                        .pushReplacementNamed(
                                                            context, "/login",
                                                            arguments: false);
                                                  },
                                                  child: Text(
                                                    S.of(context).continuee,
                                                    style: AppStyle.vexa14
                                                        .copyWith(
                                                            color:
                                                                Colors.black),
                                                  ))
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ));
                    },
                  ),
                  SizedBox(height: SizeConfig.h(10)),
                  CustomNavigateWidget(
                    title: S.of(context).deleteAc,
                    color: AppStyle.redColor,
                    onTap: () async {
                      showDialog(
                          context: context,
                          builder: (context) => Center(
                                child: SizedBox(
                                  height: SizeConfig.h(275),
                                  width: SizeConfig.h(350),
                                  child: Material(
                                    borderRadius: BorderRadius.circular(15),
                                    child: Padding(
                                      padding: const EdgeInsets.all(12.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                "assets/logo.png",
                                                height: SizeConfig.h(100),
                                                width: SizeConfig.h(100),
                                              ),
                                            ],
                                          ),
                                          Spacer(flex: 1),
                                          Text(
                                            S.of(context).deleteAc,
                                            style: AppStyle.vexa20
                                                .copyWith(color: Colors.black),
                                          ),
                                          SizedBox(
                                            height: SizeConfig.h(12),
                                          ),
                                          Text(
                                            S.of(context).deleteAcDe,
                                            style: AppStyle.vexa16.copyWith(
                                                color: Colors.black54),
                                          ),
                                          Spacer(
                                            flex: 2,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    S.of(context).back,
                                                    style: AppStyle.vexa14
                                                        .copyWith(
                                                            color:
                                                                Colors.black54),
                                                  )),
                                              TextButton(
                                                  onPressed: () async {
                                                    sl<AuthBloc>()
                                                        .add(LogoutEvent());
                                                    sl<HomesettingsBloc>()
                                                        .settings
                                                        ?.user = null;
                                                    // Navigator.pop(context);
                                                    await deleteAc();
                                                    Navigator
                                                        .pushReplacementNamed(
                                                            context, "/login",
                                                            arguments: false);
                                                  },
                                                  child: Text(
                                                    S.of(context).continuee,
                                                    style: AppStyle.vexa14
                                                        .copyWith(
                                                            color:
                                                                Colors.black),
                                                  ))
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ));
                    },
                  ),
                  SizedBox(height: SizeConfig.w(10)),
                  Text(
                    S.of(context).version_number +
                        '\n${homeModel.appLatestVersion}',
                    style: TextStyle(
                      color: Color(0xFFB9B9FD),
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      fontSize: SizeConfig.w(14),
                      height: 2,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: SizeConfig.w(40)),
                ],
              );
            }),
      ),
    );
  }
}

void shareApp() {
  if (Platform.isIOS)
    actionShare(preferences!["apple_app_url"], SizeConfig.screenWidth);
  else
    actionShare(preferences!["android_app_url"], SizeConfig.screenWidth);
}

bool isLanguageActive(String slug, homeModel) {
  return (homeModel.languages.contains(Languages(slug: slug)));
}
