import 'package:flutter/material.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/AppErrorWidget.dart';
import 'package:tajra/App/Widgets/AppLoader.dart';
import 'package:tajra/App/Widgets/EmptyPlacholder.dart';
import 'package:tajra/Ui/Settings/blocs/packages_bloc/packages_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tajra/Ui/Settings/pay.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/constants.dart';
import 'package:tajra/inner_layer/Settings/domain/usecases/subscribe_use_case.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';

class PackagesPage extends StatefulWidget {
  final String fullName;
  final String? countryCode;
  final String? mobile;
  final int? countryId;
  final int? stateId;
  final int? cityId;

  const PackagesPage(
      {Key? key,
      required this.fullName,
      this.countryCode,
      this.mobile,
      this.countryId,
      this.stateId,
      this.cityId})
      : super(key: key);

  @override
  _PackagesPageState createState() => _PackagesPageState();
}

class _PackagesPageState extends State<PackagesPage> {
  final PackagesBloc bloc = PackagesBloc();
  bool isLoading = false;

  @override
  void initState() {
    bloc.add(GetPackagesEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> onWillPop() async {
      await Navigator.pushNamedAndRemoveUntil(
          context, '/base', (route) => false);
      return Future.value(true);
    }

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              S.of(context).packages,
              style: TextStyle(
                color: AppStyle.darkTextColor,
                fontSize: SizeConfig.w(18),
                fontFamily: 'HelveticaNeueLTArabicBold',
                height: 1,
              ),
            ),
            leading: GestureDetector(
              onTap: () async {
                await Navigator.pushNamedAndRemoveUntil(
                    context, '/base', (route) => false);
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: AppStyle.primaryColor,
                size: 15,
              ),
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: BlocBuilder(
              bloc: bloc,
              builder: (context, PackagesState state) {
                if (state is ErrorInPackages) {
                  return AppErrorWidget(text: state.error);
                }
                if (state is LoadingPackages) {
                  return AppLoader();
                }
                if (state is PackagesReady) {
                  return isLoading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.packages.length,
                          padding: EdgeInsets.all(SizeConfig.w(32)),
                          itemBuilder: (context2, index) {
                            return Padding(
                              padding:
                                  EdgeInsets.only(bottom: SizeConfig.w(14)),
                              child: Container(
                                height: 191,
                                decoration: index == 0
                                    ? BoxDecoration(
                                        border: Border.all(
                                            color: AppStyle.greyColor),
                                        borderRadius: BorderRadius.circular(6),
                                      )
                                    : BoxDecoration(
                                        border: Border.all(
                                            color: AppStyle.greyColor),
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                padding: EdgeInsets.symmetric(
                                    horizontal: SizeConfig.w(20)),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      state.packages[index].title ?? '',
                                      style: TextStyle(
                                        color: AppStyle.darkTextColor,
                                        fontSize: SizeConfig.w(14),
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        height: 1,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      state.packages[index].priceText ?? '',
                                      style: TextStyle(
                                        color: AppStyle.lightColor,
                                        fontSize: SizeConfig.w(20),
                                        fontFamily: 'HelveticaNeueLTArabicBold',
                                        height: 1,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      state.packages[index].description ?? '',
                                      style: TextStyle(
                                        color: AppStyle.primaryColor,
                                        fontSize: SizeConfig.w(14),
                                        fontFamily:
                                            'HelveticaNeueLTArabicRoman',
                                        height: 1,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        setState(() {
                                          isLoading = true;
                                        });
                                        // Navigator.pushNamed(context, '/payment');
                                        final result =
                                            await SubscribeUseCase(sl())
                                                .call(SubscribeParams({
                                          'package_id':
                                              state.packages[index].id,
                                          'name': widget.fullName,
                                          'country_code': widget.countryCode,
                                          'mobile': widget.mobile,
                                          'country_id': widget.countryId,
                                          'state_id': widget.stateId,
                                          'city_id': widget.cityId,
                                        }));
                                        result.fold((l) {
                                          setState(() {
                                            isLoading = false;
                                          });
                                          AppSnackBar.show(
                                              context,
                                              l.errorMessage.substring(l
                                                              .errorMessage
                                                              .length -
                                                          3) ==
                                                      '422'
                                                  ? S
                                                      .of(context)
                                                      .subscription_request
                                                  : l.errorMessage,
                                              ToastType.Error);
                                        }, (r) {
                                          setState(() {
                                            isLoading = false;
                                          });

                                          if (r.data!.payUrl == null) {
                                            AppSnackBar.show(
                                                context,
                                                S
                                                    .of(context)
                                                    .subscribedSuccessfully,
                                                ToastType.Success);
                                            Navigator.pushReplacementNamed(
                                                context, '/paymentDone');
                                          } else {
                                            session_id = r.data!.sessionId!;
                                            pay_id = r.data!.payId!;

                                            AppSnackBar.show(
                                                context,
                                                S
                                                    .of(context)
                                                    .subscribedSuccessfully,
                                                ToastType.Success);
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      WebViewExample(
                                                          urlpay:
                                                              r.data!.payUrl!)),
                                            );
                                          }
                                        });
                                      },
                                      child: Container(
                                        width: double.maxFinite,
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              colors: [
                                                AppStyle.primaryColor,
                                                AppStyle.secondaryColor,
                                              ],
                                              begin: Alignment.topLeft,
                                              end: Alignment.bottomRight),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        padding: EdgeInsets.symmetric(
                                            vertical: SizeConfig.w(10)),
                                        child: Text(
                                          S.of(context).choose_the_package,
                                          style: TextStyle(
                                            fontFamily:
                                                'HelveticaNeueLTArabicBold',
                                            fontSize: SizeConfig.w(12),
                                            color: AppStyle.whiteColor,
                                            height: 1,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                }
                return Container();
              })),
    );
  }
}
