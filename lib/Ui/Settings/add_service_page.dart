import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Ui/Home/widgets/service_card_widget.dart';
import 'package:tajra/Ui/Settings/widgets/custom_titled_text_field.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';
import '../../generated/l10n.dart';
import '../worker_profile/widgets/custom_radio_tile.dart';

class AddServicePage extends StatefulWidget {
  const AddServicePage({Key? key}) : super(key: key);

  @override
  _AddServicePageState createState() => _AddServicePageState();
}

int? groupValue;
int? groupValue2;
String? id;

class _AddServicePageState extends State<AddServicePage> {
  List<XFile>? _imageFileList;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  List<String> imagenet = [];
  List<String> imagedata = [];
  final serviceController = TextEditingController();
  final descController = TextEditingController();
  bool isLoading = false;
  bool isLoadingser = true;

  final ImagePicker _picker = ImagePicker();

  Future<void> _onImageButtonPressed(ImageSource source,
      {BuildContext? context}) async {
    try {
      final List<XFile>? pickedFileList = await _picker.pickMultiImage();
      setState(() {
        if (_imageFileList == null)
          _imageFileList = pickedFileList;
        else
          _imageFileList!.addAll(pickedFileList ?? []);
      });
    } catch (e) {}
  }

  bool addOrEdit = false;
  bool veiw = false;
  bool isvew = false;
  late List<Category> categories = [];
  @override
  void initState() {
    sl<HomesettingsBloc>().add(GetSettings(countryId: 166));
    var settings = sl<HomesettingsBloc>().settings!;
    GetCategories(sl()).call(NoParams()).then((value) => value.fold((l) {
          setState(() {
            isLoadingser = false;
            if (sl<HomesettingsBloc>().settings?.user?.grade == 0) {
              for (int i = 0; i < settings.categories!.length; i++) {
                if (settings.categories![i].id == 62) {
                  categories.add(settings.categories![i]);
                  groupValue = 62;
                  groupValue2 = 62;
                }
              }
            } else {
              categories = settings.categories!;
            }
          });
        }, (r) {
          setState(() {
            isLoadingser = false;
            if (sl<HomesettingsBloc>().settings?.user?.grade == 0) {
              for (int i = 0; i < r.length; i++) {
                if (r[i].id == 62) {
                  categories.add(r[i]);
                  groupValue = 62;
                  groupValue2 = 62;
                }
              }
            } else {
              categories = r;
            }
          });
        }));

    GetUserPost(sl()).call(NoParams()).then((value) => value.fold((l) {
          setState(() {
            isvew = true;
            id = null;
          });
        }, (r) {
          setState(() {
            isvew = true;
            serviceController.text = r.title!;
            id = r.id.toString();
            descController.text = r.description!;
            categories.forEach((element) {
              if (element.title == r.categoryText!) {
                groupValue = element.id;
                groupValue2 = element.id;
              }
            });
            imagenet = r.imagesBag!;
            for (int i = 0; i < r.imagesData!.length; i++) {
              imagedata.add("posts/" + r.imagesData![i].substring(6));
            }
          });
        }));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> onWillPop() async {
      await Navigator.pushNamedAndRemoveUntil(
          context, '/base', (route) => false);
      return Future.value(true);
    }

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            isvew
                ? id == null
                    ? S.of(context).add_service
                    : S.of(context).editser
                : "",
            style: TextStyle(
              color: AppStyle.darkTextColor,
              fontSize: SizeConfig.w(18),
              fontFamily: 'HelveticaNeueLTArabicBold',
              height: 1,
            ),
          ),
          leading: GestureDetector(
            onTap: () async {
              await Navigator.pushNamedAndRemoveUntil(
                  context, '/base', (route) => false);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: AppStyle.primaryColor,
              size: 15,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(20)),
          child: Form(
            key: formKey,
            child: ListView(
              shrinkWrap: true,
              children: [
                CustomTitledTextField(
                  controller: serviceController,
                  title: S.of(context).service_name,
                  validator: (v) {
                    if (v != null) {
                      if (v == '') {
                        return S.of(context).serviceNameRequired;
                      }
                    }
                    return null;
                  },
                ),
                SizedBox(height: SizeConfig.w(10)),
                CustomTitledTextField(
                  controller: descController,
                  title: S.of(context).service_description,
                  validator: (v) {
                    if (v != null) {
                      if (v == '') {
                        return S.of(context).descriptionRequired;
                      }
                    }
                    return null;
                  },
                ),
                SizedBox(height: SizeConfig.w(10)),
                SizedBox(height: SizeConfig.w(10)),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: AppStyle.greyColor, width: 2),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: SizeConfig.w(15)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            S.of(context).service_photos,
                            style: TextStyle(
                              fontFamily: 'HelveticaNeueLTArabicBold',
                              color: AppStyle.primaryColor,
                              height: 1,
                              fontSize: SizeConfig.h(12),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _imageFileList = null;
                                imagenet = [];
                                imagedata = [];
                              });
                            },
                            child: Text(
                              S.of(context).cancel,
                              style: TextStyle(
                                fontFamily: 'HelveticaNeueLTArabicBold',
                                color: AppStyle.redColor,
                                height: 1,
                                fontSize: SizeConfig.h(12),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      imagenet.isNotEmpty &&
                              _imageFileList != null &&
                              _imageFileList!.isNotEmpty
                          ? Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.w(20)),
                              child: Wrap(children: [
                                for (int i = 0; i < _imageFileList!.length; i++)
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: SizeConfig.w(7),
                                        vertical: SizeConfig.w(5)),
                                    child: SizedBox(
                                        width: SizeConfig.w(75),
                                        height: SizeConfig.w(75),
                                        child: Image.file(
                                          File(_imageFileList![i].path),
                                          fit: BoxFit.fill,
                                        )),
                                  ),
                                for (int j = 0; j < imagenet.length; j++)
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: SizeConfig.w(7),
                                        vertical: SizeConfig.w(5)),
                                    child: SizedBox(
                                        width: SizeConfig.w(75),
                                        height: SizeConfig.w(75),
                                        child: Image.network(
                                          imagenet[j],
                                          fit: BoxFit.fill,
                                        )),
                                  ),
                              ]),
                            )
                          : imagenet.isEmpty
                              ? _imageFileList != null &&
                                      _imageFileList!.isNotEmpty
                                  ? Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: SizeConfig.w(20)),
                                      child: Wrap(
                                        children: _imageFileList!
                                            .map(
                                              (e) => Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: SizeConfig.w(7),
                                                    vertical: SizeConfig.w(5)),
                                                child: SizedBox(
                                                    width: SizeConfig.w(75),
                                                    height: SizeConfig.w(75),
                                                    child: Image.file(
                                                      File(e.path),
                                                      fit: BoxFit.fill,
                                                    )),
                                              ),
                                            )
                                            .toList(),
                                      ),
                                    )
                                  : SizedBox()
                              : Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: SizeConfig.w(20)),
                                  child: Wrap(
                                    children: imagenet
                                        .map(
                                          (e) => Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: SizeConfig.w(7),
                                                vertical: SizeConfig.w(5)),
                                            child: SizedBox(
                                                width: SizeConfig.w(75),
                                                height: SizeConfig.w(75),
                                                child: Image.network(
                                                  e,
                                                  fit: BoxFit.fill,
                                                )),
                                          ),
                                        )
                                        .toList(),
                                  ),
                                ),
                      SizedBox(height: SizeConfig.w(15)),
                    ],
                  ),
                ),
                SizedBox(height: SizeConfig.w(10)),
                GestureDetector(
                  onTap: () {
                    _onImageButtonPressed(
                      ImageSource.gallery,
                      context: context,
                    );
                  },
                  child: DottedBorder(
                    borderType: BorderType.RRect,
                    radius: Radius.circular(6),
                    color: AppStyle.greyColor,
                    strokeWidth: 2,
                    dashPattern: [6, 6],
                    padding: EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: AppStyle.greyDark,
                              width: 2,
                            ),
                          ),
                          child: Icon(
                            Icons.add,
                            color: AppStyle.greyDark,
                          ),
                        ),
                        SizedBox(height: SizeConfig.w(5)),
                        Text(
                          S.of(context).upload_image,
                          style: TextStyle(
                            fontFamily: 'HelveticaNeueLTArabicBold',
                            fontSize: SizeConfig.w(12),
                            color: AppStyle.greyDark,
                            height: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: SizeConfig.w(20)),
                isLoadingser
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.of(context).choose_service,
                            style: TextStyle(
                              fontFamily: 'HelveticaNeueLTArabicBold',
                              fontSize: SizeConfig.w(16),
                              color: AppStyle.primaryColor,
                              height: 1,
                            ),
                          ),
                          SizedBox(height: SizeConfig.w(20)),
                          GridView.count(
                            shrinkWrap: true,
                            padding: EdgeInsets.only(
                                top: SizeConfig.w(20),
                                left: SizeConfig.w(20),
                                right: SizeConfig.w(20),
                                bottom: SizeConfig.w(100)),
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 3,
                            crossAxisSpacing: SizeConfig.w(10),
                            mainAxisSpacing: SizeConfig.w(10),
                            children: categories.map((e) {
                              bool isShow = false;
                              e.subCategories!.forEach((element) {
                                if (element.id == groupValue) {
                                  isShow = true;
                                }
                              });
                              return ServiceCardWidget(
                                svgPath: e.coverImage,
                                service: e.title,
                                isFocused: e.id == groupValue
                                    ? true
                                    : isShow
                                        ? true
                                        : false,
                                onTap: () {
                                  setState(() {
                                    groupValue = e.id;
                                    if (e.subCategories!.isNotEmpty)
                                      showDialog(
                                          context: context,
                                          builder: (_) {
                                            return StatefulBuilder(
                                              builder: (context, setState) =>
                                                  AlertDialog(
                                                scrollable: true,
                                                title: Text(S
                                                    .of(context)
                                                    .choose_service),
                                                content: Container(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      ...e.subCategories
                                                              ?.map((ee) {
                                                            return Column(
                                                              children: [
                                                                CustomRadioTile(
                                                                  value: ee.id,
                                                                  borderColor:
                                                                      Colors
                                                                          .transparent,
                                                                  groupValue:
                                                                      groupValue2 ??
                                                                          999,
                                                                  reason:
                                                                      ee.title,
                                                                  onChanged: (int
                                                                      newValue) {
                                                                    setState(
                                                                        () {
                                                                      groupValue =
                                                                          newValue;
                                                                      groupValue2 =
                                                                          newValue;
                                                                    });
                                                                  },
                                                                ),
                                                              ],
                                                            );
                                                          }).toList() ??
                                                          [],
                                                      // ...settings.categories
                                                      //         ?.map((e) => CustomCheckboxTile(title: e.title))
                                                      //         .toList() ??
                                                      //     [],
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                          });
                                  });
                                },
                              );
                            }).toList(),
                          ),

                          // ...settings.categories
                          //         ?.map((e) => CustomCheckboxTile(title: e.title))
                          //         .toList() ??
                          //     [],
                        ],
                      ),
                isLoading
                    ? Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 1.4,
                        ),
                      )
                    : GestureDetector(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          if (formKey.currentState?.validate() ?? false) {
                            addPost();
                          }
                        },
                        child: Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  AppStyle.primaryColor,
                                  AppStyle.secondaryColor,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding:
                              EdgeInsets.symmetric(vertical: SizeConfig.w(15)),
                          child: Text(
                            isvew
                                ? id == null
                                    ? S.of(context).add_service
                                    : S.of(context).editser
                                : "",
                            style: TextStyle(
                              fontFamily: 'HelveticaNeueLTArabicBold',
                              fontSize: SizeConfig.w(14),
                              color: AppStyle.whiteColor,
                              height: 1,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                SizedBox(height: SizeConfig.w(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  final debouncer = Debouncer(milliseconds: 500);

  void addPost() {
    setState(() {
      isLoading = true;
    });
    List<File> images = [];
    for (var e in _imageFileList ?? []) {
      images.add(File(e.path));
    }
    debouncer.run(() {
      AddPost(sl())
          .call(AddPostParams(
              parentId: groupValue ?? 1,
              title: serviceController.text,
              description: descController.text,
              images: images,
              id: id,
              imagesold: imagedata))
          .then((value) => value.fold((l) {
                AppSnackBar.show(
                    context,
                    l.errorMessage.substring(l.errorMessage.length - 3) == '422'
                        ? S.of(context).already_have_a_service
                        : l.errorMessage.substring(l.errorMessage.length - 3) ==
                                '403'
                            ? S.of(context).please_subscribe_first
                            : l.errorMessage,
                    ToastType.Error);
                setState(() {
                  isLoading = false;
                });
              }, (r) async {
                await Navigator.pushNamedAndRemoveUntil(
                    context, '/base', (route) => false);
                AppSnackBar.show(context, S.of(context).addedSuccessfully,
                    ToastType.Success);
              }));
    });
  }
}
