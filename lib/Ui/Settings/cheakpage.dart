import 'package:flutter/material.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/generated/l10n.dart';

import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';

class cheakpage extends StatefulWidget {
  cheakpage({Key? key}) : super(key: key);

  @override
  _cheakpageState createState() => _cheakpageState();
}

bool showedBaseTour = false;

class _cheakpageState extends State<cheakpage> {
  int countryId = 166;
  @override
  void initState() {
    super.initState();
    sl<HomesettingsBloc>().add(GetSettings(countryId: countryId));
  }

  setpage() {
    Future.delayed(const Duration(milliseconds: 2000), () async {
      if (sl<HomesettingsBloc>().settings?.user?.isTechnician ?? false) {
        await Navigator.popAndPushNamed(context, '/addService');
      } else {
        await Navigator.popAndPushNamed(context, '/promote_account');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setpage();
    return Scaffold(
        body: Center(
      child: CircularProgressIndicator(),
    ));
  }
}
