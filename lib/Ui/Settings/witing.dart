import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:tajra/constants.dart';
import 'package:tajra/generated/l10n.dart';

class Witing extends StatefulWidget {
  Witing({Key? key}) : super(key: key);
  @override
  WitingState createState() => WitingState();
}

class WitingState extends State<Witing> {
  checkout() async {
    Future.delayed(const Duration(milliseconds: 200), () async {
      try {
        await sl<Dio>().post("api/payments/thawani/success-callback",
            data: {"session_id": session_id, "pay_id": pay_id});
        Navigator.pushReplacementNamed(context, '/paymentDone');
      } on DioError catch (e) {
        AppSnackBar.show(
            context, S.of(context).payment_process, ToastType.Error);

        Future.delayed(const Duration(milliseconds: 2000), () {
          Navigator.pop(
            context,
          );
          Future.delayed(const Duration(milliseconds: 200), () {
            Navigator.pop(
              context,
            );
          });
        });
      } catch (e) {
        AppSnackBar.show(
            context, S.of(context).payment_process, ToastType.Error);

        Future.delayed(const Duration(milliseconds: 2000), () {
          Navigator.pop(
            context,
          );
          Future.delayed(const Duration(milliseconds: 200), () {
            Navigator.pop(
              context,
            );
          });
        });
      }
    });
  }

  @override
  void initState() {
    checkout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
