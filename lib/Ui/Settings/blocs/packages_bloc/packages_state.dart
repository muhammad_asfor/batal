part of 'packages_bloc.dart';

@immutable
abstract class PackagesState {}

class PackagesInitial extends PackagesState {}

class LoadingPackages extends PackagesState {}

class PackagesReady extends PackagesState {
  final List<Package> packages;
  PackagesReady(this.packages);
}

class ErrorInPackages extends PackagesState {
  final String error;
  ErrorInPackages(this.error);
}
