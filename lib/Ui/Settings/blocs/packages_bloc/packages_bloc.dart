import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:tajra/inner_layer/Settings/domain/entities/package.dart';
import 'package:tajra/inner_layer/Settings/domain/usecases/get_packages_use_case.dart';

import '../../../../injections.dart';

part 'packages_event.dart';
part 'packages_state.dart';

class PackagesBloc extends Bloc<PackagesEvent, PackagesState> {
  PackagesBloc() : super(PackagesInitial()) {
    on<GetPackagesEvent>(_onGetMyPoint);
  }

  Future<void> _onGetMyPoint(
      GetPackagesEvent event, Emitter<PackagesState> emit) async {
    emit(LoadingPackages());
    final result = await GetPackagesUseCase(sl()).call(NoParams());
    result.fold((l) {
      emit(ErrorInPackages(l.errorMessage));
    }, (r) {
      emit(PackagesReady(r));
    });
  }
}
