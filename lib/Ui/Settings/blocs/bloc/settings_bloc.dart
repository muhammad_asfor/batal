import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';

import '../../../../injections.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc() : super(PagesInitial()) {
    on<GetPagesEvent>(_onGetPagesEvent);
  }

  Future<void> _onGetPagesEvent(
      GetPagesEvent event, Emitter<SettingsState> emit) async {
    emit(LoadingPages());
    final result = await GetPages(sl()).call(NoParams());
    result.fold((l) {
      emit(ErrorInPages(l.errorMessage));
    }, (r) {
      emit(PagesReady(r));
    });
  }
}
