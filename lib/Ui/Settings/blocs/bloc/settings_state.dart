part of 'settings_bloc.dart';

@immutable
abstract class SettingsState {}

class PagesInitial extends SettingsState {}

class LoadingPages extends SettingsState {}

class PagesReady extends SettingsState {
  final List<PageModel> pages;
  PagesReady(this.pages);
}

class ErrorInPages extends SettingsState {
  final String error;
  ErrorInPages(this.error);
}
