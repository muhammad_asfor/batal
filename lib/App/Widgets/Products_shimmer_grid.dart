import 'package:flutter/material.dart';
import '/App/Widgets/ShimmerProductCard.dart';
import '/Utils/SizeConfig.dart';
class ProductsShimmerGrid extends StatefulWidget {
  ProductsShimmerGrid({Key? key, required this.returnCustomScrollView})
      : super(key: key);
  final bool returnCustomScrollView;
  @override
  _ProductsShimmerGridState createState() => _ProductsShimmerGridState();
}

class _ProductsShimmerGridState extends State<ProductsShimmerGrid> {
  @override
  Widget build(BuildContext context) {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: 15,
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(10)),
        itemBuilder: (context, index) {
          return ProductShimmerCard();
        },
      );
  }
}
