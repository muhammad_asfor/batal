import 'package:flutter/material.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';

import 'package:shimmer/shimmer.dart';

class ProductShimmerCard extends StatefulWidget {
  ProductShimmerCard({Key? key}) : super(key: key);

  @override
  _ProductShimmerCardState createState() => _ProductShimmerCardState();
}

class _ProductShimmerCardState extends State<ProductShimmerCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.w(6)),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            bottomLeft: Radius.circular(16),
            bottomRight: Radius.circular(16),
          ),
          // color: Color(0xFF29ABE2).withOpacity(0.05),
          border: Border.all(
            color: AppStyle.greyColor,
            // color: Color(0xFF29ABE2),
            width: 2,
          ),
        ),
        padding: EdgeInsets.all(SizeConfig.h(12)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    height: 64,
                    width: 64,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                      color: Colors.grey,
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(SizeConfig.h(5)),
                        ),
                        width: SizeConfig.h(60),
                        height: SizeConfig.h(10),
                        // fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(height: SizeConfig.w(5)),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(SizeConfig.h(5)),
                        ),
                        width: SizeConfig.h(60),
                        height: SizeConfig.h(10),
                        // fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(height: SizeConfig.w(5)),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(SizeConfig.h(5)),
                        ),
                        width: SizeConfig.h(60),
                        height: SizeConfig.h(10),
                        // fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(SizeConfig.h(5)),
                ),
                width: SizeConfig.h(60),
                height: SizeConfig.h(40),
                // fit: BoxFit.fill,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
