import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:progiom_cms/core.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

class ServiceProviderCard extends StatefulWidget {
  final Service? service;

  final bool forPointSale;

  ServiceProviderCard({Key? key, this.service, this.forPointSale: false})
      : super(key: key);

  @override
  _ServiceProviderCardState createState() => _ServiceProviderCardState();
}

class _ServiceProviderCardState extends State<ServiceProviderCard> {
  bool loadingCart = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigator.pushNamed(
        //   context,
        //   "/productDetails",
        //   arguments: {"id": product.id.toString(), "goToOptions": false},
        // );
      },
      child: Container(
        height: 231,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),
          border: Border.all(
            color: AppStyle.greyColor,
            width: 2,
          ),
        ),
        padding: EdgeInsets.fromLTRB(10, 30, 10, 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                height: 64,
                width: 64,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                  ),
                  // color: Color(0xFFacedfb),
                  image: DecorationImage(
                    image: NetworkImage(widget.service!.coverImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Text(
              widget.service!.user?.name ?? '',
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicBold',
                color: AppStyle.textColor,
                fontSize: 14,
              ),
            ),
            Text(
              widget.service!.categoryText ?? '',
              style: TextStyle(
                fontFamily: 'HelveticaNeueLTArabicRoman',
                color: AppStyle.greyTextColor,
                fontSize: 10,
              ),
            ),
            // Row(
            //   children: [
            //     SvgPicture.asset('assets/icons/rate.svg'),
            //     SizedBox(width: 5),
            //     Text('4.5'),
            //   ],
            // ),
            Center(
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    "/workerProfile",
                    arguments: widget.service!.id.toString(),
                  );
                },
                child: Container(
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppStyle.greyButtonColor,
                  ),
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    S.of(context).watch,
                    style: TextStyle(
                      fontFamily: 'HelveticaNeueLTArabicBold',
                      color: AppStyle.primaryColor,
                      fontSize: 10,
                      height: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
