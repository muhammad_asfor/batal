import 'package:flutter/material.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:tajra/Ui/Search/widgets/search_bar.dart';
import '../../Utils/SizeConfig.dart';
import '../../Utils/Style.dart';

import '../../generated/l10n.dart';

final GlobalKey searchNavKey = GlobalKey();

class CustomAppBar extends StatefulWidget {
  const CustomAppBar({
    Key? key,
    required this.name,
    required this.onChangeCountry,
    this.country,
    this.state,
  });

  final String name;
  final Function onChangeCountry;
  final CountryModel? country;
  final StatesModel? state;

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  CountryModel? selectedCountry;
  final searchController = TextEditingController();

  StatesModel? selectedState;
  CityModel? selectedCity;
  bool isLoading = true;

  @override
  void initState() {
    selectedCountry = widget.country;
    selectedState = widget.state;
    super.initState();
  }

  String query = "";

  @override
  Widget build(BuildContext context) {
    var topPadding = MediaQuery.of(context).padding.top;
    return Container(
      padding: EdgeInsets.only(top: topPadding),
      width: double.infinity,
      height: SizeConfig.h(200 + topPadding),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
          gradient: LinearGradient(
              colors: [AppStyle.secondaryColor, AppStyle.primaryColor],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter)),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(30)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      S.of(context).hello +
                          " " +
                          (widget.name == ''
                              ? 'بك'
                              : widget.name.split(' ')[0]),
                      style: TextStyle(
                          fontFamily: 'HelveticaNeueLTArabicBold',
                          color: AppStyle.whiteColor,
                          fontSize: SizeConfig.h(24),
                          wordSpacing: 3,
                          height: 1),
                    ),
                    Text(
                      S.of(context).do_you_need_help,
                      style: TextStyle(
                        fontFamily: 'HelveticaNeueLTArabicRoman',
                        color: AppStyle.whiteColor,
                        fontSize: SizeConfig.h(16),
                        wordSpacing: 3,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SearchBar(
              controller: searchController,
              country: selectedCountry,
              state: selectedState,
              onFieldSubmitted: (Map<String, dynamic> data) {
                Navigator.pushNamed(context, '/searchPage', arguments: {
                  'country': data['country'],
                  'state': data['state'],
                  'city': data['city'],
                  'name': searchController.text,
                });
              },
              onTap: (Map<String, dynamic> data) {
                selectedCountry = data['country'];
                selectedCity = data['city'];
                selectedState = data['state'];
                widget.onChangeCountry({
                  'country': selectedCountry,
                  'state': selectedState,
                  'city': selectedCity
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
